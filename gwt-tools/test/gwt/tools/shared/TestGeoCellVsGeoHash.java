/**
 * 
 */
package gwt.tools.shared;

import gwt.tools.shared.GeoHash.GeoHashRange;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.gwt.dev.util.collect.HashMap;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class TestGeoCellVsGeoHash {

	static LinkedList<GeoPoint> points = new LinkedList<GeoPoint>();
	static HashMap<GeoPoint, String> pointCells = new HashMap<GeoPoint, String>();
	static HashMap<GeoPoint, Long> pointHashs = new HashMap<GeoPoint, Long>();
	static HashMap<GeoPoint, Boolean> pointValid = new HashMap<GeoPoint, Boolean>();
	static long validCount = 0;
	static GeoPoint northEast;
	static GeoPoint southWest;
	static GeoHash geoHash;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass public static void setUpBeforeClass() throws Exception {
		double east = 7.2, north = 49.5, west = 6.5, south = 49.2; // saarbrücken
		northEast = new GeoPoint(north, east);
		southWest = new GeoPoint(south, west);
		double centerLat = (southWest.getLat() + northEast.getLat()) / 2;
		double centerLon = (southWest.getLon() + northEast.getLon()) / 2;
		geoHash = new GeoHash(null);
		for (int x = 0; x < 100000; x++) {
			//			GeoPoint point = new GeoPoint(180 * Math.random() - 90, 360 * Math.random() - 180);
			GeoPoint point = new GeoPoint(centerLat + 2 * Math.random() - 1, centerLon + 2 * Math.random() - 1);
			points.add(point);
			pointCells.put(point, GeoCellUtil.compute(point, 8));
			pointHashs.put(point, geoHash.get(point.getLat(), point.getLon()));
			boolean isValid = Util.contains(point.getLat(), point.getLon(), south, west, north, east);
			pointValid.put(point, isValid);
			if (isValid) {
				validCount++;
				//			System.out.println(Long.toHexString(pointHashs.get(point)) + ": " + pointValid.get(point) + "; " + point);
			}
		}
		System.out.format("Test data: #points=%d, #valid=%d\n", points.size(), validCount);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After public void tearDown() throws Exception {
	}

	@Test public void testCell3() {
		runCellTest(northEast, southWest, 3);
	}

	@Test public void testCell4() {
		runCellTest(northEast, southWest, 4);
	}

	@Test public void testCell5() {
		runCellTest(northEast, southWest, 5);
	}

	@Test public void testCell6() {
		runCellTest(northEast, southWest, 6);
	}

	//	@Test public void testCell7() {
	//		runCellTest(northEast, southWest, 7);
	//	}

	//	@Test public void testHash1() {
	//		runHashTest(northEast, southWest, null);
	//	}

	@Test public void testHash_60() {
		runHashTest(northEast, southWest, .6);
	}

	@Test public void testHash_50() {
		runHashTest(northEast, southWest, .5);
	}

	@Test public void testHash_40() {
		runHashTest(northEast, southWest, .4);
	}

	@Test public void testHash_3() {
		runHashTest(northEast, southWest, .3);
	}

	@Test public void testHash_22() {
		runHashTest(northEast, southWest, .22);
	}

	@Test public void testHash_21() {
		runHashTest(northEast, southWest, .21);
	}

	@Test public void testHash_2() {
		runHashTest(northEast, southWest, .2);
	}

	@Test public void testHash_19() {
		runHashTest(northEast, southWest, .19);
	}

	@Test public void testHash_17() {
		runHashTest(northEast, southWest, .17);
	}

	@Test public void testHash_15() {
		runHashTest(northEast, southWest, .15);
	}

	@Test public void testHash_1() {
		runHashTest(northEast, southWest, .1);
	}

	@Test public void testHash_100() {
		runHashTest(northEast, southWest, null);
	}

	//	@Test public void testHash() {
	//		geoHash.setAccuracy(.75);
	//		System.out.println(Long.toBinaryString(geoHash.get(50, 50)));
	//		List<GeoHashRange> ranges = geoHash.getBoundingBoxHashes(51, 52, 49, 48);
	//		System.out.println(ranges.size());
	//		for (GeoHashRange range : ranges) {
	//			System.out.println(Long.toBinaryString(range.min) + " - " + Long.toBinaryString(range.max));
	//		}
	//	}

	//	@Test public void testGet() {
	//		String encoded = "u0zh"; // from geohash.org for (50 10)
	//		printLong(GeoHash.decode(encoded));
	//		printLong(geoHash.get(50, 10));
	//	}
	//
	//	@Test public void testGet2() {
	//		String encoded = "t16btf8z9zp6"; // from geohash.org for (7.134199 49.120988)
	//		printLong(GeoHash.decode(encoded));
	//		printLong(geoHash.get(7.134199, 49.120988));
	//	}
	//
	//	@Test public void testGetVsBox() {
	//		//		String encoded = "t16btf8z9zp6"; // from geohash.org for (7.134199 49.120988)
	//		//		printLong(GeoHash.decode(encoded));
	//		printLong(geoHash.get(7.134199, 49.120988));
	//		geoHash.setAccuracy(.25);
	//		List<GeoHashRange> ranges = geoHash.getBoundingBoxHashes(7.134199, 49.120988, 6.134199, 48.120988);
	//		System.out.println(ranges.size() + " - " + geoHash.unifiedRanges);
	//		for (GeoHashRange range : ranges) {
	//			printLong(range.min);
	//			printLong(range.max);
	//			System.out.println();
	//		}
	//	}
	//
	//	@Test public void testEncoding() {
	//		String encoded = "u0zh";
	//		int length = encoded.length();
	//		Long decoded = GeoHash.decode(encoded);
	//		printLong(decoded);
	//		System.out.println(GeoHash.encode(decoded, length));
	//	}

	private void printLong(long longg) {
		System.out.println(geoHash.encode(longg));
		System.out.println(longg);
		System.out.println(Long.toHexString(longg));
		System.out.println(Long.toBinaryString(longg));
	}

	private void runCellTest(GeoPoint northEast, GeoPoint southWest, int resolution) {
		System.out.print("cell r=" + resolution + "   ");
		long start = System.nanoTime();
		List<String> cells = GeoCellUtil.interpolate(GeoCellUtil.compute(northEast, resolution),
			GeoCellUtil.compute(southWest, resolution));
		Collections.sort(cells);
		System.out.format("; # %3d in %6dµs", cells.size(), ((System.nanoTime() - start) / 1000));
		if (cells.size() > 999) {
			System.out.println();
			return;
			//		System.out.println(Util.join(cells, "\n"));
		}

		start = System.currentTimeMillis();
		List<GeoPoint> inRange = getPointsInCells(cells);
		fMeasure(cells, start, inRange);
	}

	private void runHashTest(GeoPoint northEast, GeoPoint southWest, Double accuracy) {
		System.out.format("hash a=%.2f", accuracy);
		geoHash.setAccuracy(accuracy);
		long start = System.nanoTime();
		//		System.out.println("box: " + Long.toHexString(geoHash.get(northEast.getLat(), northEast.getLon())) + " x "
		//			+ Long.toHexString(geoHash.get(southWest.getLat(), southWest.getLon())));
		List<GeoHashRange> ranges = geoHash.getBoundingBoxHashes(northEast.getLat(), northEast.getLon(),
			southWest.getLat(), southWest.getLon());
		//		Collections.sort(cells);
		System.out.format("; # %3d in %6dµs", ranges.size(), ((System.nanoTime() - start) / 1000));
		if (ranges.size() > 999) {
			System.out.println();
			return;
			//		System.out.println("\n" + Util.join(ranges, "\n"));
		}

		start = System.currentTimeMillis();
		List<GeoPoint> inRange = getPointsInRange(ranges);
		fMeasure(ranges, start, inRange);
	}

	private void fMeasure(List<?> sectors, long startTime, List<GeoPoint> points) {
		System.out.format("; #sectors %4d in %4dms", sectors.size(), (System.currentTimeMillis() - startTime));
		double valid = countValid(points, northEast, southWest);
		double precision = valid / points.size();
		double recall = valid / validCount;
		System.out.format("; p=%.4f; r=%.4f; s=%10.1f\n", precision, recall, sectors.size() * (points.size() - valid));
	}

	/**
	 * @param inRange
	 * @param northEast
	 * @param southWest
	 * @return
	 */
	private int countValid(List<GeoPoint> inRange, GeoPoint northEast, GeoPoint southWest) {
		int valid = 0;
		for (GeoPoint point : inRange) {
			if (Util.contains(point.getLat(), point.getLon(), southWest.getLat(), southWest.getLon(),
				northEast.getLat(), northEast.getLon())) {
				valid++;
			}
		}
		return valid;
	}

	/**
	 * @param boundingBoxHashes
	 * @return
	 */
	private List<GeoPoint> getPointsInCells(List<String> cells) {
		// simulates what the datastore request would do
		LinkedList<GeoPoint> matches = new LinkedList<GeoPoint>();
		for (String cell : cells) {
			for (GeoPoint point : points) {
				//				if (GeoCellUtil.containsPoint(cell, point)) {
				if (pointCells.get(point).startsWith(cell)) {
					matches.add(point);
				}
			}
		}
		return matches;
	}

	private List<GeoPoint> getPointsInRange(List<GeoHashRange> ranges) {
		// simulates what the datastore request would do
		LinkedList<GeoPoint> matches = new LinkedList<GeoPoint>();
		for (GeoHashRange range : ranges) {
			for (GeoPoint point : points) {
				if (Util.inRange(pointHashs.get(point), range.min, range.max)) {
					matches.add(point);
				}
			}
		}
		return matches;
	}
}
