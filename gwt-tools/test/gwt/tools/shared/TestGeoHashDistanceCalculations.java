package gwt.tools.shared;

import gwt.tools.shared.GeoHash.GeoHashRange;

import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

public class TestGeoHashDistanceCalculations {

	@Test
	public void test() {
		GeoHash geoHash = new GeoHash(.2);
		GeoPoint point = new GeoPoint(50.6582392, 13.2313552); // Zöblitz lat lng aka y x
		long hash = geoHash.get(point);
		System.out.println(point);
		System.out.println(hash);
		GeoRect boundingBox = geoHash.calculateBoundingBox(point, 10000);
		System.out.println(boundingBox);
		List<GeoHashRange> ranges = geoHash.getBoundingBoxHashes(boundingBox);
		System.out.println(ranges);
		for (GeoHashRange range : ranges) {
			System.out.println(range);
			if (range.min <= hash && range.max >= hash) {
				Assert.assertTrue(true);
				return;
			}
		}
		Assert.fail();
	}

}
