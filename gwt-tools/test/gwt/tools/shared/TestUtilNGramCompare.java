package gwt.tools.shared;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;


public class TestUtilNGramCompare {

	List<String> strings = Util.createList("abc", "abcabc", "sdfgabc", "abcsdfg");
	List<char[]> chars = Util.mapList(strings, new Util.Converter<String, char[]>(){

		@Override
		public char[] convert(String source) {
			return source.toCharArray();
		}
		
	});
	
	@Test
	public void testCompareViaNgramsStringStringInt() {
		for (String a : strings) {
			for (String b : strings) {
				double similarity = Util.compareViaNgrams(a, b, 3);
				System.out.format("%10s %10s %5.5f\n", a, b, similarity);
				testMutality(a, b);
				if (a.equals(b)) {
					assertEquals(similarity, 1d, 0.01);
				}
				else {
					assertTrue(similarity < 1d);
				}
			}
		}
	}
	
	@Test
	public void testStringSpeed() {
		
	}
	
	private void testMutality(String a, String b) {
		assertEquals(Util.compareViaNgrams(a, b, 3), Util.compareViaNgrams(b, a, 3), 0.01);
	}

}
