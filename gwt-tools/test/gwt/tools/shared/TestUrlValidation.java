/**
 * 
 */
package gwt.tools.shared;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class TestUrlValidation {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After public void tearDown() throws Exception {
	}

	private static List<String> urls = Util.createList("http://google.com", "http://google.com/", "google.com/");

	@Test public void test() {
		for (String url : urls) {
			System.out.println(Util.isValidUrl(url) + " - " + url);
			if (!Util.isValidUrl(url)) {
				//				fail("URL validation failed on URL=" + url);
			}
		}

	}

}
