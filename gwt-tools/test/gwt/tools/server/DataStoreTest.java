package gwt.tools.server;

import gwt.tools.server.dao.RawDatastoreDao;
import gwt.tools.server.dao.cache.DatastoreDao;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

public class DataStoreTest {

	private final LocalServiceTestHelper helper = new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());

	@Before public void setUp() {
		helper.setUp();
	}

	@After public void tearDown() {
		helper.tearDown();
	}

	public static class TestClass {
		String someText;
	}

	@Test public void test() {

		DatastoreDao dao = new RawDatastoreDao(this.getClass().getName());
		//Movie entity =new Movie("hallo", "test", "testtest", 0L);

		//		dao.put("1", entity );

		TestClass result = (TestClass) dao.get("1");

		//		ObjectDatastore datastore  = new AnnotationObjectDatastore(); 
		//
		//		
		//		// store the instance and all other reachable instances
		//		Key key = datastore.store(entity);
		//
		//		// converted into a query by kind with a key name
		//		Movie result = datastore.load(Movie.class,"hallo");
		//
		//		// the identical instance is always returned from same datastore

		//System.out.println((Movie)result.object);
	}
}
