/**
 * 
 */
package gwt.tools.server;

import gwt.tools.shared.Util;

import java.util.Collection;
import java.util.HashSet;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class TestUtilUnion {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After public void tearDown() throws Exception {
	}

	public static class TestObject {
		public final String a;
		public final String b;

		/**
		 * 
		 */
		public TestObject(String a, String b) {
			this.a = a;
			this.b = b;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override public boolean equals(Object obj) {
			return (obj instanceof TestObject) && (a.equals(((TestObject) obj).a));
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override public String toString() {
			return "TestObject(" + a + ", " + b + ")";
		}
	}

	@Test public void test() {
		Collection<TestObject> c1 = Util.createList(new TestObject("a", null), new TestObject("b", null));
		Collection<TestObject> c2 = Util.createList(new TestObject("a", "asdf"), new TestObject("b", "asdf"));
		long start = System.currentTimeMillis();
		Collection<TestObject> union = Util.union(c2, c1);
		System.out.println(System.currentTimeMillis() - start);
		System.out.println(union);
		//		HashSet<TestObject> h1 = new HashSet<TestObject>(c1);
		//		HashSet<TestObject> h2 = new HashSet<TestObject>(c2);
		HashSet<TestObject> hu = new HashSet<TestObject>(c1);
		hu.addAll(c2);
		System.out.println(hu);
	}

}
