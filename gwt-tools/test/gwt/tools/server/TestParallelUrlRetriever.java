/**
 * 
 */
package gwt.tools.server;

import gwt.tools.server.dao.ParallelUrlRetriever;
import gwt.tools.shared.exceptions.NetworkException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.LinkedList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.appengine.tools.development.testing.LocalURLFetchServiceTestConfig;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class TestParallelUrlRetriever { // extends GWTTestCase {

	private final LocalServiceTestHelper helper = new LocalServiceTestHelper(new LocalURLFetchServiceTestConfig());

	public static String baseUrl = "http://api.freebase.com/api/service/mqlread?query=";
	public static List<String> ids = Util.createList("/en/45_2006", "/en/9_2005", "/en/10_1979", "/en/54",
		"/en/69_2004", "/en/101", "/en/300_2007", "/en/1900", "/en/1941", "/en/1969", "/en/1984_1984", "/en/2046_2004",
		"/en/three_amigos", "/en/quien_es_el_senor_lopez", "/en/and_justice_for_all_1979",
		"/en/what_have_i_done_to_deserve_this", "/en/batteries_not_included",
		"/en/weird_al_yankovic_the_ultimate_collection", "/en/manos_the_hands_of_fate", "/en/weird_al_yankovic_live",
		"/en/weird_al_yankovic_the_ultimate_video_collection", "/en/weird_al_yankovic_the_videos",
		"/en/16_vayathinile", "/en/15_park_avenue", "/en/20_million_miles_to_earth", "/en/36_hours",
		"/en/2_fast_2_furious", "/en/99_river_street", "/en/37_2_le_matin", "/en/8mm", "/en/5_card_stud",
		"/en/7g_rainbow_colony", "/en/3-iron", "/en/11_harrowhouse", "/en/3_10_to_yuma_1957",
		"/en/20_000_leagues_under_the_sea", "/en/2010_the_year_we_make_contact", "/en/10_5_apocalypse", "/en/8_mile",
		"/en/681_ad_the_glory_of_khan", "/en/100_girls", "/en/40_days_and_40_nights", "/en/50_cent_the_new_breed",
		"/en/3_the_dale_earnhardt_story", "/en/200_motels", "/en/61__2001", "/en/24_hour_party_people",
		"/en/8_million_ways_to_die", "/en/10th_wolf", "/en/25th_hour", "/en/48_hrs", "/en/7_seconds_2005",
		"/en/28_days_later", "/en/21_grams", "/en/3_women", "/en/9th_company", "/en/45_minutes_from_hollywood",
		"/en/102_dalmatians", "/en/16_years_of_alcohol", "/en/1942_a_love_story", "/en/12b", "/en/2009_lost_memories",
		"/en/16_blocks", "/en/15_minutes", "/en/50_first_dates", "/en/3_chains_o_gold", "/en/8_heads_in_a_duffel_bag",
		"/en/9_songs", "/en/3_ninjas_high_noon_at_mega_mountain", "/en/20_fingers_2004", "/en/06_18_67",
		"/en/3_needles", "/en/28_days_2000", "/en/42nd_street_1933", "/en/101_dalmatians", "/en/12_angry_men",
		"/en/36_china_town", "/en/2001_a_space_odyssey", "/en/10_things_i_hate_about_you",
		"/en/7_mujeres_1_homosexual_y_carlos", "/en/3_ninjas", "/en/88_minutes", "/en/500_years_later",
		"/en/8_seconds", "/en/100_arabica", "/en/1971", "/en/50_ways_of_saying_fabulous",
		"/en/1492_conquest_of_paradise", "/en/5x2", "/en/2019_after_the_fall_of_new_york", "/en/301_302", "/en/4d_man",
		"/en/7_television_commercials", "/en/28_weeks_later", "/en/13_ghosts", "/en/10_5", "/en/13_going_on_30",
		"/en/2ldk", "/en/36_chowringee_lane", "/en/7_phere");
	public static List<String> urls = new LinkedList<String>();
	static {
		for (String id : ids) {
			urls.add(baseUrl + Util.encodeUriParameter("{\"query\":[{\"type\" : [], \"id\":\"" + id + "\"}]}"));
		}
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass public static void setUpBeforeClass() throws Exception {
	}

	@Before public void setUp() {
		helper.setUp();
	}

	@After public void tearDown() {
		helper.tearDown();
	}

	class Request {

		public final int index;

		/**
		 * @param x
		 */
		public Request(int index) {
			this.index = index;
		}

	}

	@Test public void testParallel() throws NetworkException, IOException {
		System.out.println("### Loading " + urls.size() + " URLs in parallel.");
		long start = System.currentTimeMillis();
		final long[] bytesLoaded = new long[] { 0 };
		ParallelUrlRetriever.Retriever retriever = new ParallelUrlRetriever.Retriever();
		for (int x = 0; x < ids.size(); x++) {
			final String url = urls.get(x);
			final int index = x;
			retriever.addUrl(url, new AsyncCallback<byte[]>() {

				@Override public void onSuccess(byte[] result) {
					bytesLoaded[0] += result.length;
				}

				@Override public void onFailure(Throwable caught) {
					System.out.println("error for URL[" + index + "]=" + url + ": ");
					caught.printStackTrace();
				}
			});
		}
		ParallelUrlRetriever.waitForAll();
		System.out.println("Total time = " + (System.currentTimeMillis() - start) + "ms; bytes loaded=" + bytesLoaded);
	}

	@Test public void testSerial() throws MalformedURLException, NetworkException {
		System.out.println("### Loading " + urls.size() + " URLs serially.");
		long start = System.currentTimeMillis();
		long bytesLoaded = 0;
		for (int x = 0; x < ids.size(); x++) {
			String result = Util.urlContentToString(urls.get(x));
			if (Util.isEmpty(result)) {
				System.out.println("error for url " + x + ": " + urls.get(x));
			} else {
				bytesLoaded += result.length();
			}
		}
		System.out.println("Total time = " + (System.currentTimeMillis() - start) + "ms; bytes loaded=" + bytesLoaded);
	}

	//	/*
	//	 * (non-Javadoc)
	//	 * 
	//	 * @see com.google.gwt.junit.client.GWTTestCase#getModuleName()
	//	 */
	//	@Override public String getModuleName() {
	//		return "gwt.tools.GwtTools";
	//	}

}
