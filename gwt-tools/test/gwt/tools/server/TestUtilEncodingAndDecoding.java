package gwt.tools.server;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Test;

public class TestUtilEncodingAndDecoding {

	@Test
	public void test() {
		for (int x = 0; x < 100000; x++) {
			Long number = (long) (Math.random() * Long.MAX_VALUE);
			String encoded = Util.encode(number);
			System.out.println(number + " -> "+encoded);
			Assert.assertEquals(number, Util.decode(encoded));
		}
	}

}
