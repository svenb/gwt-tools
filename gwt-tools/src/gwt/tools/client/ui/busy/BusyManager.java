/**
 * 
 */
package gwt.tools.client.ui.busy;

import gwt.tools.shared.Log;
import gwt.tools.shared.handler.SimpleCallback;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.user.client.Timer;

/**
 * Internal class for managing delay of showing the indicator and timeout
 * handling.
 * 
 * @author Sven Buschbeck
 * 
 */
public class BusyManager implements DefaultFeatures {

	public static int DEFAULT_TIMEOUT = 30000;

	private final SetVisibleCallback callback;
	private int busyCount = 0;
	private boolean becomingVisible = false;
	private int delayUntilVisible;
	private int timeout = DEFAULT_TIMEOUT;
	private Timer timeoutTimer;
	private SimpleCallback timeoutCallback;

	public static interface SetVisibleCallback {
		void showIndicator(boolean show);
	}

	public BusyManager(SetVisibleCallback callback) {
		assert callback != null;
		this.callback = callback;
	}

	public void setBusy(boolean busy) {
		busyCount = Math.max(0, busy ? busyCount + 1 : busyCount - 1);
		Log.debug(this, ".setBusy: busy,count", busy, busyCount);
		updateVisibility(isBusy());
	}

	public boolean isBusy() {
		return busyCount > 0;
	}

	private void updateVisibility(boolean visible) {
		boolean changed = visible != becomingVisible;
		becomingVisible = visible;
		if (changed && becomingVisible) {
			if (becomingVisible && getDelayUntilVisible() > 0) {
				// don't show right away...
				Scheduler.get().scheduleFixedDelay(new RepeatingCommand() {

					@Override public boolean execute() {
						updateVisibility(becomingVisible);
						return false;
					}
				}, getDelayUntilVisible());
			} else {
				callback.showIndicator(visible);
			}
			resetTimer();
		} else {
			callback.showIndicator(visible);
		}
	}

	private void resetTimer() {
		//		Log.info("BusyIndicator.resetTimer: timout=" + timeout + "; visible=" + isVisible());
		stopTimer();

		if (timeout > 0 && isBusy()) {
			//			final BusyIndicator selfReference = this;
			//			Log.info("BusyIndicator.resetTimer: new timeout of " + timeout + " milliseconds; visible=" + isVisible());
			if (timeoutTimer != null) {
				timeoutTimer.cancel();
			}
			(timeoutTimer = new Timer() {

				@Override public void run() {
					//					Log.info("BusyIndicator.resetTimer: timeout! got callback=" + (getTimeoutCallback() != null)
					//						+ "; visible=" + isVisible());
					setBusy(false);
					//					if (getTimeoutCallback() != null) {
					//						//						getTimeoutCallback().done(selfReference, true);
					//						getTimeoutCallback().done();
					//					}
				}
			}).schedule(timeout);
		}
	}

	private void stopTimer() {
		//		Log.info("BusyIndicator.stopTimer: timeout=" + timeout + "; visible=" + isVisible());
		if (timeoutTimer != null) {
			timeoutTimer.cancel();
		}
	}

	public int getDelayUntilVisible() {
		return delayUntilVisible;
	}

	public void setDelayUntilVisible(int delayUntilVisible) {
		this.delayUntilVisible = delayUntilVisible;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.client.ui.busy.DefaultFeatures#getTimeout()
	 */
	@Override public int getTimeout() {
		return timeout;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.client.ui.busy.DefaultFeatures#setTimeout(int)
	 */
	@Override public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

}
