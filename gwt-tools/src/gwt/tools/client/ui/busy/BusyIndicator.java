package gwt.tools.client.ui.busy;

import gwt.tools.shared.Log;
import gwt.tools.shared.Util;
import gwt.tools.shared.handler.SimpleCallback;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

public class BusyIndicator extends Composite {

	private static BusyIndicatorUiBinder uiBinder = GWT.create(BusyIndicatorUiBinder.class);

	interface BusyIndicatorUiBinder extends UiBinder<Widget, BusyIndicator> {
	}

	public static int DEFAULT_TIMEOUT = 30000;

	public static enum Position {
		Center, Left, Right, Above, Below;
	}

	@UiField Image progressIndicatorImage;

	private String stylePositionBackup = null;
	private HandlerRegistration handlerRegistration = null;

	private Widget overlayedWidget;
	private int timeout = DEFAULT_TIMEOUT;
	private Timer timeoutTimer;
	private SimpleCallback timeoutCallback;
	private int busyCount = 0;
	private boolean becomingVisible = false;
	private Position position = Position.Center;
	private int delayUntilVisible;

	public static BusyIndicator create(Widget widgetToBeOverlayed) {
		return create(widgetToBeOverlayed, null);
	}

	public static BusyIndicator create(Widget widgetToBeOverlayed, String indicatorImageUrl) {
		return create(widgetToBeOverlayed, indicatorImageUrl, false);
	}

	public static BusyIndicator create(Widget widgetToBeOverlayed, String indicatorImageUrl, boolean visible) {
		return create(widgetToBeOverlayed, indicatorImageUrl, visible, Position.Center, 100, null);
	}

	public static BusyIndicator create(Widget widgetToBeOverlayed, String indicatorImageUrl, boolean visible,
		Position position, int delayUntilVisible, SimpleCallback timeoutCallback) {
		BusyIndicator busyIndicator = new BusyIndicator(widgetToBeOverlayed, visible, indicatorImageUrl);
		busyIndicator.setPosition(position);
		busyIndicator.setDelayUntilVisible(delayUntilVisible);
		//		RootLayoutPanel.get().add(busyIndicator);
		Document.get().getBody().appendChild(busyIndicator.getElement());
		busyIndicator.setTimeoutCallback(timeoutCallback);
		return busyIndicator;
	}

	public static BusyIndicator create(Widget widgetToBeOverlayed, ImageResource indicatorImageResource,
		boolean visible, Position position, int delayUntilVisible, SimpleCallback timeoutCallback) {
		BusyIndicator busyIndicator = new BusyIndicator(widgetToBeOverlayed, visible, indicatorImageResource);
		busyIndicator.setPosition(position);
		busyIndicator.setDelayUntilVisible(delayUntilVisible);
		//		RootLayoutPanel.get().add(busyIndicator);
		Document.get().getBody().appendChild(busyIndicator.getElement());
		busyIndicator.setTimeoutCallback(timeoutCallback);
		return busyIndicator;
	}

	/**
	 * @param delayUntilVisible
	 */
	private void setDelayUntilVisible(int delayUntilVisible) {
		this.delayUntilVisible = delayUntilVisible;
	}

	public BusyIndicator() {
		initWidget(uiBinder.createAndBindUi(this));
		resetTimer();
	}

	public BusyIndicator(boolean visible) {
		this();
		setVisible(visible);
	}

	public BusyIndicator(Widget widgetToBeOverlayed, boolean visible) {
		this(visible);
		if (widgetToBeOverlayed != null) {
			overlay(widgetToBeOverlayed);
		}
	}

	public BusyIndicator(Widget widgetToBeOverlayed, boolean visible, String indicatorImageUrl) {
		this(widgetToBeOverlayed, visible);
		if (Util.notEmpty(indicatorImageUrl)) {
			progressIndicatorImage.setUrl(indicatorImageUrl);
		}
	}

	public BusyIndicator(Widget widgetToBeOverlayed, boolean visible, ImageResource indicatorImageResource) {
		this(widgetToBeOverlayed, visible);
		if (indicatorImageResource != null) {
			setImage(indicatorImageResource);
		}
	}

	/**
	 * 
	 * Instead of showing the indicator image in a block box, it gets shown as
	 * overlay for the given widget, or returns to default behavior if the given
	 * widget is null.
	 * 
	 * @param overlayWidget
	 */
	public void overlay(final Widget overlayWidget) {
		this.overlayedWidget = overlayWidget;
		Style style = progressIndicatorImage.getElement().getStyle();
		if (overlayWidget != null) {
			stylePositionBackup = style.getProperty("position");
			style.setProperty("position", "fixed");
			adjustOverlay();
		} else {
			style.setProperty("position", (stylePositionBackup != null) ? stylePositionBackup : "static");
			if (handlerRegistration != null) {
				handlerRegistration.removeHandler();
			}
		}
	}

	public void setOverlayedWidget(Widget widget) {
		overlay(widget);
	}

	public Widget getOverlayedWidget() {
		return overlayedWidget;
	}

	private void adjustOverlay() {
		if (overlayedWidget == null) {
			return;
		}
		Style style = progressIndicatorImage.getElement().getStyle();
		//		style.setPropertyPx("left", overlayWidget.getAbsoluteLeft());
		//		style.setPropertyPx("top", overlayWidget.getAbsoluteTop());
		int x;
		int y;
		switch (position) {
		case Left:
			x = overlayedWidget.getAbsoluteLeft() - getOffsetWidth();
			y = overlayedWidget.getAbsoluteTop() + overlayedWidget.getOffsetHeight() / 2 - getOffsetHeight() / 2;
			break;
		case Right:
			x = overlayedWidget.getAbsoluteLeft() + overlayedWidget.getOffsetWidth();
			y = overlayedWidget.getAbsoluteTop() + overlayedWidget.getOffsetHeight() / 2 - getOffsetHeight() / 2;
			break;
		case Above:
			x = overlayedWidget.getAbsoluteLeft() + overlayedWidget.getOffsetWidth() / 2 - getOffsetWidth() / 2;
			y = overlayedWidget.getAbsoluteTop() - getOffsetHeight();
			break;
		case Below:
			x = overlayedWidget.getAbsoluteLeft() + overlayedWidget.getOffsetWidth() / 2 - getOffsetWidth() / 2;
			y = overlayedWidget.getAbsoluteTop() + overlayedWidget.getOffsetHeight();
			break;
		case Center:
		default:
			x = overlayedWidget.getAbsoluteLeft() + overlayedWidget.getOffsetWidth() / 2 - getOffsetWidth() / 2;
			y = overlayedWidget.getAbsoluteTop() + overlayedWidget.getOffsetHeight() / 2 - getOffsetHeight() / 2;
			break;
		}

		style.setPropertyPx("left", x);
		style.setPropertyPx("top", y);

		if (isVisible()) {
			overlayedWidget.getElement().getStyle().setOpacity(0.5);
		} else {
			overlayedWidget.getElement().getStyle().setOpacity(1);
		}
	}

	public void setBusy(boolean busy) {
		busyCount = Math.max(0, busy ? busyCount + 1 : busyCount - 1);
		Log.debug(this, ".setBusy: busy,count", busy, busyCount);
		updateVisibility(busyCount > 0);
	}

	private void updateVisibility(boolean visible) {
		boolean changed = visible != becomingVisible;
		becomingVisible = visible;
		if (changed && becomingVisible) {
			if (becomingVisible && getDelayUntilVisible() > 0) {
				// don't show right away...
				Scheduler.get().scheduleFixedDelay(new RepeatingCommand() {

					@Override public boolean execute() {
						updateVisibility(becomingVisible);
						return false;
					}
				}, getDelayUntilVisible());
			} else {
				super.setVisible(visible);
				adjustOverlay();
			}
			resetTimer();
		} else {
			super.setVisible(visible);
		}
	}

	/**
	 * @deprecated Use {@link #setBusy(boolean)} instead as it incoporates a
	 *             counter so that multiple calls to setBusy(true) will require
	 *             the same amout of setBusy(false) to make the indicator
	 *             appear. This especially helpful if several processes use a
	 *             single busy indicator. Otherwise, this method will reset that
	 *             counter.
	 */
	@Deprecated @Override public void setVisible(boolean visible) {
		assert false;
		// using setVisible directly will reset the busy counter
		busyCount = visible ? 1 : 0;
		updateVisibility(visible);
	}

	public void busy() {
		setBusy(true);
	}

	public void idle() {
		setBusy(false);
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
		resetTimer();
	}

	public int getTimeout() {
		return timeout;
	}

	private void resetTimer() {
		//		Log.info("BusyIndicator.resetTimer: timout=" + timeout + "; visible=" + isVisible());
		stopTimer();

		if (timeout > 0 && isVisible()) {
			//			final BusyIndicator selfReference = this;
			//			Log.info("BusyIndicator.resetTimer: new timeout of " + timeout + " milliseconds; visible=" + isVisible());
			if (timeoutTimer != null) {
				timeoutTimer.cancel();
			}
			(timeoutTimer = new Timer() {

				@Override public void run() {
					//					Log.info("BusyIndicator.resetTimer: timeout! got callback=" + (getTimeoutCallback() != null)
					//						+ "; visible=" + isVisible());
					setBusy(false);
					if (getTimeoutCallback() != null) {
						//						getTimeoutCallback().done(selfReference, true);
						getTimeoutCallback().done();
					}
				}
			}).schedule(timeout);
		}
	}

	private void stopTimer() {
		//		Log.info("BusyIndicator.stopTimer: timeout=" + timeout + "; visible=" + isVisible());
		if (timeoutTimer != null) {
			timeoutTimer.cancel();
		}
	}

	public void setTimeoutCallback(SimpleCallback timeoutCallback) {
		this.timeoutCallback = timeoutCallback;
	}

	public SimpleCallback getTimeoutCallback() {
		return timeoutCallback;
	}

	@Override protected void onUnload() {
		stopTimer();
		setVisible(false); // make sure the overlay is restored
		super.onUnload();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.user.client.ui.Widget#onLoad()
	 */
	@Override protected void onLoad() {
		resetTimer();
		super.onLoad();
	}

	/**
	 * Sets the image resource to be used when showing the busy indicator
	 * 
	 * @param image
	 */
	public void setImage(ImageResource image) {
		progressIndicatorImage.setResource(image);
	}

	public void setPosition(Position position) {
		this.position = position;
		adjustOverlay();
	}

	public int getDelayUntilVisible() {
		return delayUntilVisible;
	}
}
