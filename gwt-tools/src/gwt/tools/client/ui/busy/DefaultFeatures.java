/**
 * 
 */
package gwt.tools.client.ui.busy;

interface DefaultFeatures {
	int getDelayUntilVisible();

	void setDelayUntilVisible(int delayUntilVisible);

	int getTimeout();

	void setTimeout(int timeout);

	void setBusy(boolean busy);

	boolean isBusy();
}