/**
 * 
 */
package gwt.tools.client.ui.busy;

import gwt.tools.client.ui.busy.BusyManager.SetVisibleCallback;
import gwt.tools.client.ui.common.DummyWidget;

import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class FloatingBusyIndicator extends DummyWidget implements DefaultFeatures {

	public final InlineBusyIndicator indicator = new InlineBusyIndicator();
	private Widget relatedWidget;
	private BusyManager mgr = new BusyManager(new SetVisibleCallback() {

		@Override public void showIndicator(boolean visible) {
			if (visible) {
				assert relatedWidget != null : "Not show a FloatingBusyIndicator without a related widget. (Use setRelatedWidget() or the relatedWidget=\"{...}\" property.";
				popup.showRelativeTo(relatedWidget);
			} else {
				popup.hide();
			}
		}
	});

	private PopupPanel popup;

	public FloatingBusyIndicator() {
		super();
		popup = new PopupPanel(false, false);
		popup.setStyleName("gt-floating-busy-indicator");
		indicator.setVisible(true);
		popup.setWidget(indicator);
	}

	public void setRelatedWidget(Widget widget) {
		relatedWidget = widget;
	}

	public Widget getRelatedWidget() {
		return relatedWidget;
	}

	public void setImage(ImageResource resource) {
		indicator.setImage(resource);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.client.ui.busy.DefaultFeatures#isBusy()
	 */
	@Override public boolean isBusy() {
		return mgr.isBusy();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.client.ui.busy.DefaultFeatures#setBusy(boolean)
	 */
	@Override public void setBusy(boolean busy) {
		mgr.setBusy(busy);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.client.ui.busy.DefaultFeatures#getDelayUntilVisible()
	 */
	@Override public int getDelayUntilVisible() {
		return mgr.getDelayUntilVisible();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.client.ui.busy.DefaultFeatures#setDelayUntilVisible(int)
	 */
	@Override public void setDelayUntilVisible(int delayUntilVisible) {
		mgr.setDelayUntilVisible(delayUntilVisible);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.client.ui.busy.DefaultFeatures#getTimeout()
	 */
	@Override public int getTimeout() {
		return mgr.getTimeout();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.client.ui.busy.DefaultFeatures#setTimeout(int)
	 */
	@Override public void setTimeout(int timeout) {
		mgr.setTimeout(timeout);
	}

}
