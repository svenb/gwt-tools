/**
 * 
 */
package gwt.tools.client.ui.busy;

import gwt.tools.client.ui.busy.BusyManager.SetVisibleCallback;
import gwt.tools.client.ui.common.Blend;
import gwt.tools.client.ui.common.DummyWidget;

import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.Widget;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class OverlayBusyIndicator extends DummyWidget implements DefaultFeatures {

	public final InlineBusyIndicator indicator = new InlineBusyIndicator();
	private Widget overlayedWidget;
	private BusyManager mgr = new BusyManager(new SetVisibleCallback() {

		private Blend blend;

		@Override public void showIndicator(boolean visible) {
			if (visible) {
				if (blend == null) {
					blend = new Blend(overlayedWidget, indicator);
					blend.addStyleName("gt-overlay-busy-indicator");
				} else {
					blend.show();
				}
			} else {
				assert blend != null;
				blend.hide();
			}
		}
	});

	public OverlayBusyIndicator() {
	}

	public Widget getOverlayedWidget() {
		return overlayedWidget;
	}

	public void setOverlayedWidget(Widget overlayedWidget) {
		this.overlayedWidget = overlayedWidget;
	}

	public void setImage(ImageResource resource) {
		indicator.setImage(resource);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.client.ui.busy.DefaultFeatures#isBusy()
	 */
	@Override public boolean isBusy() {
		return mgr.isBusy();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.client.ui.busy.DefaultFeatures#setBusy(boolean)
	 */
	@Override public void setBusy(boolean busy) {
		mgr.setBusy(busy);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.client.ui.busy.DefaultFeatures#getDelayUntilVisible()
	 */
	@Override public int getDelayUntilVisible() {
		return mgr.getDelayUntilVisible();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.client.ui.busy.DefaultFeatures#setDelayUntilVisible(int)
	 */
	@Override public void setDelayUntilVisible(int delayUntilVisible) {
		mgr.setDelayUntilVisible(delayUntilVisible);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.client.ui.busy.DefaultFeatures#getTimeout()
	 */
	@Override public int getTimeout() {
		return mgr.getTimeout();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.client.ui.busy.DefaultFeatures#setTimeout(int)
	 */
	@Override public void setTimeout(int timeout) {
		mgr.setTimeout(timeout);
	}

}
