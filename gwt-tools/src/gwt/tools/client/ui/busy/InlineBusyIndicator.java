/**
 * 
 */
package gwt.tools.client.ui.busy;

import gwt.tools.client.resources.Resources;
import gwt.tools.client.ui.busy.BusyManager.SetVisibleCallback;

import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.Image;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class InlineBusyIndicator extends Image implements DefaultFeatures {

	private BusyManager mgr = new BusyManager(new SetVisibleCallback() {

		@Override public void showIndicator(boolean visible) {
			setVisible(visible);
		}
	});

	public InlineBusyIndicator() {
		setImage(Resources.instance.getBusyIndicator());
		addStyleName("gt-inline-busy-indicator");
		setVisible(false);
	}

	public void setImage(ImageResource resource) {
		setResource(resource);
	}

	public void setImageUrl(ImageResource resource) {
		setResource(resource);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.client.ui.busy.DefaultFeatures#isBusy()
	 */
	@Override public boolean isBusy() {
		return mgr.isBusy();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.client.ui.busy.DefaultFeatures#setBusy(boolean)
	 */
	@Override public void setBusy(boolean busy) {
		mgr.setBusy(busy);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.client.ui.busy.DefaultFeatures#getDelayUntilVisible()
	 */
	@Override public int getDelayUntilVisible() {
		return mgr.getDelayUntilVisible();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.client.ui.busy.DefaultFeatures#setDelayUntilVisible(int)
	 */
	@Override public void setDelayUntilVisible(int delayUntilVisible) {
		mgr.setDelayUntilVisible(delayUntilVisible);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.client.ui.busy.DefaultFeatures#getTimeout()
	 */
	@Override public int getTimeout() {
		return mgr.getTimeout();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.client.ui.busy.DefaultFeatures#setTimeout(int)
	 */
	@Override public void setTimeout(int timeout) {
		mgr.setTimeout(timeout);
	}

}
