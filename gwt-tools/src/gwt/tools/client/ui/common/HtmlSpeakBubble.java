package gwt.tools.client.ui.common;

import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.Widget;

public class HtmlSpeakBubble extends SpeakBubble implements HasHTML {

	private HTML htmlContent;

	public HtmlSpeakBubble() {
		super();
	}

	public HtmlSpeakBubble(String html, String cssClass, Widget showNextTo) {
		super(cssClass, showNextTo);
		setHTML(html);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.user.client.ui.HasText#getText()
	 */
	@Override public String getText() {
		return (htmlContent != null) ? htmlContent.getText() : null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.user.client.ui.HasText#setText(java.lang.String)
	 */
	@Override public void setText(String text) {
		set(htmlContent = new HTML(text));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.user.client.ui.HasHTML#getHTML()
	 */
	@Override public String getHTML() {
		return (htmlContent != null) ? htmlContent.getHTML() : null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.user.client.ui.HasHTML#setHTML(java.lang.String)
	 */
	@Override public void setHTML(String html) {
		set(htmlContent = new HTML(SafeHtmlUtils.fromTrustedString(html)));
	}

}
