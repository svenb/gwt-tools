/**
 * 
 */
package gwt.tools.client.ui.common;

import gwt.tools.client.HandlerRegistrationManager;
import gwt.tools.client.Util;
import gwt.tools.client.Util.BoundingBox;
import gwt.tools.shared.Log;

import java.util.Iterator;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class SpeakBubble extends Composite implements HasWidgets {

	private static int MIN_ARROW_DISTANCE_X = 20;
	private static int MIN_ARROW_DISTANCE_Y = 20;

	private static SpeakBubbleUiBinder uiBinder = GWT.create(SpeakBubbleUiBinder.class);

	interface SpeakBubbleUiBinder extends UiBinder<Widget, SpeakBubble> {
	}

	@UiField HTMLPanel panel;
	@UiField HTMLPanel arrow;
	@UiField HTMLPanel arrowBackground;
	@UiField HTMLPanel arrowForeground;
	// @UiField HTML content;
	@UiField VerticalFlowPanel content;

	public static enum ShowMode {
		hide(false), show(false), on_hover(true), toggle_on_click(true);

		public final boolean requiresActivationWidget;

		private ShowMode(boolean requiresActivationWidget) {
			this.requiresActivationWidget = requiresActivationWidget;
		}
	}

	public static interface VisibilityChanged {
		void visibilityChanged(boolean visible);
	}

	public VisibilityChanged visibilityChanged;

	private String backgroundColor;
	private String borderColor;
	private ShowMode showMode = ShowMode.show;

	private PopupPanel popup;
	private Widget showNextTo;
	private Widget activationWidget;
	private int showDelayed = 0;
	private HandlerRegistrationManager regMan = new HandlerRegistrationManager();
	private boolean isShowing = false;

	private static enum Direction {
		up, down, left, right;
	}

	public SpeakBubble() {
		initWidget(new DummyWidget());
		popup = new PopupPanel(true);
		popup.setStylePrimaryName("gt-speak-bubble-popup");
		popup.setWidget(uiBinder.createAndBindUi(this));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.user.client.ui.UIObject#addStyleName(java.lang.String)
	 */
	@Override public void addStyleName(String style) {
		popup.addStyleName(style);
	}

	public SpeakBubble(String cssClass, Widget showNextTo) {
		this();
		setNextTo(showNextTo);
		addStyleName(cssClass);
	}

	private void adjustArrow() {
		BoundingBox tb = Util.getBoundingBox(showNextTo);
		// int tx = Util.getCenterX(showNextTo);
		// int ty = Util.getCenterY(showNextTo);
		// int cx = Util.getCenterX(popup);
		// int cy = Util.getCenterY(popup);
		// BoundingBox bb = Util.getBoundingBox(popup.getWidget());
		// HACK absLeft and top return 0 both despite that left and top is set on
		// the element's CSS. So we take the values from the CSS directly instead.
		BoundingBox bb = Util.getBoundingBox((int) Util.getValueAndUnit(popup, "left").value,
				(int) Util.getValueAndUnit(popup, "top").value, panel);
		Direction direction = null;
		if (tb.centerY <= bb.top) {
			direction = Direction.up;
		} else {
			if (tb.centerY >= bb.bottom) {
				direction = Direction.down;
			} else {
				if (tb.centerX <= bb.left) {
					direction = Direction.left;
				} else {
					if (tb.centerX >= bb.right) {
						direction = Direction.right;
					} else {
						assert false : "bb=" + bb + "; tb=" + tb;
					}
				}
			}
		}
		// Log.debug(this,
		// ".adjustArrow: tb,bb,p.x,p.y,p.w.width,p.w.height,panel.bb,direction",
		// tb, bb,
		// popup.getPopupLeft(), popup.getPopupTop(),
		// popup.getWidget().getOffsetWidth(), popup.getWidget()
		// .getOffsetHeight(), Util.getBoundingBox(panel), direction);
		// C WRN 2164 SpeakBubble: .adjustArrow:
		// tx,ty,bb,p.x,p.y,p.w.width,p.w.height,panel.bb,direction=1773,11,Util.BoundingBox(trbl
		// 17 103 99 1767),0,0,103,99,Util.BoundingBox(trbl 0 103 99 0),up
		// tx=1773; p.x = 1767
		// C WRN 9160 SpeakBubble: .adjustArrow:
		// tx,ty,bb,p.x,p.y,p.w.width,p.w.height,panel.bb,direction=1773,11,Util.BoundingBox(trbl
		// 17 659 104 1767),556,5,103,99,Util.BoundingBox(trbl 5 659 104 556),up
		// tx = 1773, bb.x = 1767, bb.r=
		setArrowDirection(direction);
		switch (direction) {
		case up:
		case down:
			// int arrowWidth = arrow.getOffsetWidth(); handled by CSS now: the fore
			// and background arrow are position:relative so that arrow.x==0 is the
			// center of the arrow
			// int halfArrowWidth = arrowWidth / 2;
			int deltaX = tb.centerX - bb.left;// - halfArrowWidth;
			if (deltaX < MIN_ARROW_DISTANCE_X) {
				// Log.debug(this, ".adjustArrow: target too far left");
				panel.getElement().getStyle().setLeft(deltaX - MIN_ARROW_DISTANCE_X, Unit.PX);
				arrow.getElement().getStyle().setLeft(MIN_ARROW_DISTANCE_X, Unit.PX);
			} else if (deltaX > bb.width - MIN_ARROW_DISTANCE_X) {
				// Log.debug(this, ".adjustArrow: target too far right");
				panel.getElement().getStyle().setLeft(deltaX - bb.width + MIN_ARROW_DISTANCE_X, Unit.PX);
				arrow.getElement().getStyle().setRight(MIN_ARROW_DISTANCE_X, Unit.PX);
			} else {
				// Log.debug(this,
				// ".adjustArrow: target within speak bubble dimensions");
				arrow.getElement().getStyle().setLeft(deltaX, Unit.PX);
			}
			if (backgroundColor != null) {
				// Log.debug(this, ".adjustArrow: setting background color",
				// backgroundColor);
				Util.setStyle(arrowForeground, "borderColor", backgroundColor + " transparent");
			}
			if (borderColor != null) {
				// Log.debug(this, ".adjustArrow: setting border color", borderColor);
				Util.setStyle(arrowBackground, "borderColor", borderColor + " transparent");
			}
			break;
		case left:
		case right:
			// if (tb.centerY < bb.top) {
			// arrow.getElement().getStyle().setTop(20, Unit.PX);
			// } else if (tb.centerY > bb.bottom) {
			// arrow.getElement().getStyle().setBottom(20, Unit.PX);
			// } else {
			// arrow.getElement().getStyle().setTop(tb.centerY - bb.top, Unit.PX);
			// }
			int deltaY = tb.centerY - bb.top;
			if (deltaY < MIN_ARROW_DISTANCE_Y) {
				panel.getElement().getStyle().setLeft(deltaY - MIN_ARROW_DISTANCE_Y, Unit.PX);
				arrow.getElement().getStyle().setLeft(MIN_ARROW_DISTANCE_Y, Unit.PX);
			} else if (deltaY > bb.height - MIN_ARROW_DISTANCE_Y) {
				panel.getElement().getStyle().setLeft(deltaY - bb.height + MIN_ARROW_DISTANCE_Y, Unit.PX);
				arrow.getElement().getStyle().setRight(MIN_ARROW_DISTANCE_Y, Unit.PX);
			} else {
				arrow.getElement().getStyle().setLeft(deltaY, Unit.PX);
			}
			if (backgroundColor != null) {
				Util.setStyle(arrowForeground, "border-color", "transparent " + backgroundColor);
			}
			if (borderColor != null) {
				Util.setStyle(arrowBackground, "border-color", "transparent " + borderColor);
			}
			break;

		default:
			assert false : direction;
		}
	}

	private void setArrowDirection(Direction direction) {
		for (Direction dir : Direction.values()) {
			panel.removeStyleDependentName(dir.name());
		}
		panel.addStyleDependentName(direction.name());
	}

	public void setNextTo(Widget showNextTo) {
		assert showNextTo != null;
		this.showNextTo = showNextTo;
		// if (activationWidget == null) {
		// setActivationWidget(showNextTo);
		// }
		if (showNextTo != null && getShowMode().requiresActivationWidget && getActivationWidget() == null) {
			setActivationWidget(showNextTo);
		}
		if (ShowMode.show.equals(getShowMode())) {
			show();
		}
	}

	public Widget getNextTo() {
		return showNextTo;
	}

	public Widget getActivationWidget() {
		return activationWidget;
	}

	public void setActivationWidget(Widget activationWidget) {
		if (this.activationWidget == activationWidget) {
			return;
		}
		if (activationWidget == null && showNextTo != null) {
			activationWidget = showNextTo;
		}
		regMan.clear();
		this.activationWidget = activationWidget;
		if (activationWidget == null) {
			return;
		}

		if (showNextTo == null) {
			this.showNextTo = activationWidget;
		}

		setHandlers();
	}

	private void setHandlers() {
		regMan.clear();
		if (ShowMode.on_hover.equals(getShowMode())) {
			regMan.setDom(activationWidget, new MouseOverHandler() {

				@Override public void onMouseOver(MouseOverEvent event) {
					Log.trace(this, ".<init>.unfold.mouseOver...");
					event.stopPropagation();
					event.preventDefault();
					show();
				}
			}, MouseOverEvent.getType());
			regMan.setDom(activationWidget, new MouseOutHandler() {

				@Override public void onMouseOut(MouseOutEvent event) {
					Log.trace(this, ".<init>.unfold.mouseOut...");
					event.stopPropagation();
					event.preventDefault();
					hide();
				}

			}, MouseOutEvent.getType());
		}
		if (ShowMode.toggle_on_click.equals(getShowMode())) {
			regMan.setDom(activationWidget, new ClickHandler() {

				@Override public void onClick(ClickEvent event) {
					event.stopPropagation();
					event.preventDefault();
					if (isShowing())
						hide();
					else
						show();
				}
			}, ClickEvent.getType());
		}
	}

	private static interface Showable {
		void show();
	}

	public void show() {
		show(new Showable() {

			@Override public void show() {
				popup.showRelativeTo(showNextTo);
			}
		});
	}

	public void showToTheRight() {
		show(Util.getRight(showNextTo) + 8, showNextTo.getAbsoluteTop());
	}

	public void show(final int left, final int top) {
		Log.debug(this, ".show: left,top", left, top);
		show(new Showable() {

			@Override public void show() {
				popup.setPopupPosition(left, top);
				popup.show();
			}
		});
	}

	private void show(final Showable showable) {
		setIsShowing(true);
		Scheduler.get().scheduleFixedDelay(new RepeatingCommand() {

			@Override public boolean execute() {
				if (isShowing) {
					showable.show();
					Scheduler.get().scheduleDeferred(new ScheduledCommand() {

						@Override public void execute() {
							adjustArrow();
						}
					});
				}
				return false;
			}
		}, Math.max(1, getShowDelayed()));
	}

	public void hide() {
		setIsShowing(false);
		popup.hide();
	}

	public int getShowDelayed() {
		return showDelayed;
	}

	public void setShowDelayed(int showDelayed) {
		this.showDelayed = showDelayed;
	}

	public String getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
		Util.setStyle(panel, "backgroundColor", backgroundColor);
	}

	public String getBorderColor() {
		return borderColor;
	}

	public void setBorderColor(String borderColor) {
		this.borderColor = borderColor;
		Util.setStyle(panel, "borderColor", borderColor);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.user.client.ui.HasWidgets#add(com.google.gwt.user.client
	 * .ui.Widget)
	 */
	@Override public void add(Widget w) {
		content.add(w);
	}

	public void set(Widget w) {
		clear();
		add(w);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.user.client.ui.HasWidgets#clear()
	 */
	@Override public void clear() {
		content.clear();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.user.client.ui.HasWidgets#iterator()
	 */
	@Override public Iterator<Widget> iterator() {
		return content.iterator();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.user.client.ui.HasWidgets#remove(com.google.gwt.user.client
	 * .ui.Widget)
	 */
	@Override public boolean remove(Widget w) {
		return content.remove(w);
	}

	public ShowMode getShowMode() {
		return showMode;
	}

	public void setShowMode(ShowMode showMode) {
		this.showMode = showMode;
		if (showMode.requiresActivationWidget && getActivationWidget() == null && getNextTo() != null) {
			setActivationWidget(showNextTo);
		}
		if (ShowMode.show.equals(showMode)) {
			// Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			//
			// @Override public void execute() {
			show();
			// }
			// });
		}
		if (ShowMode.hide.equals(showMode)) {
			// Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			//
			// @Override public void execute() {
			hide();
			// }
			// });
		}
	}

	public boolean isShowing() {
		return isShowing && isAttached() && isVisible();
	}

	private void setIsShowing(boolean isShowing) {
		boolean changed = isShowing != isShowing();
		this.isShowing = isShowing;
		if (changed && visibilityChanged != null) {
			visibilityChanged.visibilityChanged(isShowing);
		}
	}

}
