/**
 * 
 */
package gwt.tools.client.ui.common;

import gwt.tools.client.HandlerRegistrationManager;
import gwt.tools.client.Util;
import gwt.tools.shared.Log;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class CustomHint extends PopupPanel {

	private Widget showNextTo;
	private Widget activationWidget;
	private int showDelayed = 0;
	private HandlerRegistrationManager regMan = new HandlerRegistrationManager();
	private boolean showing = false;
	protected boolean init = true;

	public CustomHint() {
		super();
		addStyleName("custom-hint");
		setAnimationEnabled(false);
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {

			@Override public void execute() {
				init = false;
				removeFromParent();
				// animation does not work with Chrome (continuously fades in and out)
				//				setAnimationEnabled(true);
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.user.client.ui.Widget#onLoad()
	 */
	@Override protected void onLoad() {
		super.onLoad();
		if (init && isAttached()) {
			try {
				// keep it removed at any time during initialization
				removeFromParent();
			} catch (Throwable t) {
			}
		}
	}

	public CustomHint(String html, String styleName, UIObject showNextTo) {
		this(SafeHtmlUtils.fromTrustedString(html), styleName, showNextTo);
	}

	public CustomHint(SafeHtml html, String styleName, UIObject showNextTo) {
		this(new HTMLPanel(html), styleName, showNextTo);
	}

	public CustomHint(Widget hintContent) {
		this(hintContent, null);
	}

	public CustomHint(Widget hintContent, String styleName) {
		this(hintContent, styleName, null);
	}

	public CustomHint(Widget hintContent, String styleName, UIObject showNextTo) {
		super(true, false);
		setWidget(hintContent);
		addStyleName("custom-hint");
		if (Util.notEmpty(styleName)) {
			addStyleName(styleName);
		}
		if (showNextTo != null) {
			showRelativeTo(showNextTo);
		}
	}

	public Widget getNextTo() {
		return showNextTo;
	}

	public void setNextTo(Widget showNextTo) {
		assert showNextTo != null;
		this.showNextTo = showNextTo;
		if (activationWidget == null) {
			setActivationWidget(showNextTo);
		}
	}

	public Widget getActivationWidget() {
		return activationWidget;
	}

	public void setActivationWidget(Widget activationWidget) {
		if (this.activationWidget == activationWidget) {
			return;
		}
		if (activationWidget == null && showNextTo != null) {
			activationWidget = showNextTo;
		}
		regMan.clear();
		this.activationWidget = activationWidget;
		if (activationWidget == null) {
			return;
		}

		if (showNextTo == null) {
			this.showNextTo = activationWidget;
		}

		setHandlers();
	}

	private void setHandlers() {
		regMan.clear();
		regMan.setDom(activationWidget, new MouseOverHandler() {

			@Override public void onMouseOver(MouseOverEvent event) {
				Log.trace(this, ".<init>.unfold.mouseOver...");
				event.stopPropagation();
				event.preventDefault();
				setPopupPositionAndShow(Util.getRight(showNextTo) + 8, showNextTo.getAbsoluteTop());
			}
		}, MouseOverEvent.getType());
		regMan.setDom(activationWidget, new MouseOutHandler() {

			@Override public void onMouseOut(MouseOutEvent event) {
				Log.trace(this, ".<init>.unfold.mouseOut...");
				event.stopPropagation();
				event.preventDefault();
				hide();
			}
		}, MouseOutEvent.getType());
	}

	public void setEnabled(boolean enabled) {
		regMan.clear();
		if (enabled) {
			setHandlers();
		} else {
			hide();
		}
	}

	public boolean isEnabled() {
		return !regMan.isEmpty();
	}

	//	/*
	//	 * (non-Javadoc)
	//	 * 
	//	 * @see com.google.gwt.user.client.ui.PopupPanel#setVisible(boolean)
	//	 */
	//	@Override public void setVisible(boolean visible) {
	//		if (visible) {
	//			showRelativeTo(showNextTo);
	//		} else {
	//			hide();
	//		}
	//	}

	//	/*
	//	 * (non-Javadoc)
	//	 * 
	//	 * @see com.google.gwt.user.client.ui.PopupPanel#setVisible(boolean)
	//	 */
	//	@Override public void setVisible(boolean visible) {
	//		Util.setVisible(this, visible);
	//	}

	/**
	 * @param right
	 * @param absoluteTop
	 */
	public void setPopupPositionAndShow(final int left, final int top) {
		Log.debug(this, ".setPopupPositionAndShow: left,top", left, top);
		//		if (isAttached()) {
		//			removeFromParent();
		//		}
		showing = true;
		Scheduler.get().scheduleFixedDelay(new RepeatingCommand() {

			@Override public boolean execute() {
				if (showing) {
					setPopupPosition(left, top);
					show();
				}
				return false;
			}
		}, Math.max(1, getShowDelayed()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.user.client.ui.PopupPanel#hide()
	 */
	@Override public void hide() {
		showing = false;
		super.hide();
	}

	public int getShowDelayed() {
		return showDelayed;
	}

	public void setShowDelayed(int showDelayed) {
		this.showDelayed = showDelayed;
	}
}
