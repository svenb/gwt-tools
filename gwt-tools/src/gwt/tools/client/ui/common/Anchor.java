/**
 * 
 */
package gwt.tools.client.ui.common;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.HasText;

/**
 * ClickAnchor allows you to add a ClickHandler right on creation. Nothing else.
 * 
 * @author Sven Buschbeck
 * 
 */
public class Anchor extends com.google.gwt.user.client.ui.Anchor implements HasText {

	public Anchor() {
		super(true);
		setStylePrimaryName("sb-Anchor");
	}

	public Anchor(ClickHandler clickHandler) {
		this("", clickHandler);
	}

	public Anchor(String text, ClickHandler clickHandler) {
		this(text);
		addClickHandler(clickHandler);
	}

	public Anchor(SafeHtml html, ClickHandler clickHandler) {
		this(html);
		addClickHandler(clickHandler);
	}

	public Anchor(String text) {
		super(text);
		setStylePrimaryName("sb-Anchor");
	}

	public Anchor(SafeHtml html) {
		super(html);
		setStylePrimaryName("sb-Anchor");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.user.client.ui.FocusWidget#setEnabled(boolean)
	 */
	@Override public void setEnabled(boolean enabled) {
		//		System.out.println("ClickAnchor.setEnabled(" + enabled + ")");
		super.setEnabled(enabled);
		setStyleDependentName("disabled", !enabled);
		// the super method those not hinder that the click event gets fired. :(
		if (isAttached()) {
			onDetach();
			if (enabled) {
				sinkEvents(Event.ONCLICK);
			} else {
				unsinkEvents(Event.ONCLICK);
			}
			onAttach();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.user.client.ui.Widget#onLoad()
	 */
	@Override protected void onLoad() {
		super.onLoad();
		if (isEnabled()) {
			sinkEvents(Event.ONCLICK);
		} else {
			unsinkEvents(Event.ONCLICK);
		}
	}

}
