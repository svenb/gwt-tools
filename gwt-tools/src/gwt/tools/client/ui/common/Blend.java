/**
 * 
 */
package gwt.tools.client.ui.common;

import gwt.tools.client.Util;
import gwt.tools.shared.Log;
import gwt.tools.shared.Log.FormatOnDemand;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * A widget that overlays a given other one to indicate that the widget is
 * currently inactive/busy/can not be used. The overlay can have a label or
 * further, additional widgets.
 * 
 * @author Sven Buschbeck
 * 
 */
public class Blend extends PopupPanel {

	private final Widget overlayedWidget;

	public Blend(Widget overlayedWidget, String label) {
		this(overlayedWidget, new Label(label));
	}

	public Blend(Widget overlayedWidget, String label, Widget... widgets) {
		// this(overlayedWidget, new Label(label), widgets);
		this(overlayedWidget, Util.union(new Label(label), widgets));
	}

	public Blend(Widget overlayedWidget, Widget... widgets) {
		super(false, false);
		this.overlayedWidget = overlayedWidget;
		setGlassEnabled(true);
		setStyleName("gt-blend");
		if (Util.size(widgets) == 1) {
			setWidget(widgets[0]);
		} else {
			VerticalFlowPanel panel = new VerticalFlowPanel();
			for (Widget widget : widgets) {
				panel.add(widget);
			}
			setWidget(panel);
		}
		show();
	}

	public void show() {
		final int x = overlayedWidget.getAbsoluteLeft();
		final int y = overlayedWidget.getAbsoluteTop();
		final int width = overlayedWidget.getOffsetWidth();
		final int height = overlayedWidget.getOffsetHeight();

		setVisible(false);
		super.show();
		setPopupPosition(x + width / 2 - getOffsetWidth() / 2, y + height / 2 - getOffsetHeight() / 2);
		positionGlass(x, y, width, height);
		setVisible(true);
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {

			@Override public void execute() {
				positionGlass(x, y, width, height);
			}
		});
	}

	private void positionGlass(final int x, final int y, final int width, final int height) {
		Style glassStyle = getGlassElement().getStyle();
		Log.debug(this, ".show: bb", new FormatOnDemand() {

			@Override public String format() {
				return "x=" + x + "; y=" + y + "; w=" + width + "; h=" + height;
			}
		});
		glassStyle.setLeft(x, Unit.PX);
		glassStyle.setTop(y, Unit.PX);
		glassStyle.setWidth(width, Unit.PX);
		glassStyle.setHeight(height, Unit.PX);
		// glassStyle.setProperty("left", x + "px !important");
		// glassStyle.setProperty("top", y + "px !important");
		// glassStyle.setProperty("width", width + "px !important");
		// glassStyle.setProperty("height", height + "px !important");
	}

}
