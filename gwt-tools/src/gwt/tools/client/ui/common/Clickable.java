/**
 * 
 */
package gwt.tools.client.ui.common;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HasHandlers;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class Clickable implements IsWidget {

	/**
	 * 
	 */
	private static final String CSS_CLASS_NAME = "clickable";
	private static final String CSS_CLASS_NAME_NOT = "not-clickable";
	private final Widget clickableWidget;
	private final String titleEnabled;
	private final String titleDisabled;

	public Clickable(Widget clickableWidget) {
		this(clickableWidget, (String) null);
	}

	public Clickable(HasHandlers clickableWidget) {
		//		assert clickableWidget instanceof Widget : "The given parameter needs to be a widget. parameter="
		//			+ clickableWidget;
		this((Widget) clickableWidget);
	}

	public Clickable(Widget clickableWidget, ClickHandler clickHandler) {
		this(clickableWidget, null, clickHandler);
	}

	public Clickable(Widget clickableWidget, final boolean enabled) {
		this(clickableWidget, null, enabled, null);
	}

	public Clickable(Widget clickableWidget, String title) {
		this(clickableWidget, title, null);
	}

	public Clickable(Widget clickableWidget, String title, ClickHandler clickHandler) {
		this(clickableWidget, title, true, clickHandler);
	}

	public Clickable(Widget clickableWidget, String title, final boolean enabled, ClickHandler clickHandler) {
		this(clickableWidget, title, title, enabled, clickHandler);
	}

	public Clickable(Widget clickableWidget, String titleEnabled, String titleDisabled, final boolean enabled,
		ClickHandler clickHandler) {
		super();
		assert clickableWidget != null;
		this.clickableWidget = clickableWidget;
		//		initWidget(clickableWidget);
		this.titleEnabled = titleEnabled;
		this.titleDisabled = titleDisabled;
		addClickHandler(clickHandler);
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {

			@Override public void execute() {
				setEnabled(enabled);
			}
		});
	}

	public boolean isEnabled() {
		return getWidget().getStyleName().contains(CSS_CLASS_NAME);
	}

	public void setEnabled(boolean enabled) {
		Widget widget = getWidget();
		widget.setStyleName(CSS_CLASS_NAME, enabled);
		widget.setStyleName(CSS_CLASS_NAME_NOT, !enabled);
		if (enabled) {
			widget.sinkEvents(Event.ONCLICK);
			if (titleEnabled != null) {
				getWidget().setTitle(titleEnabled);
			}
		} else {
			widget.unsinkEvents(Event.ONCLICK);
			if (titleDisabled != null) {
				getWidget().setTitle(titleDisabled);
			}
		}
	}

	/**
	 * @param clickHandler
	 */
	public void addClickHandler(ClickHandler clickHandler) {
		if (clickHandler != null) {
			getWidget().addHandler(clickHandler, ClickEvent.getType());
		}
	}

	public void setTitle(String title) {
		getWidget().setTitle(title);
	}

	public Widget getWidget() {
		return clickableWidget;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.user.client.ui.IsWidget#asWidget()
	 */
	@Override public Widget asWidget() {
		return getWidget();
	}

	//	private Widget getWidget() {
	//		return clickableWidget;
	//	}

}
