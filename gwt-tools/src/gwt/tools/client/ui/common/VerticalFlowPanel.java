package gwt.tools.client.ui.common;

public class VerticalFlowPanel extends CustomFlowPanel {

	/**
	 * 
	 */
	public static final String DISPLAY_STYLE = "block";

	@Override protected String getFlowStyle() {
		return DISPLAY_STYLE;
	}

}
