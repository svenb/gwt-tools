/**
 * 
 */
package gwt.tools.client.ui.common;

import gwt.tools.shared.Log;
import gwt.tools.shared.Util;
import gwt.tools.shared.Util.Counter;
import gwt.tools.shared.dto.KeyValuePair;

import java.util.Collection;
import java.util.HashMap;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.HTML;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class WordCloud extends HTML {

	public static final String stopWords = "^(i|me|my|myself|we|us|our|ours|ourselves|you|your|yours|yourself|yourselves|he|him|his|himself|she|her|hers|herself|it|its|itself|they|them|their|theirs|themselves|what|which|who|whom|whose|this|that|these|those|am|is|are|was|were|be|been|being|have|has|had|having|do|does|did|doing|will|would|should|can|could|ought|i'm|you're|he's|she's|it's|we're|they're|i've|you've|we've|they've|i'd|you'd|he'd|she'd|we'd|they'd|i'll|you'll|he'll|she'll|we'll|they'll|isn't|aren't|wasn't|weren't|hasn't|haven't|hadn't|doesn't|don't|didn't|won't|wouldn't|shan't|shouldn't|can't|cannot|couldn't|mustn't|let's|that's|who's|what's|here's|there's|when's|where's|why's|how's|a|an|the|and|but|if|or|because|as|until|while|of|at|by|for|with|about|against|between|into|through|during|before|after|above|below|to|from|up|upon|down|in|out|on|off|over|under|again|further|then|once|here|there|when|where|why|how|all|any|both|each|few|more|most|other|some|such|no|nor|not|only|own|same|so|than|too|very|say|says|said|shall|st|nd|rd|th)$";
	public static final String punctuation = "[!\\\"&()*+,-\\.\\/:;<=>?\\[\\\\\\]^`\\{|\\}~]+";
	public static final String wordSeparators = "[\\s\u3031-\u3035\u309b\u309c\u30a0\u30fc\uff70]+";
	public static final String discard = "^(@|https?:)";
	public static final String htmlTags = "(<[^>]*?>|<script.*?<\\/script>|<style.*?<\\/style>|<head.*?><\\/head>)";
	public static final String matchTwitter = "^https?:\\/\\/([^\\.]*\\.)?twitter\\.com";

	public static interface CustomRotation {
		double rotationAngle(String word, Integer count, int maxCount);
	}

	private static int idCount = 0;
	//	private Collection<String> allWords; don't keep a ref to save memory
	private int maxWordsLength = 30;
	private int maxWordsCount = 40;
	private int maxFontSize = 40;
	private int minFontSize = 10;
	private int random = 2;
	private double verticalProbability = .25;

	private Counter<String> wordCount = new Counter<String>();
	private JsArray<JavaScriptObject> jsWordMap;
	private boolean upToDate = false;

	public CustomRotation customRotation;

	public WordCloud() {
		super();
		addStyleName("word-cloud");
		//		getElement().getStyle().setBorderStyle(BorderStyle.SOLID);
		//		getElement().getStyle().setBorderWidth(3, Unit.PX);
		//		getElement().getStyle().setBorderColor("#d08040");

		//		assert libsReady() : "This widget depends on two JavaScript libraries: http://d3js.org/d3.v2.min.js and https://raw.github.com/jasondavies/d3-cloud/master/d3.layout.cloud.js. Both need to be loaded before your application.";
	}

	public WordCloud(Collection<String> texts, int width, int height) {
		this();
		setPixelSize(width, height);
		setWords(texts);
	}

	public WordCloud(Collection<String> texts) {
		this();
		setWords(texts);
	}

	public String getWords() {
		//		return Util.join(allWords, " ");
		return Util.join(wordCount.getObjects(), " ");
	}

	public void setWords(String allWords) {

	}

	public void setWords(String... allWords) {
		setWords(Util.createList(allWords));
	}

	public void setWords(Collection<String> allWords) {
		//		this.allWords = allWords;// = Util.removeAll(words, ".", ",", "!", "?", ";", "for");
		HashMap<String, String> originalCapitalization = new HashMap<String, String>();
		for (String words : allWords) {
			//			String[] singleWords = words.replaceAll(punctuation, "").split(" ");
			String[] singleWords = words.replaceAll(punctuation, "").split(wordSeparators);
			for (String word : singleWords) {
				// TODO_ keep word in original writing from (capitalization) - but how do decide what is "correct" if word appears twice with different capitalization? we just keep the last one hoping we are lucky...
				String originalWord = word.trim();
				String cleanWord = originalWord.toLowerCase();
				if (cleanWord.length() < 2 || cleanWord.matches(discard) || cleanWord.matches(stopWords)) {
					//				if (cleanWord.length() < 2) {
					//				Log.debug(this, ".setWords: cleanWord filtered out", cleanWord);
					continue;
				}
				//			if (cleanWord.matches(discard) ) {
				//				Log.debug(this, ".setWords: cleanWord discarded", cleanWord, originalCapitalization);
				//				continue;
				//			}
				//			if (!cleanWord.matches(stopWords)) {
				//				Log.debug(this, ".setWords: cleanWord is a stop word", cleanWord, originalCapitalization);
				//				continue;
				//			}
				cleanWord = Util.shortenString(cleanWord, maxWordsLength);
				wordCount.add(cleanWord);
				originalCapitalization.put(cleanWord, originalWord);
			}
		}

		Log.debug(this, ".setWords: #words", wordCount.size());
		final int maxCountInt = wordCount.getMaxCount();
		final double maxCount = maxCountInt;
		jsWordMap = initJsWordMap();
		final double minSize = getMinFontSize();
		final double maxSize = getMaxFontSize();
		final boolean random = getFontSizeRandom() > 0;
		final double randomness = getFontSizeRandom();
		for (KeyValuePair<String, Integer> word : wordCount.getMappings(true, getMaxWordCount())) {
			String wordLabel = originalCapitalization.containsKey(word.key) ? originalCapitalization.get(word.key)
				: word.key;
			int rotation = (int) ((customRotation != null) ? customRotation.rotationAngle(wordLabel, word.value,
				maxCountInt) : (Math.random() < verticalProbability) ? -90 : 0);
			int size = (int) (minSize + ((word.value / maxCount) * (maxSize - minSize)));
			if (random) {
				size += Math.round((Math.random() - .5) * randomness);
			}
			addWord(wordLabel, size, rotation, jsWordMap);
		}
		upToDate = false;
		updateWordCloud();
		Log.debug(this, ".setWords: final HTML", getHTML());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.user.client.ui.Widget#onLoad()
	 */
	@Override protected void onLoad() {
		super.onLoad();
		updateWordCloud();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	public WordCloud clone() {
		WordCloud clone = new WordCloud();
		clone.customRotation = customRotation;
		clone.maxFontSize = maxFontSize;
		clone.maxWordsCount = maxWordsCount;
		clone.maxWordsLength = maxWordsLength;
		clone.minFontSize = minFontSize;
		clone.verticalProbability = verticalProbability;
		clone.wordCount = wordCount.clone();
		clone.copyFrom(this);
		return clone;
	}

	private void updateWordCloud() {
		Element element = getElement();
		int clientWidth = (element == null) ? 0 : element.getClientWidth();
		int clientHeight = (element == null) ? 0 : element.getClientHeight();
		Log.debug(this, ".updateWordCloud: width,height,#words", clientWidth, clientHeight, wordCount.size());
		if (Util.notEmpty(wordCount.getCountMap()) && jsWordMap != null && clientWidth > 0 && clientHeight > 0
			&& (!upToDate || getElement().getChildCount() < 1)) {
			upToDate = true;
			//			gwt.tools.client.Util.removeAllChildren(element);
			element.setInnerHTML("");
			String id = element.getId();
			if (Util.isEmpty(id)) {
				element.setId(id = this.getClass().getName() + idCount++);
			}
			Log.debug(this, ".updateWordCloud: activityId", id);
			try {
				drawCloud(clientWidth, clientHeight, verticalProbability, id, jsWordMap);
			} catch (Throwable t) {
				Log.warn(this, ".updateWordCloud: calling drawCloud", t);
			}
		}
	}

	private native JsArray<JavaScriptObject> initJsWordMap()/*-{
		return new Array();
	}-*/;

	private native void addWord(String word, int wordSize, int wordRotation, JsArray<JavaScriptObject> jsWordMap)/*-{
		jsWordMap.push({
			text : word,
			size : wordSize,
			rotation : wordRotation
		});
	}-*/;

	private native JavaScriptObject drawCloud(int width, int height, final double verticalProbability,
		String selectorOfElementToAddTo, JsArray<JavaScriptObject> jsWordMap)/*-{
		//		var fontSize = d3.scale.log().range([ 10, 100 ]);
		//
		//		var layout = cloud().size([ 960, 600 ]).timeInterval(10)
		//		//		      .text(function(d) { return d.key; })
		//		.font("Impact").fontSize(function(d) {
		//			return fontSize(+d.value);
		//		}).rotate(function(d) {
		//			return ~~(Math.random() * 5) * 30 - 60;
		//		}).padding(1).on("word", progress).on("end", draw).words(words).start();

		//		window.alert("1: " + jsWordMap);
		$wnd.d3.layout.cloud().size([ width, height ]).words(jsWordMap).rotate(
				function(d) {
					//return ~~(Math.random() * 2) * 90;
					//return (Math.random() < verticalProbability) ? -90 : 0;
					return d.rotation;
				}).fontSize(function(d) {
			return d.size;
		}).on(
				"end",
				function(parsedWords) { // draw function
					$wnd.d3.select(
							$wnd.document
									.getElementById(selectorOfElementToAddTo))
							.append("svg").attr("width", width).attr("height",
									height).append("g").attr(
									"transform",
									"translate(" + (width / 2) + ","
											+ (height / 2) + ")").selectAll(
									"text").data(parsedWords).enter().append(
									"text").style("font-size", function(d) {
								return d.size + "px";
							}).attr("text-anchor", "middle").attr(
									"transform",
									function(d) {
										return "translate(" + [ d.x, d.y ]
												+ ")rotate(" + d.rotate + ")";
									}).text(function(d) {
								return d.text;
							});
				}).start();
	}-*/;

	public int getMaxWordCount() {
		return maxWordsCount;
	}

	public void setMaxWordCount(int maxWords) {
		this.maxWordsCount = maxWords;
	}

	public int getMaxFontSize() {
		return maxFontSize;
	}

	public void setMaxFontSize(int maxFontSize) {
		this.maxFontSize = maxFontSize;
	}

	private native boolean libsReady()/*-{
		return $wnd.d3 != null && $wnd.d3.layout != null
				&& $wnd.d3.layout.cloud != null;
	}-*/;

	/**
	 * @return
	 */
	public boolean isEmpty() {
		return wordCount.isEmpty();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.user.client.ui.UIObject#setVisible(boolean)
	 */
	@Override public void setVisible(boolean visible) {
		super.setVisible(visible);
		if (visible) {
			Scheduler.get().scheduleDeferred(new ScheduledCommand() {

				@Override public void execute() {
					updateWordCloud();
				}
			});
		}
	}

	public int getMinFontSize() {
		return minFontSize;
	}

	public void setMinFontSize(int minFontSize) {
		this.minFontSize = minFontSize;
	}

	public double getVerticalProbability() {
		return verticalProbability;
	}

	public void setVerticalProbability(double verticalProbability) {
		this.verticalProbability = verticalProbability;
	}

	/**
	 * @param copyFromWordCloud
	 */
	public void copyFrom(final WordCloud copyFromWordCloud) {
		// getInnerHtml returned "" without waiting a bit here
		//		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
		//
		//			@Override public void execute() {
		jsWordMap = copyFromWordCloud.jsWordMap;
		wordCount = copyFromWordCloud.wordCount;
		//				copy(getElement(), copyFromWordCloud.getElement());
		//				getElement().setInnerHTML(copyFromWordCloud.getElement().getInnerHTML());
		//		Log.debug(this, ".copy: source.innerHtml", copyFromWordCloud.getHTML());
		setHTML(copyFromWordCloud.getHTML());
		//		Log.debug(this, ".copy: me.innerHtml", getElement().getInnerHTML());
		upToDate = true;
		//			}
		//		});
	}

	private native void copy(Element source, Element target)/*-{
		otherCloud.innerHtml = me.innerHtml;
	}-*/;

	/**
	 * Up to which amount the font size is random
	 * 
	 * @return 0=not random; 1= +/- 1 px
	 */
	public int getFontSizeRandom() {
		return random;
	}

	/**
	 * Up to which amount the font size is random
	 * 
	 * @param random
	 *            0 mean no randomness; 1= +/- 1 px, ....
	 */
	public void setFontSizeRandom(int random) {
		this.random = random;
	}

}
