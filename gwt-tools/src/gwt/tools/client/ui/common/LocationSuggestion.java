/**
 * 
 */
package gwt.tools.client.ui.common;

import com.google.gwt.user.client.ui.SuggestOracle;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class LocationSuggestion implements SuggestOracle.Suggestion {


	public final String locationLabel;
	public final Double latitude;
	public final Double longitude;

	public LocationSuggestion(String locationLabel, Double latitude, Double longitude) {
		this.locationLabel = locationLabel;
		this.longitude = longitude;
		this.latitude = latitude;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.user.client.ui.SuggestOracle.Suggestion#getDisplayString()
	 */
	@Override public String getDisplayString() {
		return locationLabel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.user.client.ui.SuggestOracle.Suggestion#getReplacementString
	 * ()
	 */
	@Override public String getReplacementString() {
		return locationLabel;
	}
}
