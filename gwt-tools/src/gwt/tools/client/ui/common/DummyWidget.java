/**
 * 
 */
package gwt.tools.client.ui.common;

import com.google.gwt.user.client.ui.HTML;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class DummyWidget extends HTML {

	public DummyWidget() {
		super();
		setVisible(false);
	}

}
