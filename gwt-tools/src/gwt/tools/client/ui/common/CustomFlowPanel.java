package gwt.tools.client.ui.common;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;

public abstract class CustomFlowPanel extends FlowPanel {

	protected abstract String getFlowStyle();

	@Override public void add(Widget widget) {
		setFlow(widget);
		super.add(widget);
	}

	protected void setFlow(Widget widget) {
		widget.getElement().getStyle().setProperty("display", getFlowStyle());
		widget.addStyleName("flow-" + getFlowStyle());
	}
}
