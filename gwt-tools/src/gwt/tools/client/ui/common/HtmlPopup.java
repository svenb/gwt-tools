/**
 * 
 */
package gwt.tools.client.ui.common;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.UIObject;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class HtmlPopup extends PopupPanel {

	public HtmlPopup(String html) {
		this(SafeHtmlUtils.fromString(html));
	}

	public HtmlPopup(String html, UIObject showNextTo) {
		this(SafeHtmlUtils.fromTrustedString(html));
		showRelativeTo(showNextTo);
	}

	public HtmlPopup(SafeHtml html, String styleName) {
		this(html);
		addStyleName(styleName);
	}

	public HtmlPopup(String html, String styleName, UIObject showNextTo) {
		this(SafeHtmlUtils.fromTrustedString(html), styleName, showNextTo);
	}

	public HtmlPopup(SafeHtml html, String styleName, UIObject showNextTo) {
		this(html, styleName);
		showRelativeTo(showNextTo);
	}

	//	public HtmlPopup(SafeHtml html, String styleName, int x, int y, int width, int height) {
	//		this(html, x, y, width, height);
	//		addStyleName(styleName);
	//	}
	//
	//	public HtmlPopup(SafeHtml html, int x, int y, int width, int height) {
	//		this(html);
	//		setPopupPosition(x, y);
	//		setPixelSize(width, height);
	//	}

	public HtmlPopup(SafeHtml html, String styleName, ImageResource closeButton) {
		this(html, styleName, new Image(closeButton));
	}

	public HtmlPopup(SafeHtml html, String styleName, Image closeButton) {
		super(true, false);
		HTMLPanel htmlPanel = new HTMLPanel(html);
		htmlPanel.add(closeButton);
		new Clickable(closeButton, "Close.", new ClickHandler() {

			@Override public void onClick(ClickEvent event) {
				hide();
			}
		});
		setWidget(htmlPanel);
		addStyleName(styleName);
	}

	public HtmlPopup(SafeHtml html) {
		super(true, false);
		setWidget(new HTML(html));
	}

	/**
	 * @param right
	 * @param absoluteTop
	 */
	public void setPopupPositionAndShow(int left, int top) {
		setPopupPosition(left, top);
		show();
	}
}
