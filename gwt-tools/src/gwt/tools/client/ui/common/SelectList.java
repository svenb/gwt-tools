/**
 * 
 */
package gwt.tools.client.ui.common;

import gwt.tools.client.Util;
import gwt.tools.shared.Util.Converter;

import java.util.LinkedList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.TableRowElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.RowHoverEvent;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class SelectList<EntityType> extends Composite {

	private static SelectListUiBinder uiBinder = GWT.create(SelectListUiBinder.class);

	interface SelectListUiBinder extends UiBinder<Widget, SelectList> {

	}

	private LinkedList<Widget> actions = new LinkedList<Widget>();

	public static interface ContextActionHandler<EntityType> {
		void handleAction(EntityType target);
	}

	public static interface ActionHandler<EntityType> {
		void handleAction();
	}

	@UiField VerticalFlowPanel panel;
	@UiField CellTable<EntityType> list;
	@UiField LinkList actionLinks;
	@UiField IconList icons;

	//	private CellTable<EntityType> list;
	private SingleSelectionModel<EntityType> selectionModel;
	//	private HashMap<ActionHandler<?>, String> actionMap = new HashMap<SelectList.ActionHandler<?>, String>();
	private LinkList contextActionLinks = new LinkList();
	private ListDataProvider<EntityType> provider;

	private PopupPanel contextMenuPopup;
	private PopupPanel iconMenuPopup;
	//	protected TableRowElement hoveredRow;
	protected int hoveredEntityIndex = -1;

	public SelectList(Converter<EntityType, String> renderer) {
		this();
		setRenderer(renderer);
	}

	public SelectList() {
		initWidget(uiBinder.createAndBindUi(this));
		//		panel.setWidget(list = new CellTable<EntityType>());
		selectionModel = new SingleSelectionModel<EntityType>();
		list.setSelectionModel(selectionModel);
		provider = new ListDataProvider<EntityType>();
		provider.addDataDisplay(list);
		//		icons.setVisible(false);
		//		icons.getElement().getStyle().setPosition(Position.ABSOLUTE);
		iconMenuPopup = new PopupPanel(true, false);
		iconMenuPopup.setWidget(icons);
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {

			@Override public void onSelectionChange(SelectionChangeEvent event) {
				//				Log.debug(this, "init.selected: event.source", event.getSource());
				int rowIndex = provider.getList().indexOf(selectionModel.getSelectedObject());
				TableRowElement rowElement = list.getRowElement(rowIndex);
				showActions(rowElement.getAbsoluteBottom());
			}
		});
		list.addRowHoverHandler(new RowHoverEvent.Handler() {

			@Override public void onRowHover(RowHoverEvent event) {
				if (event.isUnHover()) {
					GWT.log(this + "init.list.row.unhover");
					hideIcons();
				} else {
					GWT.log(this + "init.list.row.hover. index=" + event.getHoveringRow().getSectionRowIndex());
					hoveredEntityIndex = event.getHoveringRow().getSectionRowIndex();
					showIcons(event.getHoveringRow());
				}
			}
		});
		//		list.addHandler(new MouseOutHandler() {
		//
		//			@Override public void onMouseOut(MouseOutEvent event) {
		//				hideIcons();
		//			}
		//		}, MouseOutEvent.getType());
	}

	public void addContextAction(String label, final ContextActionHandler<EntityType> handler) {
		contextActionLinks.addLink(label, new ClickHandler() {

			@Override public void onClick(ClickEvent event) {
				contextMenuPopup.hide();
				handler.handleAction(selectionModel.getSelectedObject());
			}
		});
	}

	public void addDeleteAction(String label) {
		addContextAction(label, new ContextActionHandler<EntityType>() {

			@Override public void handleAction(EntityType target) {
				contextMenuPopup.hide();
				provider.getList().remove(target);
			}
		});
	}

	public void addDeleteAction(Image deleteIcon) {
		icons.addIcon(deleteIcon, new ClickHandler() {

			@Override public void onClick(ClickEvent event) {
				if (hoveredEntityIndex > -1) {
					provider.getList().remove(hoveredEntityIndex);
					hideIcons();
				}
			}
		});
	}

	public void addDeleteAction(String label, Image deleteIcon) {
		addDeleteAction(label);
		addDeleteAction(deleteIcon);
	}

	public void addAction(String label, final ActionHandler<EntityType> handler) {
		addAction(SafeHtmlUtils.fromString(label), handler);
	}

	public void addAction(SafeHtml safeHtml, final ActionHandler<EntityType> handler) {
		actionLinks.addLink(safeHtml, new ClickHandler() {

			@Override public void onClick(ClickEvent event) {
				handler.handleAction();
			}
		});
	}

	private void showActions(int y) {
		if (getData().size() > 0 && Util.notEmpty(contextActionLinks)) {
			contextMenuPopup = new PopupPanel(true, true);
			contextMenuPopup.setWidget(contextActionLinks);
			contextMenuPopup.setPopupPosition(getAbsoluteLeft(), y);
			contextMenuPopup.show();
		}
	}

	private void showIcons(TableRowElement row) {
		if (getData().size() > 0 && row != null && Util.notEmpty(icons)) {
			GWT.log(this + ".showIcons: #icons=" + icons.getElement().getChildCount());
			//			if (iconMenuPopup == null) {
			//				iconMenuPopup = new PopupPanel(true, false);
			//				contextMenuPopup.setWidget(icons);
			//			}
			iconMenuPopup.setPopupPosition(row.getAbsoluteRight() - icons.getOffsetWidth(), row.getAbsoluteTop());
			iconMenuPopup.show();

			//			Util.setStyle(icons, "left", row.getAbsoluteTop(), Unit.PX);
			//			Util.setStyle(icons, "right", row.getAbsoluteRight(), Unit.PX);
			//			icons.setVisible(true);
			//			GWT.log(this + ".showIcons: icons=" + icons);
		}
	}

	private void hideIcons() {
		if (iconMenuPopup != null) {
			iconMenuPopup.hide();
		}
		//		hoveredRow = null;
		hoveredEntityIndex = -1;
	}

	public void setData(List<EntityType> data) {
		provider.setList(data);
	}

	public List<EntityType> getData() {
		return provider.getList();
	}

	public void setRenderer(final gwt.tools.shared.Util.Converter<EntityType, String> converter) {
		if (list.getColumnCount() > 0) {
			list.removeColumn(0);
		}
		list.addColumn(new SafeHtmlColumn<EntityType>() {

			@Override public SafeHtml getValue(EntityType entity) {
				return SafeHtmlUtils.fromTrustedString(converter.convert(entity));
			}
		});
		list.addColumn(new SafeHtmlColumn<EntityType>() {

			@Override public SafeHtml getValue(EntityType entity) {
				return SafeHtmlUtils.fromTrustedString("&nbsp;");
			}
		});
	}
}
