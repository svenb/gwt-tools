/**
 * 
 */
package gwt.tools.client.ui.common;

import java.util.LinkedList;

import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class SuggestLocationBox extends SuggestBox {

	private static SuggestLocationOracle oracle = new SuggestLocationOracle();

	public SuggestLocationBox() {
		super(oracle);
	}

	//	/*
	//	 * (non-Javadoc)
	//	 * 
	//	 * @see com.google.gwt.user.client.ui.Widget#onLoad()
	//	 */
	//	@Override protected void onLoad() {
	//		super.onLoad();
	//		SuggestionDisplay suggestionDisplay = getSuggestionDisplay();
	//		if (suggestionDisplay instanceof IsWidget) {
	//			Util.setStyle((IsWidget) suggestionDisplay, "maxWidth", getOffsetWidth(), Unit.PX);
	//		}
	//
	//	}

	public HandlerRegistration addSuggestionHandler(SelectionHandler<LocationSuggestion> handler) {
		return super
			.addSelectionHandler((SelectionHandler<Suggestion>) ((SelectionHandler<? extends Suggestion>) handler));
	}

	public LinkedList<Suggestion> getLatestSuggestions() {
		return oracle.latest;
	}

}
