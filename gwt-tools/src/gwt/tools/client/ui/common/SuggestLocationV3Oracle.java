package gwt.tools.client.ui.common;

import java.util.LinkedList;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.user.client.ui.SuggestOracle;
import com.google.maps.gwt.client.Geocoder;
import com.google.maps.gwt.client.GeocoderRequest;
import com.google.maps.gwt.client.GeocoderResult;
import com.google.maps.gwt.client.GeocoderStatus;

public class SuggestLocationV3Oracle extends SuggestOracle {

	private final Geocoder geocoder;
	public LinkedList<LocationSuggestion> latest;

	public SuggestLocationV3Oracle() {
		Geocoder temp = null;
		try {
			temp = Geocoder.create();
		} catch (Exception e) {
			// fails if there is no Internet connection available. 
		}
		geocoder = temp;
	}
	
	@Override
	public void requestSuggestions(final Request request, final Callback callback) {
		// this is the string, the user has typed so far
		String addressQuery = request.getQuery();
		// look up suggestions, only if at least 2 letters have been typed
		if (addressQuery.length() > 2) {
			GeocoderRequest geoCoderRequest = GeocoderRequest.create();
			geoCoderRequest.setAddress(addressQuery);
			geocoder.geocode(geoCoderRequest, new Geocoder.Callback(){

				@Override
				public void handle(JsArray<GeocoderResult> locations, GeocoderStatus b) {
					LinkedList<LocationSuggestion> suggestions = new LinkedList<LocationSuggestion>();
					for (int x = 0; x < locations.length(); x++) {
						GeocoderResult location = locations.get(x);
						suggestions.add(new LocationSuggestion(location.getFormattedAddress(), location.getGeometry().getLocation().lat(), location.getGeometry().getLocation().lng()));
					}
					Response response = new Response(suggestions);
					latest = suggestions;
					callback.onSuggestionsReady(request, response);
				}
			});
		}
	}

}
