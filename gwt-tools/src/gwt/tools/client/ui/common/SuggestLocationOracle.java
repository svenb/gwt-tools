/**
 * 
 */
package gwt.tools.client.ui.common;

import gwt.tools.shared.Util;

import java.util.LinkedList;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.user.client.ui.SuggestOracle;
import com.google.maps.gwt.client.Geocoder;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class SuggestLocationOracle extends SuggestOracle {

	// this instance is needed, to call the getLocations-Service
	private final Geocoder geocoder;
	public LinkedList<Suggestion> latest;

	public SuggestLocationOracle() {
		//		geocoder = new Geocoder();
		geocoder = new Geocoder();
	}

	@Override public void requestSuggestions(final Request request, final Callback callback) {
		// this is the string, the user has typed so far
		String addressQuery = request.getQuery();
		// look up for suggestions, only if at least 2 letters have been typed
		if (addressQuery.length() > 2) {
			// old v2 
			//			geocoder.getLocations(addressQuery, new LocationCallback() {
			//
			//				@Override public void onFailure(int statusCode) {
			//					// do nothing
			//				}
			//
			//				@Override public void onSuccess(JsArray<Placemark> places) {
			//					// create an oracle response from the places, found by the
			//					// getLocations-Service
			//					Collection<Suggestion> result = new LinkedList<Suggestion>();
			//					for (int i = 0; i < places.length(); i++) {
			//						String address = places.get(i).getAddress();
			//						AddressSuggestion newSuggestion = new AddressSuggestion(address);
			//						result.add(newSuggestion);
			//					}
			//					Response response = new Response(result);
			//					callback.onSuggestionsReady(request, response);
			//				}
			//
			//			});

//			GeocoderRequest geocoderRequest = GeocoderRequest.create();
//			geocoderRequest.setAddress(addressQuery);
//			geocoder.geocode(geocoderRequest, new Geocoder.Callback() {
			geocoder.getLocations(addressQuery, new LocationCallback() {
				
				@Override
				public void onSuccess(JsArray<Placemark> locations) {
					LinkedList<Suggestion> suggestions = new LinkedList<Suggestion>();
					for (int x = 0; x < locations.length(); x++) {
						suggestions.add(new LocationSuggestion(locations.get(x).getFormattedAddress()));
					}
					Response response = new Response(suggestions);
					latest = suggestions;
					callback.onSuggestionsReady(request, response);
				}
				
				@Override
				public void onFailure(int statusCode) {
					// TODO Auto-generated method stub
				}
			});

		} else {
			Response response = new Response(Util.<Suggestion> createList());
			callback.onSuggestionsReady(request, response);
		}

	}
}
