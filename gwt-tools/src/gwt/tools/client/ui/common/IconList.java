/**
 * 
 */
package gwt.tools.client.ui.common;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class IconList extends Composite { // implements Iterable<Widget> {

	private static IconListUiBinder uiBinder = GWT.create(IconListUiBinder.class);

	@UiTemplate("LinkList.ui.xml") interface IconListUiBinder extends UiBinder<Widget, IconList> {
	}

	@UiField VerticalFlowPanel panel;

	public IconList() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	public void addIcon(String imageUrl, ClickHandler handler) {
		addIcon(new Image(imageUrl), handler);
	}

	public void addIcon(ImageResource image, ClickHandler handler) {
		addIcon(new Image(image), handler);
	}

	public void addIcon(Image image, ClickHandler handler) {
		panel.add(new Clickable(image, handler));
	}

	public void clear() {
		panel.clear();
	}

	//	/*
	//	 * (non-Javadoc)
	//	 * 
	//	 * @see java.lang.Iterable#iterator()
	//	 */
	//	@Override public Iterator<Widget> iterator() {
	//		return panel.iterator();
	//	}
}
