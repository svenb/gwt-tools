package gwt.tools.client.ui.common;

public class HorizontalFlowPanel extends CustomFlowPanel {

	/**
	 * 
	 */
	public static final String DISPLAY_STYLE = "inline-block";

	@Override protected String getFlowStyle() {
		return DISPLAY_STYLE;
	}

}
