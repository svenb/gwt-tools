/**
 * 
 */
package gwt.tools.client.ui.common;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.HTMLPanel;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class AnchorFlowPanel extends HTMLPanel implements HasClickHandlers {

	public AnchorFlowPanel(String html) {
		super("a", html);
		addStyleName("clickable");
	}

	public AnchorFlowPanel() {
		this("");
	}

	public HandlerRegistration addClickHandler(ClickHandler handler) {
		return addDomHandler(handler, ClickEvent.getType());
	}

}
