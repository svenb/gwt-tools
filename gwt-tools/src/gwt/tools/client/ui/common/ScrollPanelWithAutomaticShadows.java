package gwt.tools.client.ui.common;

import java.util.Iterator;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Document;
import com.google.gwt.event.dom.client.ScrollEvent;
import com.google.gwt.event.dom.client.ScrollHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;

public class ScrollPanelWithAutomaticShadows extends Composite implements HasWidgets {

	private static ScrollPanelWithAutomaticShadowsUiBinder uiBinder = GWT
			.create(ScrollPanelWithAutomaticShadowsUiBinder.class);

	interface ScrollPanelWithAutomaticShadowsUiBinder extends UiBinder<Widget, ScrollPanelWithAutomaticShadows> {
	}
	
	@UiField HTMLPanel panel;
	@UiField HTMLPanel shadowTopPanel;
	@UiField ScrollPanel scroller;
	@UiField HTMLPanel content;
	@UiField HTMLPanel shadowBottomPanel;

	public ScrollPanelWithAutomaticShadows() {
		initWidget(uiBinder.createAndBindUi(this));
		scroller.addScrollHandler(new ScrollHandler() {
			
			@Override
			public void onScroll(ScrollEvent event) {
				update();
			}
		});
		Window.addResizeHandler(new ResizeHandler() {
			
			@Override
			public void onResize(ResizeEvent event) {
				update();
			}
		});
		update();
	}

	@Override
	protected void onLoad() {
		super.onLoad();
		update();
//		// the content will be added later
//		new ScheduledCommand() {
//			
//			int counter;
//			@Override
//			public void execute() {
//				System.out.println(counter);
//				if (!update() && counter++ < 10) Scheduler.get().scheduleDeferred(this);
//			}
//		}.execute();
	}
	
//	private boolean update() {
	private void update() {
//		if (scroller.getMaximumVerticalScrollPosition() < 1) return;
		int scrollPosition = scroller.getVerticalScrollPosition();
		shadowTopPanel.setVisible(scrollPosition > 1);
		shadowBottomPanel.setVisible(scrollPosition +1 < scroller.getMaximumVerticalScrollPosition());
//		System.out.println("ScrollPanelWithAutomaticShadows.update: pos=" + scrollPosition + "; maxScroll=" + scroller.getMaximumVerticalScrollPosition());
//		return scroller.getMaximumVerticalScrollPosition() > 0;
	}

	@Override
	public void add(Widget w) {
		content.add(w);
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			
			@Override
			public void execute() {
				update();
			}
		});
	}

	@Override
	public void clear() {
		content.clear();
		update();
	}

	@Override
	public Iterator<Widget> iterator() {
		return content.iterator();
	}

	@Override
	public boolean remove(Widget w) {
		boolean removed = content.remove(w);
		update();
		return removed;
	}

	public int getWidgetCount() {
		return content.getWidgetCount();
	}

}
