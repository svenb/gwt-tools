/**
 * 
 */
package gwt.tools.client.ui.common;

import gwt.tools.shared.Util;

import com.google.gwt.user.client.Window;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class WindowTitle {
	
	public static String appName;
	public static String appSubTitle;
	private static String nameSubTitleSeparator;
	private static boolean appNameBeforeTitle;
	private static String nameTitleSeparator;

	public static void set(String newTitle) {
		String appName = getAppName();
		if (Util.isEmpty(newTitle)) {
//			Window.setTitle(Util.notEmpty(appName) ? appName : "");
			Window.setTitle(appName);
		}
		else {
			if (appNameBeforeTitle) Window.setTitle((Util.notEmpty(newTitle) ? appName + nameTitleSeparator : "") + newTitle);
			else Window.setTitle(newTitle + (Util.notEmpty(newTitle) ? nameTitleSeparator + appName : ""));
		}
	}

	private static String getAppName() {
		return Util.isEmpty(appSubTitle) ? appName : appName + nameSubTitleSeparator + appSubTitle;
	}

	public static void init(String appName) {
		init(appName, null);
	}
	public static void init(String appName, String appSubTitle) {
		init(appName, appSubTitle, ": ", " - ");
	}
	public static void init(String appName, String appSubTitle, String nameSubTitleSeparator, String nameTitleSeparator) {
		init(appSubTitle, appName, nameTitleSeparator, nameSubTitleSeparator, true);
	}
	public static void init(String appName, String appSubTitle, String nameSubTitleSeparator, String nameTitleSeparator, boolean appNameBeforeTitle) {
		WindowTitle.appName = appName;
		WindowTitle.appSubTitle = appSubTitle;
		WindowTitle.nameSubTitleSeparator = nameSubTitleSeparator;
		WindowTitle.nameTitleSeparator = nameTitleSeparator;
		WindowTitle.appNameBeforeTitle = appNameBeforeTitle;
		set(null);
	}

}
