/**
 * 
 */
package gwt.tools.client.ui.common;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class LinkList extends Composite {

	private static LinkListUiBinder uiBinder = GWT.create(LinkListUiBinder.class);

	interface LinkListUiBinder extends UiBinder<Widget, LinkList> {
	}

	@UiField VerticalFlowPanel panel;

	public LinkList() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	public void addLink(String label, ClickHandler handler) {
		panel.add(new Anchor(label, handler));
	}

	public void addLink(SafeHtml safeHtml, ClickHandler handler) {
		panel.add(new Anchor(safeHtml, handler));
	}

	public void clear() {
		panel.clear();
	}
}
