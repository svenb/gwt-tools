/**
 * 
 */
package gwt.tools.client.ui.animations;

import com.google.gwt.user.client.ui.IsWidget;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class Fade extends AbstractWidgetAnimation {

	/**
	 * @param widget
	 *            to be faded in or out.
	 * @param showing
	 *            true to fade in, false to fade out
	 * @param duration
	 */
	public Fade(IsWidget widget) {
		super(widget);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gwt.tools.client.ui.animations.AbstractWidgetAnimation#animationShowing
	 * (double)
	 */
	@Override protected void animationShowing(double progress) {
		style.setOpacity(progress);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gwt.tools.client.ui.animations.AbstractWidgetAnimation#animationHiding
	 * (double)
	 */
	@Override protected void animationHiding(double progress) {
		style.setOpacity(1 - progress);
	}

}
