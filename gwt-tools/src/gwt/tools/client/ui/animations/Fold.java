/**
 * 
 */
package gwt.tools.client.ui.animations;

import gwt.tools.client.Util;
import gwt.tools.client.Util.ValueAndUnit;
import gwt.tools.shared.Log;
import gwt.tools.shared.handler.SimpleCallback;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class Fold extends AbstractWidgetAnimation {

	public static enum Direction {
		down, right;
	}

	private final Direction direction = Direction.down;
	private ValueAndUnit targetValueAndUnit;
	private String targetProperty;
	private String restoreOverflow;
	private Widget widget;

	/**
	 * 
	 */
	public Fold(IsWidget widget) {
		super(widget);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.client.ui.animations.AbstractWidgetAnimation#initRun()
	 */
	@Override protected void onBeforeRun(final SimpleCallback callback) {
		Log.debug(this, "initRun: widget.attached", widget.isAttached());
		if (!widget.isAttached()) {
			return;
		}
		getTargetDimension(new SimpleCallback() {

			@Override public void done() {
				Log.debug(this, "run: property,value", targetProperty, targetValueAndUnit);
				restoreOverflow = style.getProperty("overflow");
				style.setProperty("overflow", "hidden !important");
				callback.done();
			}
		});
	}

	/**
	 * @param callback
	 * 
	 */
	private void getTargetDimension(final SimpleCallback callback) {
		//		if (targetValueAndUnit != null) {
		//			callback.done();
		//			return;
		//		}
		extractTargetDimension();
		if (targetValueAndUnit.value <= .0001) {
			final String opacityBak = style.getProperty("opacity");
			style.setOpacity(0);
			widget.setVisible(true);
			style.clearProperty(targetProperty);
			Scheduler.get().scheduleDeferred(new ScheduledCommand() {

				@Override public void execute() {
					extractTargetDimension();
					style.setProperty("opacity", opacityBak);
					callback.done();
				}
			});
		} else {
			callback.done();
		}
	}

	private void extractTargetDimension() {
		String rawDimension;
		switch (direction) {
		case down:
			targetProperty = "height";
			rawDimension = getUsefulProperty(style.getHeight(), style.getProperty("maxHeight"));
			if (Util.isEmpty(rawDimension)) {
				targetValueAndUnit = new Util.ValueAndUnit();
				targetValueAndUnit.unit = Unit.PX;
				targetValueAndUnit.value = element.getOffsetHeight();
			} else {
				targetValueAndUnit = Util.getValueAndUnit(rawDimension);
			}
			break;
		case right:
			targetProperty = "width";
			rawDimension = getUsefulProperty(style.getWidth(), style.getProperty("maxWidth"));
			if (Util.isEmpty(rawDimension)) {
				targetValueAndUnit = new Util.ValueAndUnit();
				targetValueAndUnit.unit = Unit.PX;
				targetValueAndUnit.value = element.getOffsetWidth();
			} else {
				targetValueAndUnit = Util.getValueAndUnit(rawDimension);
			}
			break;
		default:
			assert false;
			rawDimension = null;
		}
		Log.debug(this, "extractTargetDimension: targetVaU", targetValueAndUnit);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.animation.client.Animation#onComplete()
	 */
	@Override protected void onComplete() {
		super.onComplete();
		style.setProperty("overflow", restoreOverflow);
	}

	/**
	 * @param width
	 * @param property
	 * @return
	 */
	private String getUsefulProperty(String currentDimension, String maxDimension) {
		if (Util.isEmpty(currentDimension) || Util.parseInt(currentDimension, 0) == 0) {
			return maxDimension;
		}
		return currentDimension;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gwt.tools.client.ui.animations.AbstractWidgetAnimation#animationShowing
	 * (double)
	 */
	@Override protected void animationShowing(double progress) {
		//		Log.debug(this, ".animationShowing: progress", progress);
		style.setProperty(targetProperty, progress * targetValueAndUnit.value, targetValueAndUnit.unit);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gwt.tools.client.ui.animations.AbstractWidgetAnimation#animationHiding
	 * (double)
	 */
	@Override protected void animationHiding(double progress) {
		//		Log.debug(this, ".animationHiding: progress", progress);
		style.setProperty(targetProperty, (1 - progress) * targetValueAndUnit.value, targetValueAndUnit.unit);
	}

}
