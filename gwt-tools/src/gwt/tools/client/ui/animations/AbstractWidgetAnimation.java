/**
 * 
 */
package gwt.tools.client.ui.animations;

import gwt.tools.shared.handler.SimpleCallback;

import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.IsWidget;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public abstract class AbstractWidgetAnimation extends FixedGwtAnimation {

	public static enum Direction {
		in, out;
	}

	protected final Element element;
	protected final Style style;
	protected int duration = 1000;
	protected boolean reveal = true;
	private SimpleCallback doneCallback;

	public AbstractWidgetAnimation(IsWidget widget) {
		this(widget.asWidget().getElement());
	}

	public AbstractWidgetAnimation(Element element) {
		this.element = element;
		style = element.getStyle();
	}

	public void run(boolean reveal, int revealDuration, int hidingDuration) {
		duration(reveal ? revealDuration : hidingDuration).run(reveal);
	}

	public void run(boolean reveal) {
		this.reveal = reveal;
		onBeforeRun(new SimpleCallback() {

			@Override public void done() {
				superRun();
			}
		});
	}

	private void superRun() {
		super.run(duration, element);
	}

	protected void onBeforeRun(SimpleCallback callback) {
		callback.done();
	}

	public AbstractWidgetAnimation onDone(SimpleCallback callback) {
		doneCallback = callback;
		return this;
	}

	public AbstractWidgetAnimation duration(int duration) {
		duration = duration;
		return this;
	}

	public void reveal() {
		run(true);
	}

	public void reveal(int duration) {
		this.duration = duration;
		reveal();
	}

	public void hide() {
		run(false);
	}

	public void hide(int duration) {
		this.duration = duration;
		hide();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.client.ui.animations.FixedGwtAnimation#onComplete()
	 */
	@Override protected void onComplete() {
		super.onComplete();
		if (doneCallback != null) {
			doneCallback.done();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.animation.client.Animation#onUpdate(double)
	 */
	@Override protected void onUpdate(double progress) {
		if (reveal) {
			animationShowing(progress);
		} else {
			animationHiding(progress);
		}
	}

	protected abstract void animationShowing(double progress);

	protected abstract void animationHiding(double progress);

}
