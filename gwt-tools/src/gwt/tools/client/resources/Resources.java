/**
 * 
 */
package gwt.tools.client.resources;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public interface Resources extends ClientBundle {

	@Source("busy-indicator.gif") ImageResource getBusyIndicator();

	public static Resources instance = GWT.create(Resources.class);

}
