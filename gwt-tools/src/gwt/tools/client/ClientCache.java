/**
 * 
 */
package gwt.tools.client;


/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class ClientCache extends gwt.tools.shared.CommonCache {

	private static gwt.tools.shared.CommonCache cacheInstance;

	public ClientCache() {
		super(new HashMapCache());
	}

	public static gwt.tools.shared.CommonCache getInstance() {
		if (cacheInstance == null) {
			cacheInstance = new ClientCache();
		}
		return cacheInstance;
	}
}
