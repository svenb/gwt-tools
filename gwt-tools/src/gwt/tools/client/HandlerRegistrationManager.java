/**
 * 
 */
package gwt.tools.client;

import java.util.LinkedList;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ContextMenuEvent;
import com.google.gwt.event.dom.client.ContextMenuHandler;
import com.google.gwt.event.dom.client.DomEvent;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.HasKeyUpHandlers;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class HandlerRegistrationManager {

	//	private static final LinkedList<HandlerManager> handlerPerOwner = new LinkedList<HandlerManager>();

	// no weak ref for javascript so we have to take care manually, thus there will always be memory leaks.
	//	private static Timer loop = null;
	//
	//	private static void runLoop() {
	//		if (loop == null) {
	//			(loop = new Timer() {
	//
	//				@Override public void run() {
	//					HandlerManager current;
	//					for (Iterator<HandlerManager> i = handlerPerOwner.iterator(); i.hasNext();) {
	//						if (i.next().isDone()) {
	//							i.remove();
	//						}
	//					}
	//				}
	//			}).scheduleRepeating(5000);
	//		}
	//		
	//		Weakm
	//	}

	private LinkedList<HandlerRegistration> handlerRegistrations = new LinkedList<HandlerRegistration>();

	//	WeakReference<Widget> owner;
	//	WeakReference<Widget> owner;

	/**
	 * 
	 */
	//	public HandlerManager(Widget owner) {
	//		owners.add(new WeakReference<Widget>(owner));
	//		this.owner = owner;
	//		handlerPerOwner.add(this);
	//		runLoop();
	//	}

	/**
	 * 
	 * Sets handler as "regular" handler. See
	 * {@link Widget#addHandler(EventHandler, com.google.gwt.event.shared.GwtEvent.Type)}
	 * 
	 * @param widget
	 * @param onClick
	 */
	public void set(Widget widget, ClickHandler onClick) {
		handlerRegistrations.add(((HasClickHandlers) widget).addClickHandler(onClick));
	}

	/**
	 * 
	 * Sets handler as "regular" handler. See
	 * {@link Widget#addHandler(EventHandler, com.google.gwt.event.shared.GwtEvent.Type)}
	 * 
	 * @param widget
	 * @param handler
	 * @param type
	 */
	public <H extends EventHandler> void set(Widget widget, H handler, GwtEvent.Type<H> type) {
		handlerRegistrations.add(widget.addHandler(handler, type));
	}

	/**
	 * Sets handler as DomHandler, thus allowing to sink native events. See
	 * {@link Widget#addDomHandler(EventHandler, com.google.gwt.event.dom.client.DomEvent.Type)}
	 * 
	 * @param widget
	 * @param handler
	 * @param type
	 */
	public <H extends EventHandler> void setDom(Widget widget, H handler, DomEvent.Type<H> type) {
		handlerRegistrations.add(widget.addDomHandler(handler, type));
	}

	/**
	 * Removes all the event handler registered before.
	 */
	public void clear() {
		for (HandlerRegistration registration : handlerRegistrations) {
			try {
				registration.removeHandler();
			} catch (Throwable t) {
			}
		}
		handlerRegistrations.clear();
	}

	/**
	 * @param value
	 * @param keyUpHandler
	 */
	public void set(Widget widget, KeyUpHandler keyUpHandler) {
		handlerRegistrations.add(((HasKeyUpHandlers) widget).addKeyUpHandler(keyUpHandler));
	}

	public void set(Widget widget, final ContextMenuHandler contextMenuHandler, boolean avoidDefaultBrowserBehaviour) {
		handlerRegistrations.add(widget.addHandler(avoidDefaultBrowserBehaviour ? new ContextMenuHandler() {

			@Override public void onContextMenu(ContextMenuEvent event) {
				// stop the browser from opening the context menu
				event.preventDefault();
				event.stopPropagation();

				contextMenuHandler.onContextMenu(event);

			}
		} : contextMenuHandler, ContextMenuEvent.getType()));
	}

	public void set(final PopupPanel widget, ContextMenuHandler contextMenuHandler) {
		set(widget, new ContextMenuHandler() {

			@Override public void onContextMenu(ContextMenuEvent event) {
				widget.setPopupPosition(event.getNativeEvent().getClientX(), event.getNativeEvent().getClientY());
				widget.show();
			}
		}, true);
	}

	/**
	 * @return
	 */
	public boolean isEmpty() {
		return handlerRegistrations.isEmpty();
	}

	//	private boolean isDone() {
	//		if (owner.get() == null) {
	//			clear();
	//			return true;
	//		}
	//		return false;
	//	}
}
