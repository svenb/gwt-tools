/**
 * 
 */
package gwt.tools.client;

import gwt.tools.shared.Log;
import gwt.tools.shared.handler.SimpleCallback;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.canvas.dom.client.CssColor;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.http.client.UrlBuilder;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.InsertPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.ValueBoxBase;
import com.google.gwt.user.client.ui.Widget;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class Util extends gwt.tools.shared.Util {

	private static final String CSS_CLASS_ERROR = "error";
	public static void clear(Canvas canvas) {
		clear(canvas, canvas.getContext2d());
	}

	/**
	 * @param c
	 */
	public static void clear(Canvas canvas, Context2d context) {
		context.clearRect(0, 0, canvas.getCoordinateSpaceWidth(), canvas.getCoordinateSpaceHeight());
	}

	/**
	 * @param canvas
	 * @param c
	 * @param color
	 */
	public static void fill(Canvas canvas, Context2d context, String color) {
		context.save();
		context.setFillStyle(CssColor.make(color));
		context.fillRect(0, 0, canvas.getCoordinateSpaceWidth(), canvas.getCoordinateSpaceHeight());
		context.restore();
	}

	/**
	 * @param width
	 * @param height
	 * @return
	 */
	public static Canvas createCanvas(int width, int height) {
		Canvas canvas = Canvas.createIfSupported();
		canvas.setPixelSize(width, height);
		canvas.setCoordinateSpaceWidth(width);
		canvas.setCoordinateSpaceHeight(height);
		return canvas;
	}

	public static void resizeCanvas(Canvas canvas, int width, int height) {
		canvas.setPixelSize(width, height);
		canvas.setCoordinateSpaceWidth(width);
		canvas.setCoordinateSpaceHeight(height);
	}

	public static int getCenterX(UIObject widget) {
		return widget.getAbsoluteLeft() + widget.getOffsetWidth() / 2;
	}

	public static int getCenterY(UIObject widget) {
		return widget.getAbsoluteTop() + widget.getOffsetHeight() / 2;
	}

	/**
	 * Calculates the position of the right end of a widget (x + width)
	 */
	public static int getRight(UIObject widget) {
		assert widget != null;
		return widget.getAbsoluteLeft() + widget.getOffsetWidth();
	}

	//	/**
	//	 * Calculates the position below a widget (y + height)
	//	 */
	//	public static int getBottom(UIObject widget) {
	//		assert widget != null;
	//		return widget.getAbsoluteTop() + widget.getOffsetHeight();
	//	}

	/**
	 * Calculates the position below a widget (y + height)
	 */
	public static int getBottom(IsWidget widget) {
		assert widget != null;
		Widget w = widget.asWidget();
		assert w != null;
		return w.getAbsoluteTop() + w.getOffsetHeight();
	}

	public static class BoundingBox {
		public int top;
		public int right;
		public int bottom;
		public int left;
		public int centerX;
		public int centerY;
		public int width;
		public int height;

		public BoundingBox() {
		}

		public BoundingBox(int top, int right, int bottom, int left) {
			this.top = top;
			this.right = right;
			this.bottom = bottom;
			this.left = left;
			calculateDimensions();
		}

		public static BoundingBox getDefault() {
			return new BoundingBox(Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE);
		}

		public static BoundingBox getOuterBoundingBox(BoundingBox... boundingBoxes) {
			return getOuterBoundingBox(Util.createList(boundingBoxes));
		}

		public static BoundingBox getOuterBoundingBox(Collection<BoundingBox> boundingBoxes) {
			BoundingBox overallBoundingBox = BoundingBox.getDefault();
			for (BoundingBox boundingBox : boundingBoxes) {
				if (boundingBox.top < overallBoundingBox.top) {
					overallBoundingBox.top = boundingBox.top;
				}
				if (boundingBox.right > overallBoundingBox.right) {
					overallBoundingBox.right = boundingBox.right;
				}
				if (boundingBox.bottom > overallBoundingBox.bottom) {
					overallBoundingBox.bottom = boundingBox.bottom;
				}
				if (boundingBox.left < overallBoundingBox.left) {
					overallBoundingBox.left = boundingBox.left;
				}
			}
			overallBoundingBox.calculateDimensions();
			return overallBoundingBox;
		}

		public UIObject asVirtualUiObject() {
			return new UIObject() {
				/*
				 * (non-Javadoc)
				 * 
				 * @see com.google.gwt.user.client.ui.UIObject#getAbsoluteLeft()
				 */
				@Override public int getAbsoluteLeft() {
					return left;
				}

				/*
				 * (non-Javadoc)
				 * 
				 * @see com.google.gwt.user.client.ui.UIObject#getAbsoluteTop()
				 */
				@Override public int getAbsoluteTop() {
					return top;
				}

				/*
				 * (non-Javadoc)
				 * 
				 * @see com.google.gwt.user.client.ui.UIObject#getOffsetWidth()
				 */
				@Override public int getOffsetWidth() {
					return right - left;
				}

				/*
				 * (non-Javadoc)
				 * 
				 * @see com.google.gwt.user.client.ui.UIObject#getOffsetHeight()
				 */
				@Override public int getOffsetHeight() {
					return bottom - top;
				}
			};
		}

		public void calculateDimensions() {
			width = right - left;
			height = bottom - top;
			centerX = left + width / 2;
			centerY = top + height / 2;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override public String toString() {
			return "Util.BoundingBox(trbl " + top + " " + right + " " + bottom + " " + left + ")";
		}
	}

	public static BoundingBox getBoundingBox(IsWidget isWidget) {
		Widget widget = isWidget.asWidget();
		return new BoundingBox(widget.getAbsoluteTop(), getRight(widget), getBottom(widget), widget.getAbsoluteLeft());
	}

	/**
	 * Uses the given left and top values plus the width and height of the given
	 * widget.
	 * 
	 * @param left
	 * @param top
	 * @param isWidget
	 *            for width and height
	 * @return
	 */
	public static BoundingBox getBoundingBox(int left, int top, IsWidget isWidget) {
		Widget widget = isWidget.asWidget();
		return new BoundingBox(top, left + widget.getOffsetWidth(), top + widget.getOffsetHeight(), left);
	}

	public static BoundingBox getBoundingBox(Collection<IsWidget> widgets) {
		LinkedList<BoundingBox> boxes = new LinkedList<BoundingBox>();
		for (IsWidget widget : widgets) {
			boxes.add(getBoundingBox(widget));
		}
		return BoundingBox.getOuterBoundingBox(boxes);
	}

	public static interface WidgetExtractor<T> {
		IsWidget extract(T object);
	}

	public static <T> BoundingBox getBoundingBox(Collection<T> elements, WidgetExtractor<T> extractor) {
		Collection<IsWidget> widgets = new LinkedList<IsWidget>();
		for (T element : elements) {
			widgets.add(extractor.extract(element));
		}
		return getBoundingBox(widgets);
	}

	//	/**
	//	 * Null save conversion of object -> string by using object.toString()
	//	 * 
	//	 * @param object
	//	 * @return
	//	 */
	//	public static String str(Object object) {
	//		return (object == null) ? null : object.toString();
	//	}

	public static String toString(IsWidget isWidget) {
		Widget widget = isWidget.asWidget();
		return (widget == null) ? null : widget.getClass().getName() + "(" + widget.getAbsoluteLeft() + "x"
			+ widget.getAbsoluteTop() + ", " + widget.getOffsetWidth() + "x" + widget.getOffsetHeight() + ")";
	}

	public static boolean isObjectAssignableTo(Object object, Class<?> classWhichTheObjectShouldBeAssignedTo) {
		return isClassAssignableTo((Class<?>) ((object instanceof Class) ? object : object.getClass()),
			classWhichTheObjectShouldBeAssignedTo);
	}

	public static boolean isClassAssignableTo(Class<?> classOfObjectInQuestion,
		Class<?> classWhichTheObjectShouldBeAssignedTo) {
		if (classWhichTheObjectShouldBeAssignedTo == null) {
			return false;
		}

		if (classOfObjectInQuestion.equals(classWhichTheObjectShouldBeAssignedTo)) {
			return true;
		}

		Class<?> currentSuperClass = classOfObjectInQuestion;
		while ((currentSuperClass = currentSuperClass.getSuperclass()) != null) {
			if (currentSuperClass.equals(classWhichTheObjectShouldBeAssignedTo)) {
				return true;
			}
		}
		return false;
	}

	public static <WidgetType extends Widget> WidgetType removeAllChildren(WidgetType widget) {
		if (widget instanceof HasWidgets) {
			((HasWidgets) widget).clear();
		}
		if (notEmpty(widget)) {
			removeAllChildren(widget.getElement());
		}
		return widget;
	}

	/**
	 * @param element
	 */
	public static void removeAllChildren(Element element) {
		while (element.getChildCount() > 0) {
			element.removeChild(element.getChild(0));
		}
	}

	/**
	 * Removes all sub events of the given panel and adds newContent.
	 * 
	 * @param panel
	 * @param newContent
	 * @return newContent
	 */
	public static <PanelType extends IsWidget & HasWidgets, WidgetType extends IsWidget> WidgetType replaceContent(
		PanelType panel, WidgetType newContent) {
		removeAllChildren(panel.asWidget());
		panel.add(newContent.asWidget());
		return newContent;
	}

	/**
	 * @param widget
	 * @param visible
	 */
	public static void setVisible(UIObject widget, boolean visible) {
		setVisible(widget.getElement(), visible);
	}

	/**
	 * @param element
	 * @param visible
	 */
	public static void setVisible(Element element, boolean visible) {
		UIObject.setVisible(element, visible);
	}

	/**
	 * Makes given widget visible/in- by using maxHeight:0 or :none, thus CSS
	 * animations kick in.
	 * 
	 * @param widget
	 * @param visible
	 */
	public static void setVisibleViaHeight(IsWidget widget, boolean visible) {
		setVisibleViaHeight(widget.asWidget().getElement(), visible);
	}

	/**
	 * Makes given element visible/in- by using maxHeight:0 or :none, thus CSS
	 * animations kick in.
	 * 
	 * @param widget
	 * @param visible
	 */
	public static void setVisibleViaHeight(com.google.gwt.dom.client.Element element, boolean visible) {
		element.getStyle().setProperty("maxHeight", visible ? "none" : "0");
	}

	public static void clearLocationHash() {
		setLocationHash("");
	}

	public static native void setLocationHash(String hash)/*-{
		window.location.hash = "";
	}-*/;

	public static class ValueAndUnit {
		public double value;
		public Unit unit;

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override public String toString() {
			return "ValueAndUnit(" + value + ", " + unit + ")";
		}
	}

	public static ValueAndUnit getValueAndUnit(Widget widget, String propertyName) {
		return getValueAndUnit(widget.getElement(), propertyName);
	}

	public static ValueAndUnit getValueAndUnit(Element element, String propertyName) {
		return getValueAndUnit(element.getStyle().getProperty(propertyName));
	}

	/**
	 * Parses the given CSS value and returns the numeric value and the unit
	 * separately.
	 * 
	 * @param rawDimension
	 */
	public static ValueAndUnit getValueAndUnit(String cssValue) {
		ValueAndUnit result = new ValueAndUnit();

		// all units are two letters unless it is percentage
		if (notEmpty(cssValue = cssValue.trim())) {
			if (cssValue.endsWith("%")) {
				result.unit = Unit.PCT;
				result.value = parseDouble(truncateBy(cssValue, 1));
			} else {
				result.unit = Unit.valueOf(cssValue.substring(cssValue.length() - 2).toUpperCase());
				result.value = parseDouble(truncateBy(cssValue, 2));
			}
		}
		return result;
	}

	/**
	 * @param panel
	 * @param cellTable
	 * @param i
	 */
	public static void insertFirst(InsertPanel panel, Widget widget) {
		insertBefore(panel, widget, 1);
	}

	/**
	 * @param panel
	 * @param cellTable
	 * @param i
	 */
	public static void insertBefore(InsertPanel panel, Widget widget, int index) {
		if (panel.getWidgetCount() >= index) {
			panel.add(widget);
		} else {
			panel.insert(widget, index);
		}
	}

	/**
	 * Execute UI building code in the callback, not right after calling this
	 * method!
	 * 
	 * @param doneCallback
	 * 
	 */
	public static void setUncaughtExceptionHandler(final SimpleCallback doneCallback) {
		assert doneCallback != null;
		GWT.setUncaughtExceptionHandler(new GWT.UncaughtExceptionHandler() {

			@Override public void onUncaughtException(Throwable e) {
				Log.severe(this, "UNCAUGHT EXCEPTION", e);
			}
		});

		// further execution needs to be deferred so that the generic
		// gets executed
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {

			@Override public void execute() {
				doneCallback.done();
			}
		});
	}

	/**
	 * @param panel
	 * @param string
	 * @param borderColor
	 */
	public static void setStyle(Widget widget, String propertyName, String propertyValue) {
		assert widget != null;
		widget.getElement().getStyle().setProperty(propertyName, propertyValue);
	}

	public static void setStyle(IsWidget widget, String propertyName, double propertyValue, Unit unit) {
		assert widget != null && widget.asWidget() != null;
		widget.asWidget().getElement().getStyle().setProperty(propertyName, propertyValue, unit);
	}

	public static boolean notEmpty(IsWidget widget) {
		return !isEmpty(widget);
	}

	//	public static boolean notEmpty(Widget widget) {
	//		return !isEmpty(widget);
	//	}

	public static boolean notEmpty(Element element) {
		return !isEmpty(element);
	}

	public static boolean isEmpty(IsWidget widget) {
		return size(widget) < 1;
	}

	public static boolean isEmpty(Widget widget) {
		return size(widget) < 1;
	}

	public static boolean isEmpty(Element element) {
		return size(element) < 1;
	}

	/**
	 * @param widget
	 * @return
	 */
	private static int size(IsWidget widget) {
		return (widget == null) ? 0 : size(widget.asWidget());
	}

	public static int size(Widget widget) {
		return (widget == null) ? 0 : size(widget.getElement());
	}

	public static int size(Element element) {
		return (element == null) ? 0 : element.getChildCount();
	}

	/**
	 * Toggles the visiblity of all the given Widgets
	 * 
	 * @param widgets
	 */
	public static void toggleVisible(Widget... widgets) {
		for (Widget widget : widgets) {
			widget.setVisible(!widget.isVisible());
		}
	}

	/**
	 * Toggles the visibility of all the given widgets
	 * 
	 * @param widgets
	 */
	public static void toggleVisibilityViaHeight(Widget... widgets) {
		for (Widget widget : widgets) {
			setVisibleViaHeight(widget, !isVisibleViaHeight(widget));
		}
	}

	/**
	 * Toggles the visibility of all the given elements
	 * 
	 * @param widgets
	 */
	public static void toggleVisibilityViaHeight(com.google.gwt.dom.client.Element... elements) {
		for (com.google.gwt.dom.client.Element element : elements) {
			setVisibleViaHeight(element, !isVisibleViaHeight(element));
		}
	}

	public static boolean isVisibleViaHeight(Widget widget) {
		return isVisibleViaHeight(widget.getElement());
	}

	public static boolean isVisibleViaHeight(com.google.gwt.dom.client.Element element) {
		return "none".equals(element.getStyle().getProperty("maxHeight"));
	}

	public static void sortWidgets(FlowPanel panel, Comparator<? super Widget> comperator){
		List<Widget> widgets = createList(panel.iterator());
		Collections.sort(widgets, comperator);
		for (int index = 0; index < widgets.size(); index++) {
			panel.insert(widgets.get(index), index);
		}
	}
	
	/**
	 * Reloads the current page in the browser. (using the cache)
	 */
	public static void reloadPage() {
		Window.Location.reload();
	}

	/**
	 * Forces a reload of the current page in the browser using the currently
	 * set URL; (disable caching issues using
	 * "window.document.location.reload(true);")
	 */
	public static native void reloadPageForced() /*-{
		window.document.location.reload(true);
	}-*/;

	/**
	 * @return
	 */
	public static void reloadPageForcedWithCache() {
		String url = Window.Location.getHref();
		Window.Location.replace("");
		Window.Location.replace(url);
	}

	public static void goToUrl(String url) {
		goToUrl(url, false);
	}
	public static void goToUrl(String url, boolean inNewWindow) {
		if (inNewWindow) Window.open(url, "_blank", "");
		else Window.Location.assign(url);
	}
	
	public static void goHome() {
		goToUrl(getHomeUrl());
	}
	
	public static String getHomeUrl() {
		UrlBuilder urlBuilder = Window.Location.createUrlBuilder();
		urlBuilder.setHash("");
		return urlBuilder.buildString();
	}
	
	public static void showAndAutoHide(UIObject object) {
		showAndAutoHide(object, 3000);
	}
	public static void showAndAutoHide(final UIObject object, int timeInMs) {
		showAndAutoHide(object, timeInMs, null);
	}
	public static void showAndAutoHide(final UIObject object, int timeInMs, final SimpleCallback callbackOnHide) {
		assert timeInMs > 0;
		object.setVisible(true);
		Scheduler.get().scheduleFixedDelay(new RepeatingCommand() {
			
			@Override
			public boolean execute() {
				object.setVisible(false);
				if (callbackOnHide != null) callbackOnHide.done();
				return false;
			}
		}, timeInMs);
	}
	
	public static abstract class HideFeedback {
		public boolean done = false;
		public abstract void hide();
	}

	public static HideFeedback feedback(UIObject alertWidget, UIObject... widgetsContainingErrors) {
		return feedback(alertWidget, CSS_CLASS_ERROR, widgetsContainingErrors);
	}
	
	public static void feedback(boolean show, UIObject alertWidget, String cssClass, UIObject... widgetsContainingErrors) {
		alertWidget.setVisible(show);
		if (Util.notEmpty(widgetsContainingErrors))
			for (UIObject widget : widgetsContainingErrors) {
				widget.setStyleName(cssClass, show);
			}
	}
	
	public static HideFeedback feedback(final UIObject alertWidget, final String cssClass, final UIObject... widgetsContainingErrors) {
		return feedback(alertWidget, cssClass, null, widgetsContainingErrors);
	}
	
	public static HideFeedback feedback(final UIObject alertWidget, int autoHideTimeoutInMs, final UIObject... widgetsContainingErrors) {
		return feedback(alertWidget, new Long(autoHideTimeoutInMs), widgetsContainingErrors);
	}
	
	public static HideFeedback feedback(final UIObject alertWidget, Long autoHideTimeoutInMs, final UIObject... widgetsContainingErrors) {
		return feedback(alertWidget, CSS_CLASS_ERROR, autoHideTimeoutInMs, widgetsContainingErrors);
	}
	public static HideFeedback feedback(final UIObject alertWidget, final String cssClass, Long autoHideTimeoutInMs, final UIObject... widgetsContainingErrors) {
		final HideFeedback hideFeedback = new HideFeedback() {
			
			@Override
			public void hide() {
				if (done) return;
				done = true;
				feedback(false, alertWidget, cssClass, widgetsContainingErrors);
			}
		};
		
		// show
		feedback(true, alertWidget, cssClass, widgetsContainingErrors);
		
		if (autoHideTimeoutInMs != null) {
			// auto hide enabled
			Scheduler.get().scheduleFixedDelay(new RepeatingCommand() {
				
				@Override
				public boolean execute() {
					hideFeedback.hide();
					return false;
				}
			}, autoHideTimeoutInMs.intValue());
		}
		
		return hideFeedback;
	}
	
	public static void disableAutoSuggest(ValueBoxBase<?> inputBox) {
		inputBox.getElement().setAttribute("autocomplete", "off");
	}
}
