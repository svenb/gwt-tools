/**
 * 
 */
package gwt.tools.client;

import gwt.tools.shared.Cache;
import gwt.tools.shared.Log;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class HashMapCache implements Cache {

	HashMap<Object, Object> map = new HashMap<Object, Object>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.shared.Cache#put(java.lang.Object, java.lang.Object)
	 */
	@Override public void put(Object key, Object value) {
		map.put(key, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.shared.Cache#putAll(java.util.Map)
	 */
	@Override public void putAll(Map<? extends Object, ? extends Object> keyValuePairs) {
		map.putAll(keyValuePairs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.shared.Cache#get(java.lang.Object)
	 */
	@Override public Object get(Object key) {
		return map.get(key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.shared.Cache#getAll(java.util.Collection)
	 */
	@Override public Map<Object, Object> getAll(Collection<Object> keys) {
		Map<Object, Object> results = new HashMap<Object, Object>();
		for (Object key : keys) {
			Object cached;
			try {
				// get(key, resultClass);
				cached = get(key);
				if (cached != null) {
					results.put(key, cached);
				}
			} catch (Throwable t) {
				Log.debug(this, ".getAll: reading each separately failed on $key. ", t, key);
			}
		}
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.shared.Cache#remove(java.lang.Object)
	 */
	@Override public void remove(Object key) {
		map.remove(key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.shared.Cache#clear()
	 */
	@Override public void clear() {
		map.clear();
	}

}
