/**
 * 
 */
package gwt.tools.server;

import gwt.tools.server.apache.collections.TreeBidiMap;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
@SuppressWarnings("unchecked") public class BIDirectionalMap<Key, Value> {

	TreeBidiMap map = new TreeBidiMap();

	public Key getKey(Value value) {
		return (Key) map.inverseBidiMap().getKey(value);
	}

	public Value getValue(Key key) {
		return (Value) map.get(key);
	}

	public Object put(Key key, Value value) {
		return map.put(key, value);
	}

}
