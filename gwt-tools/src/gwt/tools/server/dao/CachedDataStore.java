/**
 * 
 */
package gwt.tools.server.dao;

import gwt.tools.server.dao.cache.AsyncInMemoryCache;
import gwt.tools.server.dao.cache.CacheWithEntryRetrieval;
import gwt.tools.server.dao.cache.DatastoreDao;
import gwt.tools.shared.Cache;
import gwt.tools.shared.Log;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class CachedDataStore implements gwt.tools.shared.Cache {

	private static String nameSpace = "gwttoolscache";

	private Cache cache;
	private DatastoreDao datastore;
	private final boolean deleteOutdated;
	private final CacheWithEntryRetrieval withEntryRetrieval;

	public CachedDataStore(boolean deleteOutdated) throws CacheException {
		this.deleteOutdated = deleteOutdated;
		cache = new AsyncInMemoryCache(nameSpace);// new SyncInMemoryCache(nameSpace);
		withEntryRetrieval = (cache instanceof CacheWithEntryRetrieval) ? (CacheWithEntryRetrieval) cache : null;
		datastore = new RawDatastoreDao(nameSpace);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.shared.Cache#clear()
	 */
	@Override public void clear() {
		cache.clear();
		datastore.clear();
	}

	//	/*
	//	 * (non-Javadoc)
	//	 * 
	//	 * @see java.util.Map#containsKey(java.lang.Object)
	//	 */
	//	@Override public boolean containsKey(Object uri) {
	//		return cache.containsKey(uri);
	//	}
	//
	//	/*
	//	 * (non-Javadoc)
	//	 * 
	//	 * @see java.util.Map#containsValue(java.lang.Object)
	//	 */
	//	@Override public boolean containsValue(Object arg0) {
	//		throw new RuntimeException("Not implemented. Do not use.");
	//	}
	//
	//	/*
	//	 * (non-Javadoc)
	//	 * 
	//	 * @see java.util.Map#entrySet()
	//	 */
	//	@Override public Set<java.util.Map.Entry<Object, Object>> entrySet() {
	//		throw new RuntimeException("Not implemented. Do not use.");
	//	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#get(java.lang.Object)
	 */
	@Override public Object get(Object key) {
		Object object = null;
		CacheEntry cacheEntry = null;
		try {
			if (withEntryRetrieval != null) {
				cacheEntry = withEntryRetrieval.getCacheEntry(key);
				object = cacheEntry.getValue();
			} else {
				object = cache.get(key);
			}
		} catch (Throwable t) {
			// might be a com.google.appengine.api.memcache.InvalidValueException when the DTOs have been changed, thus the deserialization from the cache fails
			// or simply a CacheException
			Log.debug(this, "get($key): failed to get data from underlaying cache.", t, key);
			try {
				cache.remove((cacheEntry != null) ? cacheEntry.getKey() : key);
			} catch (Throwable t2) {
				Log.debug(this, "Failed to remove non-readable entry.", t2);
			}
		}
		if (object == null) {
			object = datastore.get(key);
			cache.put(key, object);
		}
		return object;
	}

	@Override public Map<Object, Object> getAll(Collection<Object> keys) {
		Map<Object, Object> cached = null;
		try {
			cached = cache.getAll(keys);
		} catch (Throwable t) {
			Log.debug(this, ".getAll(): Failed reading in-memory cache.", t);
		}
		Collection<Object> toGet = null;
		if (cached == null) {
			cached = new HashMap<Object, Object>();
		}
		if (cached.size() == 0) {
			toGet = keys;
		} else {
			toGet = new LinkedList<Object>();
			for (Object key : keys) {
				Object value = cached.get(key);
				if (value == null) {
					toGet.add(key);
				}
			}
		}
		if (toGet.size() > 0) {
			Map<Object, Object> stored = datastore.getAll(toGet);
			cache.putAll(stored);
			cached.putAll(stored);
		}
		return cached;
	}

	//	/*
	//	 * (non-Javadoc)
	//	 * 
	//	 * @see java.util.Map#isEmpty()
	//	 */
	//	@Override public boolean isEmpty() {
	//		throw new RuntimeException("Not implemented. Do not use.");
	//	}
	//
	//	/*
	//	 * (non-Javadoc)
	//	 * 
	//	 * @see java.util.Map#keySet()
	//	 */
	//	@Override public Set<Object> keySet() {
	//		throw new RuntimeException("Not implemented. Do not use.");
	//	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#put(java.lang.Object, java.lang.Object)
	 */
	//	@Override public Object put(Object uri, Object value) {
	@Override public void put(Object uri, Object value) {
		datastore.put(uri, value, true);
		cache.put(uri, value);
		//		return value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#putAll(java.util.Map)
	 */
	@Override public void putAll(Map<? extends Object, ? extends Object> map) {
		//		for (Object uri : map.keySet()) {
		//			put(uri, map.get(uri));
		//		}
		cache.putAll(map);
		datastore.putAll(map, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#remove(java.lang.Object)
	 */
	@Override public void remove(Object uri) {
		datastore.remove(uri);
		//		return cache.remove(uri);
		cache.remove(uri);
	}

	//	/*
	//	 * (non-Javadoc)
	//	 * 
	//	 * @see java.util.Map#size()
	//	 */
	//	@Override public int size() {
	//		throw new RuntimeException("Not implemented. Do not use.");
	//	}
	//
	//	/*
	//	 * (non-Javadoc)
	//	 * 
	//	 * @see java.util.Map#values()
	//	 */
	//	@Override public Collection<Object> values() {
	//		throw new RuntimeException("Not implemented. Do not use.");
	//	}

}
