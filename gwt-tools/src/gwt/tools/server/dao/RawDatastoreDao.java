/**
 * 
 */
package gwt.tools.server.dao;

import gwt.tools.server.dao.cache.DatastoreDao;
import gwt.tools.shared.Constants;
import gwt.tools.shared.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import com.google.appengine.api.datastore.AsyncDatastoreService;
import com.google.appengine.api.datastore.Blob;
import com.google.appengine.api.datastore.DatastoreServiceConfig;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.ReadPolicy;

/**
 * 
 * 
 y * @author Sven Buschbeck
 * 
 * IDEA add a shard counter to know how many entities are in and delete the
 * oldest if the number reaches a threshold.
 */
public class RawDatastoreDao<DataType> implements DatastoreDao {

	public static final long LIVE_FOREVER = 0;
	/**
	 * 
	 */
	private static final String VALUE_PROPERTY = "value";
	private static final String DATE_PROPERTY = "date";
	/**
	 * 
	 */
	private final String NAMESPACE;
	private static final int MAX_BLOB_SIZE = 1024 * 1024 * 1024 - 10;
	AsyncDatastoreService service;
	private long maxAge;

	/**
	 * Creates cache with default item live time of one month. To create
	 * differently, use {@link #RawDatastoreDao(long)}
	 * 
	 * @param nameSpace
	 */
	public RawDatastoreDao(String nameSpace) {
		this(nameSpace, Constants.TIME_ONE_MONTH); // live for one month
	}

	/**
	 * Creates a cache with a custom time to live (in ms) for each item put in.
	 * 
	 * @param maxAge
	 *            time to live for each data store item in ms or 0 to live
	 *            forever. There are some static constants for convenience.
	 * 
	 */
	public RawDatastoreDao(String nameSpace, long maxAge) {
		NAMESPACE = nameSpace;
		this.maxAge = maxAge;
		DatastoreServiceConfig config = DatastoreServiceConfig.Builder.withReadPolicy(new ReadPolicy(
			ReadPolicy.Consistency.EVENTUAL));
		service = DatastoreServiceFactory.getAsyncDatastoreService(config);
	}

	public DataType getConverted(Object key) {
		return (DataType) get(key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.server.dao.cache.DatastoreDao#get(java.lang.Object)
	 */
	@Override public Object get(Object key) {
		try {
			Object object = extractObject(service.get(getKey(key)).get());
			Log.debug(this, "Hit for key=" + key);
			return object;
		} catch (Exception e) {
			if (e.getCause() != null
				&& e.getCause() instanceof com.google.appengine.api.datastore.EntityNotFoundException) {
				Log.debug(this, "No entry for key=" + key);
				return null;
			}
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.server.dao.cache.DatastoreDao#getAll(java.util.Collection)
	 */
	@Override public Map<Object, Object> getAll(Collection<Object> keys) {
		try {
			return extractObject(service.get(getKeys(keys)).get());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.server.dao.cache.DatastoreDao#put(java.lang.Object,
	 * java.lang.Object, boolean)
	 */
	@Override public void put(Object key, Object value, boolean asynchronous) {
		service.put(wrapInEntity(getKey(key), value));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.server.dao.cache.DatastoreDao#putAll(java.util.Map,
	 * boolean)
	 */
	@Override public void putAll(Map<? extends Object, ? extends Object> uriToValueMap, boolean asynchronous) {
		service.put(wrapInEntity(uriToValueMap));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.server.dao.cache.DatastoreDao#remove(java.lang.Object)
	 */
	@Override public void remove(Object key) {
		remove(getKey(key));
	}

	private void remove(Key key) {
		service.delete(key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.server.dao.cache.DatastoreDao#clear()
	 */
	@Override public void clear() {
		Query query = new Query(NAMESPACE).setKeysOnly();
		PreparedQuery preparedQuery = service.prepare(query);
		// HACK there must be something more efficient than that!
		for (Entity entity : preparedQuery.asIterable()) {
			service.delete(entity.getKey());
		}
	}

	/**
	 * @param key
	 * @param value
	 * @return
	 */
	private Entity wrapInEntity(Key key, Object value) {
		if (value == null) {
			return null;
		}
		Entity entity = new Entity(key);
		Blob blob = null;
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream(256);
			ObjectOutputStream stream = new ObjectOutputStream(baos);
			stream.writeObject(value);
			stream.flush();
			stream.close();
			baos.flush();
			byte[] bytes = baos.toByteArray();
			if (bytes.length > MAX_BLOB_SIZE) {
				Log.warn(this, ".createDataStoreObject: data for key=" + key
					+ " is too large (1MB limit), expect errors when deserializing...");
			}
			blob = new Blob(bytes);
		} catch (Throwable t) {
			t.printStackTrace();
		}

		entity.setProperty(VALUE_PROPERTY, blob);
		entity.setProperty(DATE_PROPERTY, new Date().getTime());
		return entity;
	}

	/**
	 * @param uriToValueMap
	 * @return
	 */
	private Iterable<Entity> wrapInEntity(Map<? extends Object, ? extends Object> uriToValueMap) {
		LinkedList<Entity> result = new LinkedList<Entity>();
		for (Object key : uriToValueMap.keySet()) {
			Entity entity = wrapInEntity(getKey(key), uriToValueMap.get(key));
			if (entity != null) {
				result.add(entity);
			}
		}
		return result;
	}

	/**
	 * @param keys
	 * @return
	 */
	private Iterable<Key> getKeys(Collection<Object> keys) {
		LinkedList<Key> resultKeys = new LinkedList<Key>();
		for (Object key : keys) {
			resultKeys.add(getKey(key));
		}
		return resultKeys;
	}

	private Key getKey(Object key) {
		assert key != null;
		return KeyFactory.createKey(NAMESPACE, key.toString());
	}

	/**
	 * @param entity
	 * @return
	 */
	private Object extractObject(Entity entity) {
		Blob blob = (Blob) entity.getProperty(VALUE_PROPERTY);
		if (blob == null || !entity.hasProperty(DATE_PROPERTY)) {
			return null;
		}
		if (maxAge > 0 && ((Long) entity.getProperty(DATE_PROPERTY)) + maxAge < new Date().getTime()) {
			Log.debug(this, ".extractObject: entity too old. key=" + entity.getKey().getName());
			entity.getKey().getName();
			return null;
		}
		Object value = null;

		try {
			ByteArrayInputStream bais = new ByteArrayInputStream(blob.getBytes());
			ObjectInputStream stream = new ObjectInputStream(bais);
			value = stream.readObject();
		} catch (Throwable t) {
			Log.debug(this, "Error occured while deserializing " + entity.getKey().getName() + ". Will be deleted.", t);
			remove(entity.getKey());
		}

		return value;
	}

	/**
	 * @param map
	 * @return
	 */
	private Map<Object, Object> extractObject(Map<Key, Entity> map) {
		Map<Object, Object> result = new HashMap<Object, Object>();
		for (Key key : map.keySet()) {
			Object value = extractObject(map.get(key));
			if (value != null) {
				result.put(key.getName(), value);
			}
		}
		return result;
	}

}
