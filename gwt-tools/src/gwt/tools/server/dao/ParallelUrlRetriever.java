/**
 * 
 */
package gwt.tools.server.dao;

import gwt.tools.shared.Log;
import gwt.tools.shared.Util;
import gwt.tools.shared.exceptions.NetworkException;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import com.google.apphosting.api.ApiProxy.ApiDeadlineExceededException;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Wrapper for the UrlFetch API used on Google's AppEngine. It allows to
 * overcome the maximum of 10 parallel requests by automatically queuing further
 * requests and execute them as soon as a slot of the max. 10 becomes available
 * again.
 * 
 * @author Sven Buschbeck
 */
public class ParallelUrlRetriever {

	/**
	 * This number is defined by the UrlFetch API of Google's App Engine. See
	 * https
	 * ://developers.google.com/appengine/docs/java/urlfetch/overview#Requests
	 */
	public static final int MAX_PARALLEL_REQUESTS = 10;
	private static boolean DEBUG = false;

	private static LinkedList<Request> running = new LinkedList<Request>();
	private static LinkedList<Request> queued = new LinkedList<Request>();
	private static Statistics statistics = new Statistics();

	private ParallelUrlRetriever() {
	}

	private static synchronized void append(Request request) {
		statistics.urlsAdded++;
		queued.add(request);
		updateRunning();
	}

	private static synchronized void updateRunning() {
		while (running.size() < MAX_PARALLEL_REQUESTS && queued.size() > 0) {
			run(queued.removeFirst());
		}
	}

	private static synchronized void run(Request request) {
		assert running.size() < MAX_PARALLEL_REQUESTS;
		running.add(request);
		request.run();
	}

	/**
	 * If there are either requests which are currently being executed or
	 * requests still in the queue waiting to be executed.
	 * 
	 * @return True if there are unfinished requests left.
	 */
	public static boolean hasRunningRequests() {
		return Util.notEmpty(running) || Util.notEmpty(queued);
	}

	public static synchronized boolean run() {
		if (DEBUG) {
			System.out.println("ParallelUrlRetriever.run: #running=" + running.size() + "; #queued=" + queued.size());
		}
		if (hasRunningRequests()) {
			//waitForOneResult();
			broadCastResults(getFinishedRequests());
		}
		return hasRunningRequests();
	}

	/**
	 * @return The data returned from the service at the given URL interpreted
	 *         as string.
	 * @throws ExecutionException
	 *             thrown on IOException, InterrupedException or if the results
	 *             code is not 200
	 */
	private static synchronized List<Request> getFinishedRequests() {
		List<Request> finishedRequests = Util.filter(running, FILTER_FINISHED_REQUESTS, true);
		updateRunning();

		if (Util.isEmpty(finishedRequests) && hasRunningRequests()) {
			waitForOneResult();
			return getFinishedRequests();
		}
		return finishedRequests;
	}

	/**
	 * @param finishedRequests
	 */
	private static void broadCastResults(List<Request> finishedRequests) {
		statistics.urlsRetrieved += finishedRequests.size();
		for (Request request : finishedRequests) {
			request.retriever.requestFinished(request);
			Result result = request.getResult();
			statistics.bytesLoaded += result.size();
			if (result.isSuccessful()) {
				request.callback.onSuccess(result.getResult());
			} else {
				request.callback.onFailure(result.getException().getCause());
			}
		}
	}

	/**
	 * If there requests finished and not yet delivered.
	 * 
	 * @return True if there are finished requests.
	 */
	public static synchronized boolean hasResultAvailable() {
		return countAvailableResults() > 0;
	}

	private static final Util.Filter<Request> FILTER_FINISHED_REQUESTS = new Util.Filter<Request>() {

		public boolean putIn(Request object) {
			return object.future.isDone();
		}
	};

	/**
	 * Get the number of finished requests that have not yet been
	 * distributed/broadcasted to the retrievers.
	 * 
	 * @return number of finished requests (overall system)
	 */
	public static synchronized int countAvailableResults() {
		return Util.count(running, FILTER_FINISHED_REQUESTS);
	}

	private static synchronized void waitForOneResult() {
		assert running.size() + queued.size() > 0;
		if (running.size() < 1) {
			updateRunning();
		}
		running.getFirst().getResult();
	}

	/**
	 * Create an instance of this retriever for every logical unit (it's cheap
	 * to create). It allows you to keep track of packages of requests as each
	 * retriever can tell whether one of it's own requests is still pending
	 * independently of the other retrievers and the overall system.
	 * 
	 * @author Sven Buschbeck
	 * 
	 */
	public static class Retriever {

		//		private final Util.Filter<Request> FILTER_MY_DONE_REQUESTS = new Util.Filter<Request>() {
		//
		//			public boolean putIn(Request object) {
		//				return object.future.isDone() && object.retriever == getMe();
		//			}
		//		};

		private LinkedList<ParallelUrlRetriever.Request> requests = new LinkedList<ParallelUrlRetriever.Request>();

		//		private final boolean debugOutput;

		//		private boolean updaterIsRunning = false;

		//		private Retriever getMe() {
		//			return this;
		//		}

		/**
		 * Create an instance of this retriever for every logical unit (it's
		 * cheap to create). It allows you to keep track of packages of requests
		 * as each retriever can tell whether one of it's own requests is still
		 * pending independently of the other retrievers and the overall system.
		 * 
		 */
		public Retriever() {
			//			this(false);
		}

		//		public Retriever(boolean debugOutput) {
		//			this.debugOutput = debugOutput;
		//		}

		/**
		 * Put the given URL in the queue to be loaded from the web. If you need
		 * to define headers, consider using
		 * {@link #addRequest(HTTPRequest, AsyncCallback)}.
		 * 
		 * @param url
		 * @param callback
		 * @throws MalformedURLException
		 */
		public void addUrl(String url, AsyncCallback<byte[]> callback) throws MalformedURLException {
			add(new Request(new URL(url), this, callback));
		}

		/**
		 * Puts a new request in the queue but allows to provide HTTP headers
		 * and the like.
		 * 
		 * @param httpRequest
		 * @param callback
		 * @throws MalformedURLException
		 */
		public void addRequest(HTTPRequest httpRequest, AsyncCallback<byte[]> callback) {
			add(new Request(httpRequest, this, callback));
		}

		private void add(Request request) {
			requests.add(request);
			append(request);
		}

		/**
		 * Returns true if this retriever has still open/running requests. But
		 * the over-all system might still be loading data, which then has just
		 * not been requested by this retriever instance.
		 * 
		 * @return True if not all the request issued to this retriever have
		 *         been executed yet.
		 */
		public boolean isRunning() {
			return requests.size() > 0;// && hasRunningRequests(); // latter is just to make sure that we actually cause loading data
		}

		private void requestFinished(Request request) {
			requests.remove(request);
		}

		private boolean waitForResults() {
			if (isRunning()) {
				run();
			}
			return isRunning();
		}
	}

	/**
	 * Waits until all the requests are done or the timeout is reached
	 * (System.currentMillis() > timeout)
	 * 
	 * @param retrievers
	 * @param timeout
	 *            Absolute time stamp according to System.currentMillis()
	 */
	public static void waitForResults(LinkedList<Retriever> retrievers, final int timeout) {
		waitForResults(retrievers, new RepeatingCommand() {

			@Override public boolean execute() {
				return System.currentTimeMillis() < timeout;
			}
		});
	}

	/**
	 * Waits until all the requests are done or a call to the given
	 * repeatingCommand returns false.
	 * 
	 * @param retrievers
	 * @param repeatingCommand
	 */
	public static void waitForResults(LinkedList<Retriever> retrievers, RepeatingCommand repeatingCommand) {
		if (Util.isEmpty(retrievers)) {
			return;
		}
		boolean anyRunning;
		int interrupted = 0;
		do {
			anyRunning = false;
			for (Retriever retriever : retrievers) {
				anyRunning |= retriever.waitForResults();
				if (!repeatingCommand.execute()) {
					return;
				}
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// When the server gets restarted, this thread will not be killed, instead, the sleep will be interrupted - without any hint that it is expected to terminate. So we just guess, if it happened ten times then stop the loop
				if (interrupted++ > 10) {
					return;
				}
				e.printStackTrace();
			}
		} while (anyRunning);
	}

	/**
	 * 
	 */
	public static void waitForAll() {
		boolean anyRunning;
		int interrupted = 0;
		while (hasRunningRequests()) {
			run();
		}
	}

	// never used... fix up if needed
	//	public synchronized Result<ReferenceObjectType> getNextResult() {
	//		Request doneRequest = Util.filterFirst(running, FILTER_DONE_REQUESTS, true);
	//		updateRunning();
	//
	//		if (doneRequest == null) {
	//			if (hasRunningRequests()) {
	//				waitForOneResult();
	//				doneRequest = Util.filterFirst(running, FILTER_DONE_REQUESTS, true);
	//				assert doneRequest != null;
	//			}
	//			return null;
	//		}
	//		return doneRequest.getResult();
	//	}

	private static class Request {

		private static URLFetchService service = URLFetchServiceFactory.getURLFetchService();

		final URL url;
		Future<HTTPResponse> future;
		private final AsyncCallback<byte[]> callback;
		private final Retriever retriever;

		private byte[] responseContent;

		private HTTPRequest httpRequest;

		/**
		 * @param retriever
		 * @param refObject
		 * @param future
		 */
		private Request(URL url, Retriever retriever, AsyncCallback<byte[]> callback) {
			this(url, null, retriever, callback);
		}

		private Request(HTTPRequest httpRequest, Retriever retriever, AsyncCallback<byte[]> callback) {
			this(null, httpRequest, retriever, callback);
		}

		private Request(URL url, HTTPRequest httpRequest, Retriever retriever, AsyncCallback<byte[]> callback) {
			this.callback = callback;
			this.retriever = retriever;
			this.url = url;
			this.httpRequest = httpRequest;
		}

		Result getResult() {
			ExecutionException exception = null;
			byte[] result = null;
			try {
				result = getRawResult();
			} catch (ExecutionException e) {
				exception = e;
			} catch (ApiDeadlineExceededException e) {
				exception = new ExecutionException("URL Fetch API Timeout", e);
			} catch (java.net.SocketTimeoutException e) {
				exception = new ExecutionException("URL Fetch API Timeout", e);
			}
			return new Result(result, exception);
		}

		/**
		 * 
		 * @return
		 * @throws ExecutionException
		 * @throws java.net.SocketTimeoutException
		 *             gets thrown when running somehow - despite that compiler
		 *             says it could not...
		 */
		private byte[] getRawResult() throws ExecutionException, java.net.SocketTimeoutException {
			if (responseContent != null) {
				return responseContent;
			}
			HTTPResponse response = null;
			try {
				response = future.get();
			} catch (InterruptedException e) {
				throw new ExecutionException("Error while retrieving results of request. URL=" + url, e);
			}
			assert response != null;
			final int responseCode = response.getResponseCode();
			if (responseCode != 200) {
//				throw new ExecutionException("Retrieving results failed. Response code=" + responseCode
//					+ "; URL=" + url, new IllegalStateException("The server responded with error code "
//					+ responseCode + ", not 200 as expected."));
				throw new ExecutionException("Retrieving results failed. Response code=" + responseCode
						+ "; URL=" + url, new NetworkException(200, responseCode));
			}
			return responseContent = response.getContent();
		}

		void run() {
			if (httpRequest != null) {
				Log.debug(this, ".run: url", httpRequest.getURL());
				future = service.fetchAsync(httpRequest);
			} else {
				Log.debug(this, ".run: url", url);
				future = service.fetchAsync(url);
			}
		}
	}

	/**
	 * A Result object contains the outcome of an URL request, i.e. the content
	 * or an exception.
	 * 
	 * @author Sven Buschbeck
	 * 
	 */
	public static class Result {

		private final ExecutionException exception;
		private final byte[] result;
		private String resultString;

		private Result(byte[] result, ExecutionException exception) {
			this.result = result;
			this.exception = exception;
		}

		/**
		 * @return
		 */
		public int size() {
			return hasResult() ? getResult().length : 0;
		}

		/**
		 * If null indicates that the execution went just fine.
		 * 
		 * @return an exception that occurred during the request or null
		 */
		public ExecutionException getException() {
			return exception;
		}

		/**
		 * The raw result as retrieved from the socket/server as byte array.
		 * 
		 * @return bytes returned by server.
		 */
		public byte[] getResult() {
			return result;
		}

		/**
		 * The returned bytes interpreted as a string
		 * 
		 * @return new String(getResult());
		 */
		public String getResultAsString() {
			return (resultString == null) ? resultString = new String(getResult()) : resultString;
		}

		/**
		 * True if there was no exception during the execution.
		 * 
		 * @return getException()==null
		 */
		public boolean isSuccessful() {
			return exception == null;
		}

		public boolean hasResult() {
			return getResult() != null;
		}

		//		public boolean seemsLikeANetworkError() {
		//			if (!isSuccessful() && getException().getCause() != null) {
		//				Throwable exception = getException().getCause();
		//				return (exception instanceof IOException || exception instanceof NetworkException);
		//			}
		//			return false;
		//		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override public String toString() {
			return "ParallelUrlRetriever.Result("
				+ isSuccessful()
				+ (isSuccessful() ? ", " + getResult().length + " bytes, " + getResultAsString() : getException()
					.getClass() + ": " + getException().getMessage()) + ")";
		}
	}

	/**
	 * 
	 */
	public static Statistics getStatistics() {
		return statistics.clone();
	}

	public static class Statistics {

		public int urlsAdded;
		public int urlsRetrieved;
		public int bytesLoaded;

		public Statistics clone() {
			Statistics clone = new Statistics();
			clone.urlsAdded = urlsAdded;
			clone.urlsRetrieved = urlsRetrieved;
			clone.bytesLoaded = bytesLoaded;
			return clone;
		}

	}

}
