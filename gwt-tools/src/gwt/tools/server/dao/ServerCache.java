/**
 * 
 */
package gwt.tools.server.dao;

import gwt.tools.shared.Cache;
import net.sf.jsr107cache.CacheException;

/**
 * Class-safe wrapper for JCache using GAE's cache implementation.
 * 
 * @author Sven Buschbeck
 * 
 */
public class ServerCache extends gwt.tools.shared.CommonCache {

	private static gwt.tools.shared.CommonCache cacheInstance;

	private ServerCache() {
		super(createCache());
	}

	public static gwt.tools.shared.CommonCache getInstance() {
		if (cacheInstance == null) {
			cacheInstance = new ServerCache();
		}
		return cacheInstance;
	}

	static Cache createCache() {
		try {
			//			Map<String, Object> props = new HashMap<String, Object>();
			//			props.put(GCacheFactory.EXPIRATION_DELTA, 3600);
			//			return CacheManager.getInstance().getCacheFactory().createCache(props);
			return new CachedDataStore(false);
		} catch (CacheException e) {
			e.printStackTrace();
		}
		return null;
	}

}
