/**
 * 
 */
package gwt.tools.server.dao.cache;

import net.sf.jsr107cache.CacheEntry;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public interface CacheWithEntryRetrieval {

	CacheEntry getCacheEntry(Object key);

}
