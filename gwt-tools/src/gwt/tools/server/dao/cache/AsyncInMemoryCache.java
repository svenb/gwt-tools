/**
 * 
 */
package gwt.tools.server.dao.cache;

import gwt.tools.shared.Cache;
import gwt.tools.shared.Log;

import java.util.Collection;
import java.util.Map;

import com.google.appengine.api.memcache.AsyncMemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class AsyncInMemoryCache implements Cache {

	AsyncMemcacheService asyncCache = null;

	/**
	 * 
	 */
	public AsyncInMemoryCache(String nameSpace) {
		asyncCache = MemcacheServiceFactory.getAsyncMemcacheService(nameSpace);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.shared.Cache#put(java.lang.Object, java.lang.Object)
	 */
	@Override public void put(Object key, Object value) {
		asyncCache.put(key, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.shared.Cache#putAll(java.util.Map)
	 */
	@Override public void putAll(Map<? extends Object, ? extends Object> keyValuePairs) {
		asyncCache.putAll(keyValuePairs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.shared.Cache#get(java.lang.Object)
	 */
	@Override public Object get(Object key) {
		try {
			return asyncCache.get(key).get();
		} catch (Exception e) {
			//			throw new ExecutionException(e);
			Log.warn(this, "get($key) threw and exception", e, key);
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.shared.Cache#getAll(java.util.Collection)
	 */
	@Override public Map<Object, Object> getAll(Collection<Object> keys) {
		try {
			return asyncCache.getAll(keys).get();
		} catch (Exception e) {
			//			throw new ExecutionException(e);
			Log.warn(this, "getAll($key) threw and exception", e, keys);
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.shared.Cache#remove(java.lang.Object)
	 */
	@Override public void remove(Object key) {
		asyncCache.put(key, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.shared.Cache#clear()
	 */
	@Override public void clear() {
		asyncCache.clearAll();
	}

}
