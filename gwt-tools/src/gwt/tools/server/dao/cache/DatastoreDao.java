/**
 * 
 */
package gwt.tools.server.dao.cache;

import java.util.Collection;
import java.util.Map;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public interface DatastoreDao {

	/**
	 * @param uri
	 * @return
	 */
	Object get(Object key);

	/**
	 * @param toGet
	 * @return
	 */
	Map<Object, Object> getAll(Collection<Object> keys);

	/**
	 * @param map
	 * @param b
	 */
	void putAll(Map<? extends Object, ? extends Object> uriToValueMap, boolean asynchronous);

	/**
	 * @param uri
	 */
	void remove(Object key);

	/**
	 * @param uri
	 * @param value
	 * @param b
	 */
	void put(Object key, Object value, boolean asynchronous);

	/**
	 * Clear all instances from the current namespace
	 */
	void clear();

}
