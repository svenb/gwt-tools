/**
 * 
 */
package gwt.tools.server.dao.cache;

import gwt.tools.shared.Cache;
import gwt.tools.shared.Log;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheManager;

import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class SyncInMemoryCache implements Cache, CacheWithEntryRetrieval {

	private net.sf.jsr107cache.Cache impl;

	public SyncInMemoryCache(String nameSpace) throws CacheException {
		this(nameSpace, 3600 * 1000);
	}

	public SyncInMemoryCache(String nameSpace, int expiration) throws CacheException {
		Map<String, Object> props = new HashMap<String, Object>();
		// FIXME use GCacheFactory.NAMESPACE instead of own namespace-fiddling!
		props.put(GCacheFactory.EXPIRATION_DELTA_MILLIS, expiration);
		props.put(GCacheFactory.NAMESPACE, nameSpace);
		impl = CacheManager.getInstance().getCacheFactory().createCache(props);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.shared.Cache#put(java.lang.Object, java.lang.Object)
	 */
	@Override public void put(Object key, Object value) {
		impl.put(key, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.shared.Cache#putAll(java.util.Map)
	 */
	@Override public void putAll(Map<? extends Object, ? extends Object> keyValuePairs) {
		impl.putAll(keyValuePairs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.shared.Cache#get(java.lang.Object)
	 */
	@Override public Object get(Object key) {
		return impl.get(key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gwt.tools.server.dao.cache.CacheWithEntryRetrieval#getCacheEntry(java
	 * .lang.Object)
	 */
	@Override public CacheEntry getCacheEntry(Object key) {
		return impl.getCacheEntry(key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.shared.Cache#getAll(java.util.Collection)
	 */
	@Override public Map<Object, Object> getAll(Collection<Object> keys) {
		Map<Object, Object> cached = null;
		try {
			cached = impl.getAll(keys);
		} catch (Throwable t) {
			// might be a com.google.appengine.api.memcache.InvalidValueException when the DTOs have been changed, thus the deserialization from the cache fails
			// or simply a CacheException
			Log.debug(this, "getAll(): failed to get all data from underlaying cache (will try for each now). error="
				+ t.getMessage());
			cached = new HashMap<Object, Object>();
			for (Object key : keys) {
				cached.put(key, get(key));
			}
		}
		return cached;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.shared.Cache#remove(java.lang.Object)
	 */
	@Override public void remove(Object key) {
		impl.remove(key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.shared.Cache#clear()
	 */
	@Override public void clear() {
		impl.clear();
	}

}
