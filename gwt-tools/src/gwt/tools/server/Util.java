package gwt.tools.server;

import gwt.tools.shared.Constants;
import gwt.tools.shared.Log;
import gwt.tools.shared.Log.FormatOnDemand;
import gwt.tools.shared.exceptions.NetworkException;
import gwt.tools.shared.exceptions.ParseException;
import gwt.tools.shared.exceptions.ServiceNotAvailableException;
import gwt.tools.shared.exceptions.TimeoutException;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;

import javax.xml.bind.JAXB;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class Util extends gwt.tools.shared.Util {

	/**
	 * 
	 */
	private static final int DEFAULT_MAX_RETRY = 3;

	// only server specific stuff, especially methods that require server libs
	// everything else (as much as possible) should be part of the shared Util

	public static URL validateAndConvertToUrl(String uri) throws MalformedURLException {
		return validateAndConvertToUrl(uri, null, null);
	}

	public static URL validateAndConvertToUrl(String uri, String uriPrefix, String uriSuffix)
		throws MalformedURLException {
		String urlString = uri;
		if (uriPrefix != null && uriPrefix.length() > 0 && !urlString.startsWith(uriPrefix)) {
			urlString = uriPrefix + urlString;
		}
		if (uriSuffix != null && uriSuffix.length() > 0 && !urlString.endsWith(uriSuffix)) {
			urlString += uriSuffix;
		}

		URL url;
		//		try {
		url = new URL(urlString);
		//		} catch (Exception e) {
		//			throw new InvalidUrlException("server.Util.validateAndConvertToUrl(uri=" + uri + ", prefix=" + uriPrefix
		//				+ ", suffix=" + uriSuffix + "): Invalid web service URL generated: " + urlString);
		//		}
		return url;
	}

	/**
	 * Loads XML from the given URL (build by concatenating prefix+url+suffix)
	 * and converts it into an object of the given class using JAXB
	 * 
	 * @param <T>
	 *            Class parameter the service XML should be converted in.
	 * @param url
	 *            Base URL of service.
	 * @param prefix
	 *            Added in front of given URL.
	 * @param suffix
	 *            Added at the end of given URL.
	 * @param theClass
	 *            Class setting T
	 * @param serviceName
	 *            Name is used to generate meaningful error messages
	 * @return Object of class T representing the web service call results.
	 * @throws ParseException
	 * @throws ServiceNotAvailableException
	 *             If error occurs while loading the XML data or parsing it.
	 */
	public static <T> T loadObject(String url, String prefix, String suffix, Class<T> theClass, String serviceName)
		throws NetworkException, MalformedURLException, ParseException {
		return loadObject(Util.validateAndConvertToUrl(url, prefix, suffix), theClass, serviceName);
	}

	public static <T> T loadObject(final String url, final Class<T> theClass) throws MalformedURLException,
		IOException, NetworkException, ParseException {
		return loadObject(url, theClass, 3);
	}

	public static <T> T loadObject(String url, Class<T> resultClass, int maxRepeat) throws IOException,
		NetworkException, MalformedURLException, ParseException {
		return loadObject(url, resultClass, maxRepeat, 500, 1000);
	}

	public static <T> T loadObject(String url, Class<T> resultClass, int maxRepeat, int minWait, int maxWait)
		throws IOException, NetworkException, MalformedURLException, ParseException {
		return loadObject(new URL(url), resultClass, url, maxRepeat, minWait, maxWait);
	}

	/**
	 * Loads XML from the given URL and converts it into an object of the given
	 * class using JAXB
	 * 
	 * @param <T>
	 *            Class parameter the service XML should be converted in.
	 * @param url
	 *            Complete and validated URL of service.
	 * @param theClass
	 *            Class setting T
	 * @param serviceName
	 *            Name is used to generate meaningful error messages
	 * @return Object of class T representing the web service call results.
	 * @throws ParseException
	 * @throws ServiceNotAvailableException
	 *             If error occurs while loading the XML data or parsing it.
	 */
	public static <T> T loadObject(final URL url, final Class<T> theClass, final String serviceName)
		throws NetworkException, ParseException {
		return loadObject(url, theClass, serviceName, DEFAULT_MAX_RETRY, 500, 1000);
	}

	/**
	 * Loads XML from the given URL and converts it into an object of the given
	 * class using JAXB
	 * 
	 * @param <T>
	 *            Class parameter the service XML should be converted in.
	 * @param url
	 *            Complete and validated URL of service.
	 * @param theClass
	 *            Class setting T
	 * @param serviceName
	 *            Name is used to generate meaningful error messages
	 * @param maxRepeat
	 *            Maximum time the system will retry to load the object before
	 *            it fails.
	 * @return Object of class T representing the web service call results.
	 * @throws IOException
	 *             Service broken
	 * @throws TimeoutException
	 *             Call timed out
	 */
	public static <T> T loadObject(final URL url, final Class<T> theClass, final String serviceName, int maxRepeat,
		int minWait, int maxWait) throws NetworkException, ParseException {
		T object;
		NetworkException error = null;
		InputStream inputStream = null;
		if (theClass.isAssignableFrom(String.class)) {
			Log.trace(Util.class, ".loadObject: load as string from URL=" + url);
			return (T) urlContentToString(url);
		}
		for (int retry = 0; retry < maxRepeat; retry++) {
			try {
				inputStream = url.openStream();
				object = JAXB.unmarshal(inputStream, theClass);
				inputStream.close();
				return object;
			} catch (SocketTimeoutException timeoutE) {
				error = new NetworkException("Loading URL timed out. URL=" + url, timeoutE);
			} catch (IOException e) {
				e.printStackTrace();
				error = new NetworkException("Loading URL failed. URL=" + url, e);
			} catch (javax.xml.bind.DataBindingException e) {
				throw new ParseException("Could not parse XML from URL=" + url, e);
			}
			Log.trace(Util.class, ".loadObject: retry #" + (retry + 1) + " for URL=" + url);
			try {
				// wait a bit before retrying
				Thread.sleep(minWait + (long) (Math.random() * (maxWait - minWait)));
			} catch (InterruptedException e) {
				Log.trace(Util.class, ".loadObject.waitForRetry: Interrupt Exeption.");
			}
		}
		assert error != null;
		// The stream does not get closed if a parsing error occurs.
		try {
			inputStream.close();
		} catch (Throwable t2) {
		}
		//		if (error instanceof java.net.SocketTimeoutException) {
		String message = "Timeout persisted after loading object for " + DEFAULT_MAX_RETRY + " times from URL=" + url
			+ "; serviceName=" + serviceName + "; class=" + theClass;
		Log.warn(Util.class, ".loadObject: " + message);
		//		} else {
		//			Log.warn("Util.loadObject: error persisted while trying " + maxRepeat + " times loading URL=" + url
		//				+ "; serviceName=" + serviceName + "; class=" + theClass + "; message=" + error.getMessage());
		//		}
		//		throw new ServiceNotAvailableException("Service \"" + serviceName + "\" not available.\n\nError message: "
		//			+ error.getMessage(), url.toExternalForm(), error);
		throw error;
	}

	public static String urlContentToString(String url) throws NetworkException, MalformedURLException {
		return urlContentToString(url, null);
	}

	public static String urlContentToString(String url, int attempts) throws NetworkException, MalformedURLException {
		return urlContentToString(new URL(url), attempts);
	}

	public static String urlContentToString(URL url) throws NetworkException {
		return urlContentToString(url, DEFAULT_MAX_RETRY);
	}

	public static String urlContentToString(URL url, int attempts) throws NetworkException {
		return urlContentToString(url, null, attempts);
	}

	public static String urlContentToString(String url, String postData) throws NetworkException, MalformedURLException {
		return urlContentToString(new URL(url), postData, DEFAULT_MAX_RETRY);
	}

	public static String urlContentToString(String url, String postData, String contentType, String acceptType)
		throws NetworkException, MalformedURLException {
		return urlContentToString(new URL(url), postData, DEFAULT_MAX_RETRY, contentType, acceptType);
	}

	public static String urlContentToString(URL url, String postData, int attempts) throws NetworkException {
		return urlContentToString(url, postData, attempts, null, null);
	}

	public static String urlContentToString(URL url, String postData, int attempts, String contentType,
		String acceptType) throws NetworkException {
		NetworkException error = null;
		String urlString = url.toExternalForm();
		if (urlString.length() > Constants.MAX_URL_LENGTH && urlString.indexOf("?") > -1) {
			// switch to post mode
			String[] urlParts = urlString.split("\\?");
			try {
				url = new URL(urlParts[0]);
				postData = urlParts[1];
				contentType = Constants.URLENCODED_MIME_TYPE;
				Log.debug(Util.class, "urlContentToString: URL too long, switching to POST mode; url,data", url,
					postData);
			} catch (MalformedURLException e) {
				throw new NetworkException("URL too long.", e);
			}
		}
		for (int attempt = 0; attempt < attempts; attempt++) {
			try {
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				if (notEmpty(contentType)) {
					connection.setRequestProperty("Content-Type", contentType);
				}
				if (notEmpty(acceptType)) {
					connection.setRequestProperty("Accept", acceptType);
				}
				if (postData != null) {
					connection.setDoOutput(true);
					connection.setDoInput(true);
					//					conn.setRequestProperty("Content-Type", "application/xml");
					//					conn.setRequestProperty("Content-Type", "application/json");
					connection.setRequestMethod("POST");

					DataOutputStream stream = new DataOutputStream(connection.getOutputStream());
					stream.write(postData.getBytes("UTF-8"));
				} else {
				}
				int code = connection.getResponseCode();
				if (code != 200) {
					connection.disconnect();
					throw new IOException("server.Util.urlContentToString(" + url + "): code=" + code + "; data="
						+ postData + "; message=" + readAll(connection.getErrorStream()));
				}
				return readAll(connection.getInputStream());

			} catch (IOException t) {
				error = new NetworkException("Failed to load data from URL=" + url + "; postData=" + postData
					+ "; error=" + t.getMessage(), t);
				try {
					Thread.sleep((long) (100 + Math.random() * 500));
				} catch (InterruptedException e) {
				}
				Log.trace(Util.class, "urlContentToString: attempt #x failed. x", attempt);
			}
		}
		throw error;
	}

	public static String readAll(InputStream inputStream) throws IOException {
		return readAll(inputStream, true);
	}

	public static String readAll(InputStream inputStream, boolean closeStreamWhenDone) throws IOException {
		if (inputStream == null) {
			return null;
		}
		BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
		String inputLine;
		String result = "";
		while ((inputLine = in.readLine()) != null) {
			result += inputLine;
		}
		if (closeStreamWhenDone) {
			in.close();
		}
		return result;
	}

	public static boolean isValidUrl(String url) {
		try {
			new URL(url);
		} catch (Throwable t) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see eu.glocalProject.shared.ui.Util#decodeUriParameter(java.lang.String)
	 */
	public static String decodeUriParameter(String encodedUriParameter) {
		try {
			return URLDecoder.decode(encodedUriParameter, "UTF-8");
		} catch (Exception e) {
			return encodedUriParameter;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see eu.glocalProject.shared.ui.Util#encodeUriParameter(java.lang.String)
	 */
	public static String encodeUriParameter(String uriParameter) {
		try {
			return URLEncoder.encode(uriParameter, "UTF-8");
		} catch (Exception e) {
			return uriParameter;
		}
	}

	protected static Coder getUriCoder() {
		return new Coder() {

			@Override public String encode(String data) {
				return encodeUriParameter(data);
			}

			@Override public String decode(String data) {
				return decodeUriParameter(data);
			}
		};
	}

	public static String encode(Long decoded, String symbols) {
		if (decoded == null) return null;
		try {
		    final int length = symbols.length();
		    StringBuilder sb = new StringBuilder();
		    while (decoded != 0) {
		        sb.append(symbols.charAt((int) (decoded % length)));
		        decoded /= length;
		    }
		    return sb.reverse().toString();
		} catch (Exception e) { e.printStackTrace(); }
		return null;
	}
	
	/**
	 * PUTs an object to a RESTful service using JAXB
	 * 
	 * @param url
	 * @param objectToSave
	 * @return true if the PUT action succeeded.
	 * @throws ServiceNotAvailableException
	 */
	@Deprecated public static <T> boolean saveObject(final URL url, final T objectToSave)
		throws ServiceNotAvailableException {
		try {
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestProperty("Content-Type", "application/xml");
			conn.setRequestMethod("PUT");

			DataOutputStream stream = new DataOutputStream(conn.getOutputStream());
			JAXB.marshal(objectToSave, stream);

			Log.debug(Util.class, "saveObject: code, response ", conn.getResponseCode(), conn.getResponseMessage());
			return true;
		} catch (IOException e) {
			throw new ServiceNotAvailableException("Entity could not be stored in repository. Entity's URI="
				+ url.toExternalForm(), "Util.saveObject", e);
		}
	}

	/**
	 * Performs GET on the given URL and returns the results wrapped in an
	 * X-path parser, thus, the URL must return valid XML.
	 * 
	 * @param serviceUrl
	 * @param validationField
	 *            an xpath expression, if the returning value is a positive
	 *            boolean like "true" or "yes" then the request is considered
	 *            successful; example for amazon WS:
	 *            "//Items/Request/IsValid/text()"
	 * @return the parser to work on the XML returned from the given URL
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws TimeoutException
	 */
	public static XpathParser loadXml(String serviceUrl, String validationField) throws ParserConfigurationException,
		SAXException, IOException, TimeoutException {
		Log.debug(Util.class, "loadXml: serviceUrl", serviceUrl);
		final XpathParser xmlDoc;
		try {
			xmlDoc = new XpathParser(new URL(serviceUrl));
		} catch (SocketTimeoutException e) {
			throw new TimeoutException("Loading XML timed out.");
		}
		String successful = xmlDoc.xpathValue(validationField);
		if (Util.parseBoolean(successful)) {
			Log.debug(Util.class, "loadXml:  success; xml", new FormatOnDemand() {

				@Override public String format() {
					return xmlDoc.toString();
				}
			});
			return xmlDoc;
		}
		Log.debug(Util.class, "loadXml: request failed. successful", successful);
		return null;
	}

}
