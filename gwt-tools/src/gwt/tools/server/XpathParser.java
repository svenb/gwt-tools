/**
 * 
 */
package gwt.tools.server;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class XpathParser {

	private Document doc;
	private DocumentBuilder builder;

	public XpathParser(String xmlData) throws ParserConfigurationException, SAXException, IOException {
		this();
		doc = builder.parse(new InputSource(new StringReader(xmlData)));
	}

	public XpathParser(byte[] xmlData) throws ParserConfigurationException, SAXException, IOException {
		this(new ByteArrayInputStream(xmlData));
	}

	public XpathParser(InputStream stream) throws ParserConfigurationException, SAXException, IOException {
		this();
		doc = builder.parse(stream);
	}

	public XpathParser(URL url) throws ParserConfigurationException, SAXException, IOException {
		this();
		doc = builder.parse(url.openStream());
	}

	private XpathParser() throws ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		builder = factory.newDocumentBuilder();
	}

	public NodeList xpathNodeList(String xpath) {
		return xpathNodeList(xpath, doc);
	}

	public NodeList xpathNodeList(String xpath, Object root) {
		XPath xpath_ = XPathFactory.newInstance().newXPath();
		try {
			XPathExpression expr = xpath_.compile(xpath);
			NodeList nodeList = (NodeList) expr.evaluate(root, XPathConstants.NODESET);
			//			System.out.println("XmlDoc.xpathNodeList: xpath=" + xpath + "; #results=" + nodeList.getLength());
			return nodeList;
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Node> xpathNodes(String xpath) {
		return xpathNodes(xpath, doc);
	}

	public List<Node> xpathNodes(String xpath, Object root) {
		try {
			NodeList nodelist = xpathNodeList(xpath, root);
			if (nodelist != null) {
				List<Node> nodes = new LinkedList<Node>();
				for (int index = 0; index < nodelist.getLength(); index++) {
					nodes.add(nodelist.item(index));
				}
				return nodes;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String xpathValue(String xpath) {
		return xpathValue(xpath, doc);
	}

	public String xpathValue(String xpath, Object root) {
		List<String> xpathValues = xpathValues(xpath, root);
		return (xpathValues.size() > 0) ? xpathValues.get(0) : null;
	}

	public List<String> xpathValues(String xpath) {
		return xpathValues(xpath, doc);
	}

	public List<String> xpathValues(String xpath, Object root) {
		try {
			NodeList nodelist = xpathNodeList(xpath, root);
			if (nodelist != null) {
				List<String> values = new LinkedList<String>();
				for (int index = 0; index < nodelist.getLength(); index++) {
					//					System.out.println(index + ":" + nodelist.item(index).getTextContent());
					values.add(nodelist.item(index).getTextContent());
				}
				return values;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override public String toString() {
		return doc.toString();
	}
}
