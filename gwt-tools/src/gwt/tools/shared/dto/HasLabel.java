/**
 * 
 */
package gwt.tools.shared.dto;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public interface HasLabel {

	String getLabel();

}
