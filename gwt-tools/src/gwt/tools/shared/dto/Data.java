/**
 * 
 */
package gwt.tools.shared.dto;

import java.io.Serializable;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public interface Data extends Serializable {

	/**
	 * Returns a reasonable hash code of the given object identifying it.
	 * 
	 * @return hash code as int
	 */
	public int getHashCode();

	/**
	 * Return true if the given data is equal to this one.
	 * 
	 * @param data
	 * @return true if equal
	 */
	public boolean equals(Object o);
}
