/**
 * 
 */
package gwt.tools.shared.dto;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public interface ExtendedToString {

	String toString(boolean extended);

}
