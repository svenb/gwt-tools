/**
 * 
 */
package gwt.tools.shared.dto;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public enum LoadLevel {
	STUB, MINIMAL, NORMAL, FULL;

	/**
	 * @param minimal2
	 * @return
	 */
	public boolean higherThan(LoadLevel level) {
		return level == null || this.ordinal() > level.ordinal();
	}

	public boolean lowerThan(LoadLevel level) {
		return level != null && this.ordinal() < level.ordinal();
	}

	/**
	 * @param loadLevel
	 * @param addAmazonUrl
	 * @return
	 */
	public static LoadLevel min(LoadLevel loadLevel1, LoadLevel loadLevel2) {
		if (loadLevel1 == null) {
			if (loadLevel2 == null) {
				return null;
			}
			return loadLevel2;
		} else if (loadLevel2 == null) {
			return loadLevel1;
		}
		return (loadLevel1.lowerThan(loadLevel2)) ? loadLevel1 : loadLevel2;
	}

	/**
	 * @return
	 */
	public static LoadLevel getLowest() {
		return LoadLevel.values()[0];
	}

	/**
	 * @param level
	 * @return
	 */
	public boolean higherOrEqualThan(LoadLevel level) {
		return !lowerThan(level);
	}

	/**
	 * @param level
	 * @return
	 */
	public boolean lowerOrEqualThan(LoadLevel level) {
		return !higherThan(level);
	}

	/**
	 * @return
	 */
	public LoadLevel previous() {
		return LoadLevel.values()[(this.ordinal() > 0) ? this.ordinal() - 1 : 0];
	}

	/**
	 * @param freshEntity
	 * @param oldEntity
	 * @return
	 */
	public static int compare(HasLoadLevel a, HasLoadLevel b) {
		if ((a == null && b == null) || (a.getLoadLevel() == null && b.getLoadLevel() == null)) {
			return 0;
		}
		if (a.getLoadLevel() == null) {
			return 1;
		}
		if (b.getLoadLevel() == null) {
			return -1;
		}
		if (a.getLoadLevel().equals(b.getLoadLevel())) {
			return 0;
		} else if (a.getLoadLevel().lowerThan(b.getLoadLevel())) {
			return -1;
		} else {
			return 1;
		}
	}
}
