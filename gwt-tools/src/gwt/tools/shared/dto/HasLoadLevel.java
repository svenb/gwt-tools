/**
 * 
 */
package gwt.tools.shared.dto;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public interface HasLoadLevel {

	LoadLevel getLoadLevel();

}
