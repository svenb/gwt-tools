/**
 * 
 */
package gwt.tools.shared.dto;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public interface HasId {

	String getId();
	//
	//	public abstract String toString(List<String> strings);
	//
	//	public abstract String toString();

}
