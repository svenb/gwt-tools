/**
 * 
 */
package gwt.tools.shared.dto;

import java.io.Serializable;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class KeyValuePair<KeyType, ValueType> implements Serializable {

	public KeyType key;
	public ValueType value;

	public KeyValuePair() {
	}

	public KeyValuePair(KeyType key, ValueType value) {
		this.key = key;
		this.value = value;
	}
}
