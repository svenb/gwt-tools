/**
 * 
 */
package gwt.tools.shared;

import gwt.tools.shared.DateTimeFormat.PredefinedFormat;
import gwt.tools.shared.dto.ExtendedToString;
import gwt.tools.shared.dto.HasId;
import gwt.tools.shared.dto.HasLabel;
import gwt.tools.shared.dto.KeyValuePair;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.regexp.shared.RegExp;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class Util {

	/**
	 * 
	 */
	private static final String DEFAULT_PAIR_SEPARATOR = "\n";
	/**
	 * 
	 */
	private static final String DEFAULT_KEY_VALUE_SEPARATOR = "=";
	private static RegExp urlValidator;
	private static RegExp urlPlusTldValidator;

	//	private static Map<String, RegExp> patternCache = new HashMap<String, RegExp>();
	//
	//	private static RegExp getPattern(String pattern) {
	//		if (!patternCache.containsKey(pattern)) {
	//			RegExp compiled = RegExp.compile(pattern);
	//			patternCache.put(pattern, compiled);
	//			return compiled;
	//		}
	//		return patternCache.get(pattern);
	//	}

	public interface Coder {
		String encode(String data);

		String decode(String data);
	}
	
	public static <T> List<T> trim(List<T> list, int maxLength) {
		ListIterator<T> iterator = list.listIterator(size(list)-1);
		while (notEmpty(list) && size(list) > maxLength)  {
			iterator.remove();
			if (iterator.hasPrevious()) iterator.previous();
		}
		return list;
	}

	/*
	 * Join methods are extracted from Apache Common Lang libraries and
	 * transformed to suit GWT
	 */
	/*
	 * Licensed to the Apache Software Foundation (ASF) under one or more
	 * contributor license agreements. See the NOTICE file distributed with this
	 * work for additional information regarding copyright ownership. The ASF
	 * licenses this file to You under the Apache License, Version 2.0 (the
	 * "License"); you may not use this file except in compliance with the
	 * License. You may obtain a copy of the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations
	 * under the License.
	 */
	public static String join(Collection<?> objects) {
		return join(objects, ", ");
	}

	public static String join(Collection<?> objects, String glue) {
		return join(objects, glue, false);
	}
	public static String join(Collection<?> objects, String glue, boolean skipNull) {
		return join(((objects != null) ? objects.toArray() : null), glue, skipNull);
	}

	public static String join(Object[] objects) {
		return join(objects, null);
	}

	public static String join(Object[] objects, String glue) {
		return join(objects, glue, false);
	}
	public static String join(Object[] objects, String glue, boolean skipNull) {
		return join(objects, glue, 0, (objects == null) ? 0 : objects.length, skipNull);
	}

	public static String join(Object[] array, String separator, int startIndex, int endIndex, boolean skipNull) {
		if (array == null) {
			return null;
		}
		int bufSize = (endIndex - startIndex);
		if (bufSize <= 0) {
			return "";
		}

		bufSize *= ((array[startIndex] == null ? 16 : array[startIndex].toString().length()) + 1);
		StringBuilder buf = new StringBuilder(bufSize);
		final boolean gotSeparator = notEmpty(separator);

		for (int i = startIndex; i < endIndex; i++) {
			if (gotSeparator && i > startIndex) {
				buf.append(separator);
			}
			if (array[i] == null && skipNull) continue;
//			buf.append((array[i] != null) ? array[i] : "null");
			buf.append(array[i]);
		}
		return buf.toString();
	}
	
	public static String join(Map<? extends Object, ? extends Object> map) {
		return join(map, DEFAULT_KEY_VALUE_SEPARATOR, DEFAULT_PAIR_SEPARATOR);
	}

	public static String join(Map<? extends Object, ? extends Object> map, String keyValueSeparator, String pairSeparator) {
		List<String> pairs = new LinkedList<String>();
		for (Object key : map.keySet()) {
			pairs.add(key + keyValueSeparator + map.get(key));
		}
		return Util.join(pairs, pairSeparator);
	}

	/*
	 * End of Apache Common Lang import
	 */

	public static String joinAll(Object... objects) {
		return joinAll(", ", objects);
	}

	public static String joinAll(String glue, Object... objects) {
		return join(objects, glue);
	}

	public static String joinKeyValuePairs(Map<String, String> map) {
		return joinKeyValuePairs(map, DEFAULT_KEY_VALUE_SEPARATOR, DEFAULT_PAIR_SEPARATOR);
	}

	public static String joinKeyValuePairs(Map<String, String> map, String pairSeparator) {
		return joinKeyValuePairs(map, DEFAULT_KEY_VALUE_SEPARATOR, pairSeparator);
	}

	public static String joinKeyValuePairs(Map<String, String> map, String keyValueSeparator, String pairSeparator) {
		List<String> pairs = new LinkedList<String>();
		for (String key : map.keySet()) {
			pairs.add(key + keyValueSeparator + map.get(key));
		}
		return join(pairs, pairSeparator);
	}

	public static <EnumType extends Enum<EnumType>> String joinKeyValuePairs(Map<EnumType, String> map,
		Class<EnumType> enumaration) {
		return joinKeyValuePairs(map, enumaration, DEFAULT_KEY_VALUE_SEPARATOR, DEFAULT_PAIR_SEPARATOR);
	}

	public static <EnumType extends Enum<EnumType>> String joinKeyValuePairs(Map<EnumType, String> map,
		Class<EnumType> enumaration, String keyValueSeparator, String pairSeparator) {
		Map<String, String> temp = new HashMap<String, String>();
		for (EnumType key : map.keySet()) {
			temp.put(key.name(), map.get(key));
		}
		return joinKeyValuePairs(temp, keyValueSeparator, pairSeparator);
	}

	/**
	 * Splits a string into key-value-pairs. "\n" separates pairs and "=" keys
	 * and values.
	 * 
	 * @param keyValuePairs
	 * @return Map of all key->values found in the string.
	 */
	public static Map<String, String> parseKeyValuePairs(String keyValuePairs) {
		return parseKeyValuePairs(keyValuePairs, DEFAULT_KEY_VALUE_SEPARATOR, DEFAULT_PAIR_SEPARATOR);
	}

	/**
	 * Splits a string into key-value-pairs according to the given pair
	 * separator and "=" as separator of each key and value.
	 * 
	 * @param keyValuePairs
	 * @param pairSeparator
	 * @return Map of all key->values found in the string.
	 */
	public static Map<String, String> parseKeyValuePairs(String keyValuePairs, String pairSeparator) {
		return parseKeyValuePairs(keyValuePairs, DEFAULT_KEY_VALUE_SEPARATOR, pairSeparator);
	}

	/**
	 * Splits a string into key-value-pairs according to the given separators.
	 * 
	 * @param keyValuePairs
	 * @param keyValueSeparator
	 * @param pairSeparator
	 * @return Map of all key->values found in the string.
	 */
	public static Map<String, String> parseKeyValuePairs(String keyValuePairs, String keyValueSeparator,
		String pairSeparator) {
		Map<String, String> map = new HashMap<String, String>();

		String[] fragments = Util.split(keyValuePairs, pairSeparator);
		if (keyValuePairs == null || keyValuePairs.length() < 1 || fragments.length < 1) {
			return map;
		}

		for (String pair : fragments) {
			String[] keyAndValue = Util.split(pair, keyValueSeparator);
			if (keyAndValue != null) {
				if (keyAndValue.length >= 2) {
					map.put(keyAndValue[0], keyAndValue[1]);
				} else {
					map.put(keyAndValue[0], null);
				}
			}
		}

		return map;
	}

	public static <EnumType extends Enum<EnumType>> Map<EnumType, String> parseKeyValuePairs(String keyValuePairs,
		Class<EnumType> enumaration) {
		return parseKeyValuePairs(keyValuePairs, enumaration, DEFAULT_KEY_VALUE_SEPARATOR, DEFAULT_PAIR_SEPARATOR);
	}

	/**
	 * @param keyValuePairs
	 * @param enumaration
	 * @param keyValueSeparator
	 * @param pairSeparator
	 * @return
	 */
	public static <EnumType extends Enum<EnumType>> Map<EnumType, String> parseKeyValuePairs(String keyValuePairs,
		Class<EnumType> enumaration, String keyValueSeparator, String pairSeparator) {
		Map<String, String> pairs = parseKeyValuePairs(keyValuePairs, keyValueSeparator, pairSeparator);
		//		Log.info("shared.Util.parseKeyValuePairs: rawMap.keys=" + pairs.keySet() + "; .values=" + pairs.values());
		Map<EnumType, String> result = new HashMap<EnumType, String>();
		for (EnumType enumValue : enumaration.getEnumConstants()) {
			if (pairs.containsKey(enumValue.name()) || pairs.containsKey(enumValue.name().toLowerCase())) {
				result.put(enumValue, pairs.get(enumValue.name()));
			}
		}
		//		Log.info("shared.Util.parseKeyValuePairs: enumMap.keys=" + result.keySet() + "; .values=" + result.values());

		return result;
	}

	/**
	 * Converts source type to a target type on demand
	 * 
	 * @author Sven Buschbeck
	 * 
	 * @param <SourceType>
	 * @param <TargetType>
	 */
	public static interface Converter<SourceType, TargetType> {
		/**
		 * @param source
		 * @return converted source
		 */
		TargetType convert(SourceType source);
	}

	/**
	 * Converts a string of key value pairs to a map where the keys are
	 * represented by an enumeration and the value is converted (using the given
	 * converter) to the given type (valueType).
	 * 
	 * @param keyValuePairs
	 * @param enumaration
	 * @param valueType
	 * @param converter
	 * @param keyValueSeparator
	 * @param pairSeparator
	 * @return map
	 */
	public static <EnumType extends Enum<EnumType>, ValueType extends Object> Map<EnumType, ValueType> parseKeyValuePairs(
		String keyValuePairs, Class<EnumType> enumaration, Class<ValueType> valueType,
		Converter<String, ValueType> converter, String keyValueSeparator, String pairSeparator) {
		Map<String, String> pairs = parseKeyValuePairs(keyValuePairs, keyValueSeparator, pairSeparator);
		Map<EnumType, ValueType> result = new HashMap<EnumType, ValueType>();
		for (EnumType enumValue : enumaration.getEnumConstants()) {
			if (pairs.containsKey(enumValue.name())) {
				result.put(enumValue, converter.convert(pairs.get(enumValue)));
			}
		}

		return result;
	}

	/**
	 * Converts the given number to a string. Adds "0" in front if the number is smaller than "length"
	 * @param number
	 * @param length
	 * @return number as string with padding "0" in front
	 */
	public static String formatNumber(int number, int length) {
		String string = "" + number;
		while(string.length() < length) string = "0" + string;
		return string;
	}

	public static String formatDate() {
		return formatDateShort(new Date());
	}

	public static String formatTime() {
		return formatTime(new Date());
	}

	/**
	 * @param date
	 */
	public static String formatDate(Date date) {
		return formatDateShort(date);
	}

	/**
	 * ISO 8601 date format, fixed across all locales.
	 * Example: 2008-10-03T10:29:40.046-04:00
	 * @param date
	 * @return
	 */
	public static String formatDateIso(Date date) {
		return DateTimeFormat.getFormat(PredefinedFormat.ISO_8601).format(date);
		//		return ISODateTimeFormat.date().print(new DateTime(date));
	}

	/**
	 * @param start
	 */
	public static String formatDateShort(Date date) {
		return DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT).format(date);
		//		return ISODateTimeFormat.basicDate().print(new DateTime(date));
	}

	public static String formatDateMedium(Date date) {
		return DateTimeFormat.getFormat(PredefinedFormat.DATE_MEDIUM).format(date);
		//		return ISODateTimeFormat.basicDate().print(new DateTime(date));
	}

	public static String formatDateLong(Date date) {
		return DateTimeFormat.getFormat(PredefinedFormat.DATE_LONG).format(date);
		//		return ISODateTimeFormat.basicDate().print(new DateTime(date));
	}

	public static String formatDateFull(Date date) {
		return DateTimeFormat.getFormat(PredefinedFormat.DATE_FULL).format(date);
		//		return ISODateTimeFormat.basicDate().print(new DateTime(date));
	}

	/**
	 * Example "November 1, 2012"
	 * 
	 * @param date
	 * @return
	 */
	public static String formatDateAmerican(Date date) {
		return ((formatAmericanDate == null) ? formatAmericanDate = DateTimeFormat.getFormat("MMMM d, yyyy")
			: formatAmericanDate).format(date);
	}
	
	/**
	 * Example "November 1, 2012 12.30 pm"
	 * 
	 * @param dateTime
	 * @return
	 */
	public static String formatDateTimeAmerican(Date dateTime) {
		return ((formatAmericanDateTime == null) ? formatAmericanDateTime = DateTimeFormat.getFormat("MMMM d, yyyy hh.mm a")
				: formatAmericanDateTime).format(dateTime);
	}

	/**
	 * Example "Sunday, November 1, 2012 12.30 pm"
	 * 
	 * @param dateTime
	 * @return
	 */
	public static String formatDateTimeAmericanWithDay(Date dateTime) {
		return ((formatAmericanDateTimeWithDay == null) ? formatAmericanDateTimeWithDay = DateTimeFormat.getFormat("E, MMMM d, yyyy hh.mm a")
				: formatAmericanDateTimeWithDay).format(dateTime);
	}
	
	public static String formatDateTimeShort(Date date) {
		return DateTimeFormat.getFormat(PredefinedFormat.DATE_TIME_SHORT).format(date);
		//		return ISODateTimeFormat.basicDate().print(new DateTime(date));
	}

	/**
	 * @param start
	 */
	public static String formatDateTimeFull(Date date) {
		return DateTimeFormat.getFormat(PredefinedFormat.DATE_TIME_FULL).format(date);
		//		return formatDateIso(date);
	}

	private static List<DateTimeFormat> formatters;

	/**
	 * @param s
	 * @return
	 */
	public static Date parseDate(String s) {
		if (formatters == null) {
			// Formatter usage examples:
			// "yyyy.MM.dd G 'at' HH:mm:ss vvvv" 1996.07.10 AD at 15:08:56 America/Los_Angeles
			// "EEE, MMM d, ''yy"                Wed, July 10, '96
			// "h:mm a"                          12:08 PM
			// "hh 'o''clock' a, zzzz"           12 o'clock PM, Pacific Daylight Time
			// "K:mm a, vvvv"                    0:00 PM, America/Los_Angeles
			// "yyyyy.MMMMM.dd GGG hh:mm aaa"	01996.July.10 AD 12:08 PM
			formatters = createList(
				// ISO: 2008-10-03T10:29:40.046-04:00
				DateTimeFormat.getFormat(PredefinedFormat.ISO_8601),
				DateTimeFormat.getFormat(PredefinedFormat.RFC_2822),
				// some custom, shorter ISO format used in XML/XSD/RDF: 2012-07-05T09:48:51+01:00
				DateTimeFormat.getFormat("yyyy-MM-dd'T'HH:mm:ssZZZ"),
				// UniTN format: 2011-08-29T00:00:00Z
				DateTimeFormat.getFormat("yyyy-MM-dd'T'HH:mm:ss'Z'"),
				// ML format: 2010-06-26T18:00:00UTC
				DateTimeFormat.getFormat("yyyy-MM-dd'T'HH:mm:ss'UTC'"));
		}
		for (DateTimeFormat formatter : formatters) {
			try {
				return formatter.parse(s);
			} catch (Exception e) {
			}
		}
		return null;
	}

	/**
	 * @param date
	 */
	private static String formatTime(Date date) {
		return DateTimeFormat.getFormat(PredefinedFormat.HOUR_MINUTE_SECOND).format(date);
	}
	/**
	 * @param date
	 */
	private static String formatTime24(Date date) {
		return DateTimeFormat.getFormat(PredefinedFormat.HOUR24_MINUTE_SECOND).format(date);
	}
	
	public static Date tomorrow(Date date) {
		return new Date(date.getTime() + Constants.TIME_ONE_DAY);
	}

	public static String[] split(String str) {
		return split(str, null, -1);
	}

	public static String[] split(String str, String separatorChars) {
		return splitWorker(str, separatorChars, -1, true);
	}

	public static String[] split(String str, String separatorChars, int max) {
		return splitWorker(str, separatorChars, max, true);
	}

	private static final String[] EMPTY_STRING_ARRAY = new String[0];

	private static String[] splitWorker(String str, String separatorChars, int max, boolean preserveAllTokens) {
		// Performance tuned for 2.0 (JDK1.4)
		// Direct code is quicker than StringTokenizer.
		// Also, StringTokenizer uses isSpace() not isWhitespace()

		if (str == null) {
			return null;
		}
		int len = str.length();
		if (len == 0) {
			return EMPTY_STRING_ARRAY;
		}
		List<String> list = new ArrayList<String>();
		int sizePlus1 = 1;
		int i = 0, start = 0;
		boolean match = false;
		boolean lastMatch = false;
		if (separatorChars == null) {
			// Null separator means use whitespace
			while (i < len) {
				if (" \t\n\r".indexOf(str.charAt(i)) >= 0) {
					if (match || preserveAllTokens) {
						lastMatch = true;
						if (sizePlus1++ == max) {
							i = len;
							lastMatch = false;
						}
						list.add(str.substring(start, i));
						match = false;
					}
					start = ++i;
					continue;
				}
				lastMatch = false;
				match = true;
				i++;
			}
		} else if (separatorChars.length() == 1) {
			// Optimise 1 character case
			char sep = separatorChars.charAt(0);
			while (i < len) {
				if (str.charAt(i) == sep) {
					if (match || preserveAllTokens) {
						lastMatch = true;
						if (sizePlus1++ == max) {
							i = len;
							lastMatch = false;
						}
						list.add(str.substring(start, i));
						match = false;
					}
					start = ++i;
					continue;
				}
				lastMatch = false;
				match = true;
				i++;
			}
		} else {
			// standard case
			while (i < len) {
				if (separatorChars.indexOf(str.charAt(i)) >= 0) {
					if (match || preserveAllTokens) {
						lastMatch = true;
						if (sizePlus1++ == max) {
							i = len;
							lastMatch = false;
						}
						list.add(str.substring(start, i));
						match = false;
					}
					start = ++i;
					continue;
				}
				lastMatch = false;
				match = true;
				i++;
			}
		}
		if (match || (preserveAllTokens && lastMatch)) {
			list.add(str.substring(start, i));
		}
		return list.toArray(new String[list.size()]);
	}

	public static <T> void truncate(LinkedList<T> list, int maxLength) {
		while (size(list) > maxLength) {
			list.removeLast();
		}
	}

	public static <T> void truncate(Collection<T> collection, int maxLength) {
		//		collection.toArray(new T[]);
		//		while (collection.size() > maxLength) {
		//			collection.
		//		}
		if (collection.size() <= maxLength) {
			return;
		}
		ArrayList<T> copy = new ArrayList<T>(collection);
		collection.clear();
		collection.addAll(copy.subList(0, maxLength));
	}

	public static String truncate(String string, int maxLength) {
		assert maxLength > 0 : "The parameter maxLength must be greater than 0.";
		if (string.length() > maxLength) {
			return string.substring(string.length() - maxLength);
		}
		return string;
	}

	public static String truncateBy(String string, int numberOfCharactersToBeRemoved) {
		if (isEmpty(string) || string.length() < numberOfCharactersToBeRemoved) {
			return string;
		}
		assert numberOfCharactersToBeRemoved > 0 : "The parameter numberOfCharactersToBeRemoved must be greater than 0.";
		return string.substring(0, string.length() - numberOfCharactersToBeRemoved);
	}

	/**
	 * Null-safe way to add several elements at the end of the given array.
	 * 
	 * @param array
	 * @param objects
	 */
	public static <Type> Type[] union(Type object, Type[] array) {
		if (isEmpty(array)) {
			return createList(object).toArray(array);
		}
		List<Type> list = createList(array);
		list.add(0, object);
		return list.toArray(array);
	}

	public static <T> Collection<T> union(Collection<T> collection, T... values) {
		return union(collection, Util.createList(values));
	}

	public static <T> Collection<T> union(Collection<T>... collections) {
		// does not work shit even not with hashCode() implemented. 
		//		LinkedHashSet<T> removeDuplicates = new LinkedHashSet<T>();
		//		for (Collection<T> collection : collections) {
		//			System.out.println("Util.union: input collection=" + collection);
		//			if (collection != null) {
		//				removeDuplicates.addAll(collection);
		//			}
		//			System.out.println("Util.union: current result set=" + removeDuplicates);
		//		}
		//		return removeDuplicates;

		// so we go old school... 
		if (collections.length < 2) {
			if (collections.length == 1) {
				return collections[0];
			}
			return null;
		}
		Collection<T> result = Util.createList(collections[0]);
		for (int x = 1; x < collections.length; x++) {
			if (isEmpty(collections[x])) {
				continue;
			}
			for (T thing : collections[x]) {
				if (!result.contains(thing)) {
					result.add(thing);
				}
			}
		}
		return result;
	}

	public static <T> List<T> overlap(Collection<T>... collections) {
		assert collections.length > 1 : "To calculate overlap at at least 2 collections are required. " + collections;
		List<T> result = new LinkedList<T>(collections[0]);
		for (int x = 1; x < collections.length; x++) {
			result.retainAll(collections[x]);
		}
		return result;
	}

	/**
	 * @param <ListType>
	 * @param list
	 * @param index
	 * @param object
	 */
	public static <ListType> void addToList(List<ListType> list, int index, ListType object) {
		while (list.size() < index) {
			list.add(object);
		}
		list.add(index, object);
	}

	/**
	 * Null-safe (objects can be null) way to add several elements at the end of
	 * the given collection.
	 * 
	 * @param collection
	 * @param objects
	 * @return
	 */
	public static <CollectionType, T extends Collection<CollectionType>> T addAll(T collection,
		CollectionType... objects) {
		if (notEmpty(objects)) {
			collection.addAll(createList(objects));
		}
		return collection;
	}

	/**
	 * Null-safe (objects can be null) way to add several elements at the end of
	 * the given collection.
	 * 
	 * @param collection
	 * @param objects
	 */
	public static <ArrayType, T extends Collection<ArrayType>> T addAll(T collection, Collection<ArrayType> objects) {
		if (notEmpty(objects)) {
			collection.addAll(objects);
		}
		return collection;
	}

	/**
	 * Null-safe way to add several elements at the end of the given array.
	 * 
	 * @param array
	 * @param objects
	 */
	public static <ArrayType> ArrayType[] addAll(ArrayType[] array, ArrayType... objects) {
		if (notEmpty(objects)) {
			List<ArrayType> list = addAll(createList(array), objects);
			return list.toArray(array);
		}
		return array;
	}

	/**
	 * Null-safe (objects can be null) way to add several elements at the given
	 * index to the given list.
	 * 
	 * @param list
	 * @param index
	 * @param objects
	 * @return
	 */
	public static <ListType, T extends List<ListType>> T addAllToList(T list, int index, Collection<ListType> objects) {
		if (notEmpty(objects)) {
			list.addAll(index, objects);
		}
		return list;
	}

	/**
	 * Create empty list.
	 * 
	 * @param <T>
	 * @return Empty list (0 elements).
	 */
	public static <T> List<T> createList() {
		return new ArrayList<T>();
	}

	/**
	 * Create from given iterator.
	 * 
	 * @param <T>
	 * @return List of all elements retrievable from the given iterator.
	 */
	public static <T> List<T> createList(Iterator<T> iterator) {
		return createLinkedList(iterator);
	}
	
	/**
	 * Create from given iterator.
	 * 
	 * @param <T>
	 * @return List of all elements retrievable from the given iterator.
	 */
	public static <T> LinkedList<T> createLinkedList(Iterator<T> iterator) {
		LinkedList<T> list = new LinkedList<T>();
		while (iterator.hasNext()) {
			list.add(iterator.next());
		}
		return list;
	}

	/**
	 * Creates a linked list from the given collection (null safe). Or type casts it if it is a linked list already.
	 * 
	 * @param <T>
	 * @return List of all elements retrievable from the given iterator.
	 */
	public static <T> LinkedList<T> createLinkedList(Collection<T> collection) {
		if (collection == null) return null;
		if (collection instanceof LinkedList<?>) {
			return (LinkedList<T>) collection;
		}
		if (notEmpty(collection)) {
			return new LinkedList<T>(collection);
		}
		return new LinkedList<T>();
	}
	
	/**
	 * Creates a list of objects from the given set.
	 * 
	 * @param <T>
	 * @param items
	 * @return A list of all given objects.
	 */
	public static <T> List<T> createList(T... items) {
		//		return new ArrayList<T>(Arrays.asList(items));
		//		return Arrays.asList(items);
		return isEmpty(items) ? new LinkedList<T>() : new LinkedList<T>(Arrays.asList(items));
	}

	/**
	 * Creates a list containing the given object.
	 * 
	 * @param <T>
	 * @param item
	 *            to be put in list
	 * @return A list containing the given object.
	 */
	public static <T> List<T> createList(T item) {
		return addAll(new LinkedList<T>(), item);
	}

	// more a union operation
	//	/**
	//	 * Creates a list of objects both, from the given one and the optional set.
	//	 * 
	//	 * @param <T>
	//	 * @param item
	//	 *            to be put in list first
	//	 * @param moreItems
	//	 *            to be added to the list after item
	//	 * @return A list contain item and moreItems.
	//	 */
	//	public static <T> List<T> createList(T item, T... moreItems) {
	//		//		return new ArrayList<T>(Arrays.asList(items));
	//		//		return Arrays.asList(items);
	//		return isEmpty(moreItems) ? createList(item) : new LinkedList<T>(addAll(Arrays.asList(moreItems), item));
	//	}

	public static <T> List<T> createList(Collection<T> toBeCopied) {
		return Util.isEmpty(toBeCopied) ? new LinkedList<T>() : new LinkedList<T>(toBeCopied);
	}

	public static <T> List<T> createList(int insertAt, Collection<T> orgList, T... toBeAdded) {
		LinkedList<T> result = (orgList == null) ? new LinkedList<T>() : new LinkedList<T>(orgList);
		result.addAll(insertAt, Arrays.asList(toBeAdded));
		return result;
	}

	public static <ListType, T extends List<ListType>> T sort(T collection, Comparator<ListType> comparator) {
		Collections.sort(collection, comparator);
		return collection;
	}

	/**
	 * Reverses the given list and returns it. WARNING: does alter the list
	 * directly, not creating a copy of it.
	 * 
	 * @param list
	 * @return the given list but in reverse order
	 */
	public static <ListType extends List<T>, T> ListType reverse(ListType list) {
		for (int x = 0; x < size(list) - 1; x++) {
			list.add(list.remove(0));
		}
		return list;
	}

	public static <T> LinkedList<T> concatinateCollections(Collection<T>... collections) {
		LinkedList<T> newCollection = new LinkedList<T>();
		for (Collection<T> list : collections) {
			if (list != null) {
				newCollection.addAll(list);
			}
		}
		return newCollection;
	}

	public static <T> Collection<T> concatinateCollectionsRemoveDuplicates(Collection<T>... collections) {
		return Util.union(collections);
	}

	public static <T> List<T> concatinateLists(List<T>... lists) {
		List<T> newList = new LinkedList<T>();
		for (List<T> list : lists) {
			if (list != null) {
				newList.addAll(list);
			}
		}
		return newList;
	}

	public static boolean contains(double x, double y, double x1, double y1, double x2, double y2) {
		//		System.out.println(x + " x " + y + " > " + x1 + " x " + y1 + "; " + x2 + " x " + y2 + " = "
		//			+ (x >= x1 && x <= x2 && y >= y1 && y <= y2));
		return x >= x1 && x <= x2 && y >= y1 && y <= y2;
	}

	public static boolean contains(double x, double y, double... xyOfPolygonAlternately) {
		assert xyOfPolygonAlternately.length % 2 == 0;
		int i, j;
		double vertice1X, vertice1Y, vertice2X, vertice2Y;
		boolean result = false;
		for (i = 0, j = xyOfPolygonAlternately.length - 2; i < xyOfPolygonAlternately.length - 1; j = (i += 2) - 2) {
			vertice1X = xyOfPolygonAlternately[i];
			vertice1Y = xyOfPolygonAlternately[i + 1];
			vertice2X = xyOfPolygonAlternately[j];
			vertice2Y = xyOfPolygonAlternately[j + 1];
			//			if ((vertice1Y > y) != (vertice2Y > y)
			//				&& (x < (vertice2X - vertice1X) * (y - vertice1Y) / (vertice2Y - vertice1Y) + vertice1X)) {
			if ((((vertice1Y <= y) && (y < vertice2Y)) || ((vertice2Y <= y) && (y < vertice1Y)))
				&& (x < (vertice2X - vertice1X) * (y - vertice1Y) / (vertice2Y - vertice1Y) + vertice1X)) {
				result = !result;
			}
		}
		return result;

	}

	/**
	 * Not case sensitive.
	 * 
	 * @param label
	 * @param keywords
	 * @return
	 */
	public static boolean containsAny(String label, String[] keywords) {
		return containsAny(label, keywords, false);
	}

	public static boolean containsAny(String label, String[] keywords, boolean caseSensitive) {
		if (!caseSensitive) {
			label = label.toUpperCase();
		}
		for (String keyword : keywords) {
			if (label.indexOf(caseSensitive ? keyword : keyword.toUpperCase()) >= 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns true if any of the hashes of the given objects matches one of the
	 * hashes given by objectHashes
	 * 
	 * @param objects
	 * @param objectHashs
	 * 
	 * @return True if any any(objects).hash == any(objectHashs)
	 */
	public static boolean containsAny(List<? extends Object> objects, Set<Integer> objectHashs) {
		for (Object object : objects) {
			if (objectHashs.contains(object.hashCode())) {
				return true;
			}
		}
		return false;
	}

	public static boolean containsAll(String label, String[] keywords) {
		return containsAll(label, keywords, false);
	}

	public static boolean containsAll(String label, String[] keywords, boolean caseSensitive) {
		boolean found = true;
		if (!caseSensitive) {
			label = label.toLowerCase();
		}
		for (String keyword : keywords) {
			found &= label.indexOf(caseSensitive ? keyword : keyword.toLowerCase()) >= 0;
		}
		return found;
	}

	/**
	 * Null-safe contains. Returns false if the given collection is null;
	 * 
	 * @param collection
	 * @param toSearch
	 * @return
	 */
	public static <T> boolean contains(Collection<T> collection, T toSearch) {
		return collection != null && collection.contains(toSearch);
	}

	/**
	 * Null-safe contains. Returns false if the given array is null;
	 * 
	 * @param collection
	 * @param toSearch
	 * @return
	 */
	public static <T> boolean contains(T[] array, T toSearch) {
		if (notEmpty(array)) {
			for (int x = 0; x < array.length; x++) {
				if (equals(array[x], toSearch)) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Casts a collection of objects of class B where B extends A to a
	 * collection of objects of class A.
	 * 
	 * @param <T>
	 *            Base class
	 * @param collection
	 *            Collection of objects of a class extending T
	 * @param aClass
	 *            Representation of T
	 * @return Collection of objects of T casted from given collection of
	 *         objects of a class extending T.
	 */
	@SuppressWarnings("unchecked") public static <T> Collection<T> downCastCollection(
		Collection<? extends T> collection, Class<T> aClass) {
		return (Collection<T>) collection;
	}

	/**
	 * Casts a list of objects of class B where B extends A to a list of objects
	 * of class A.
	 * 
	 * @param <T>
	 *            Base class
	 * @param list
	 *            List of objects of a class extending T
	 * @param aClass
	 *            Representation of T
	 * @return List of objects of T casted from given list of objects of a class
	 *         extending T.
	 */
	@SuppressWarnings("unchecked") public static <T> List<T> downCastList(List<? extends T> collection, Class<T> aClass) {
		return (List<T>) collection;
	}

	/**
	 * @param map
	 * @param class1
	 * @param class2
	 * @return
	 */
	public static <KeyType, ValueType> Map<KeyType, ValueType> downCastMap(
		Map<? extends KeyType, ? extends ValueType> map, Class<KeyType> keyClass, Class<ValueType> valueClass) {
		return (Map<KeyType, ValueType>) map;
	}

	/**
	 * Makes sure that the given value is between the given minimum and maximum.
	 * 
	 * @param value
	 * @param min
	 * @param max
	 * @return value or min/max if value exceeds min/max
	 */
	public static int constrain(int value, int min, int max) {
		return (value >= min) ? ((value <= max) ? value : max) : min;
	}

	/**
	 * Makes sure that the given value is between the given minimum and maximum.
	 * 
	 * @param value
	 * @param min
	 * @param max
	 * @return value or min/max if value exceeds min/max
	 */
	public static double constrain(double value, double min, double max) {
		if (value >= min) {
			return ((value <= max) ? value : max);
		} else {
			return min;
		}
	}

	/*
	 * File: Math.uuid.js Version: 1.3 Change History: v1.0 - first release v1.1
	 * - less code and 2x performance boost (by minimizing calls to
	 * Math.random()) v1.2 - Add support for generating non-standard uuids of
	 * arbitrary length v1.3 - Fixed IE7 bug (can't use []'s to access string
	 * chars. Thanks, Brian R.) v1.4 - Changed method to be "Math.uuid". Added
	 * support for radix argument. Use module pattern for better encapsulation.
	 * 
	 * Latest version: http://www.broofa.com/Tools/Math.uuid.js Information:
	 * http://www.broofa.com/blog/?p=151 Contact: robert@broofa.com ----
	 * Copyright (c) 2008, Robert Kieffer All rights reserved.
	 * 
	 * Redistribution and use in source and binary forms, with or without
	 * modification, are permitted provided that the following conditions are
	 * met:
	 * 
	 * Redistributions of source code must retain the above copyright notice,
	 * this list of conditions and the following disclaimer. Redistributions in
	 * binary form must reproduce the above copyright notice, this list of
	 * conditions and the following disclaimer in the documentation and/or other
	 * materials provided with the distribution. Neither the name of Robert
	 * Kieffer nor the names of its contributors may be used to endorse or
	 * promote products derived from this software without specific prior
	 * written permission.
	 * 
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
	 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
	 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
	 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
	 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
	 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
	 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
	 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	 */

	static public class UUID {
		private static final char[] CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
			.toCharArray();

		/**
		 * Generate a random uuid of the specified length. returns
		 * "VcydxgltxrVZSTV"
		 * 
		 * @param len
		 *            the desired number of characters
		 */
		public static String generate(int len) {
			return generate(len, CHARS.length);
		}

		/**
		 * Generate a random uuid of the specified length, and radix. Examples:
		 * <ul>
		 * <li>generate(8, 2) returns "01001010" (8 character ID, base=2)
		 * <li>generate(8, 10) returns "47473046" (8 character ID, base=10)
		 * <li>generate(8, 16) returns "098F4D35" (8 character ID, base=16)
		 * </ul>
		 * 
		 * @param len
		 *            the desired number of characters
		 * @param radix
		 *            the number of allowable values for each character (must be
		 *            <= 62)
		 */
		public static String generate(int len, int radix) {
			if (radix > CHARS.length) {
				throw new IllegalArgumentException("Radix must not be larger than " + CHARS.length);
			}
			char[] uuid = new char[len];
			// Compact form
			for (int i = 0; i < len; i++) {
				uuid[i] = CHARS[(int) (Math.random() * radix)];
			}
			return new String(uuid);
		}

		/**
		 * Generate a RFC4122, version 4 ID. Example:
		 * "92329D39-6F5C-4520-ABFC-AAB64544E172"
		 */
		public static String generate() {
			char[] uuid = new char[36];
			int r;

			// rfc4122 requires these characters
			uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
			uuid[14] = '4';

			// Fill in random data.  At i==19 set the high bits of clock sequence as
			// per rfc4122, sec. 4.1.5
			for (int i = 0; i < 36; i++) {
				if (uuid[i] == 0) {
					r = (int) (Math.random() * 16);
					uuid[i] = CHARS[(i == 19) ? (r & 0x3) | 0x8 : r & 0xf];
				}
			}
			return new String(uuid);
		}
	}

	public static Map<String, Double> normalizeMap(Map<String, Double> map) {
		double sum = 0;
		for (String key : map.keySet()) {
			sum += map.get(key);
		}
		for (String key : map.keySet()) {
			map.put(key, map.get(key) / sum);
		}
		return map;
	}

	public static class StopWatch {

		public long start;
		public long stop;

		StopWatch() {
			start = getTime();
		}

		public StopWatch stop() {
			stop = getTime();
			return this;
		}

		public long getDifference() {
			return stop - start;
		}

		public long getCurrentDifference() {
			return getTime() - start;
		}

		private long getTime() {
			return System.currentTimeMillis();
		}

		public void reset() {
			start = 0;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override public String toString() {
			return "Timer: " + getDifference() + "ms";
		}

		/**
		 * @return
		 */
		public long getDifferenceAndRestart() {
			long currentDifference = getCurrentDifference();
			reset();
			return currentDifference;
		}
	}

	public static StopWatch getStopWatch() {
		return new Util.StopWatch();
	}

	/**
	 * Null-safe comparison based on equals
	 * 
	 * @param object1
	 * @param object2
	 * @return
	 */
	public static boolean equals(Object object1, Object object2) {
		return (object1 == object2) || (object1 != null && object1.equals(object2));
	}

	public static boolean equals(HasId object1, HasId object2) {
		if (object1 == object2) {
			return true;
		}
		if (object1 == null || object2 == null) {
			return false;
		}
		return equals(object1.getId(), object2.getId());
	}

	/**
	 * @param a
	 * @param b
	 * @param maxDeviation
	 * @return
	 */
	public static boolean equals(double a, double b, double maxDeviation) {
		return Math.abs(a - b) < maxDeviation;
	}
	
	/**
	 * Returns true if one of the given options equals to the given objects
	 * @param object
	 * @param oneOfTheOptionsToBeEqual
	 * @return true if object.equals to at least one in oneOfTheOptionsToBeEqual
	 */
	public static <T> boolean equals(T object, T... oneOfTheOptionsToBeEqual) {
		if (isEmpty(oneOfTheOptionsToBeEqual)) return false;
		for (T oneOfTheOptions : oneOfTheOptionsToBeEqual) {
			if (equals(oneOfTheOptions, object)) return true;
		}
		return false;
	}

	public static boolean equalClass(Class<?> classA, Class<?> classB) {
		String nameOfClassA = classA.toString();
		String nameOfClassB = classB.toString();
		//		System.out.println("Util.equalClass: a=" + classA + "; b=" + classB + "; a.hash=" + nameOfClassA.hashCode()
		//			+ "; b.hash=" + nameOfClassB.hashCode() + "; equals=" + nameOfClassA.equals(nameOfClassB));
		return nameOfClassA.equals(nameOfClassB);
	}

	public static int compare(HasId o1, HasId o2) {
		if (o1 == o2) {
			return 0;
		}
		if (o1 == null || isEmpty(o1.getId())) {
			return -1;
		}
		if (o2 == null || isEmpty(o2.getId())) {
			return 1;
		}
		return o1.getId().compareTo(o2.getId());
	}
	
	public static double compareViaNgrams(String s1, String s2) {
		return compareViaNgrams(s1, s2, 3);
	}

	public static double compareViaNgrams(String s1, String s2, int n) {
		return compareViaNgrams(s1.toCharArray(), s2.toCharArray(), n);
	}

	public static double compareViaNgrams(char[] a1, char[] a2, int n) {
		assert n > 0;
		if (a1.length > a2.length) {
			char[] aTemp = a1;
			a1 = a2;
			a2 = aTemp;
		}
		int same = 0;
		for (int x1 = 0; x1 <= a1.length - n; x1++) {
			for (int x2 = 0; x2 <= a2.length - n; x2++) {
//				System.out.print(x1 + " - " + x2);
				boolean sameNgram = true;
				for (int nx = 0; nx < n; nx++) {
					sameNgram &= a1[x1 + nx] == a2[x2 + nx];
					if (!sameNgram) break;
				}
				if (sameNgram)  {
					same++;
//					System.out.println(" > " + same);
					break;
				}
//				System.out.println();
			}
		}
//		print(Util.class, "compareViaNgrams", "a, b, same", new String(a1), new String(a2), same);
		//        6        /  6         + 6         -  3  + 2
		//          12     /            12          -  6  + 2
		return (same * 2d) / (a1.length + a2.length - 2*n + 2);
	}

	/**
	 * @param searchFor
	 * @param setToSearchIn
	 * @return
	 */
	public static <T> boolean containedIn(T searchFor, T... setToSearchIn) {
		for (T object : setToSearchIn) {
			if (equals(searchFor, object)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Maps the value from the original scale (given by value range) to the
	 * desired scale (given by target range)
	 * 
	 * @param value
	 * @param valueRangeMin
	 * @param valueRangeMax
	 * @param targetRangeMin
	 * @param targetRangeMax
	 * @return value mapped form value scale to target scale
	 */
	public static double map(double value, double valueRangeMin, double valueRangeMax, double targetRangeMin,
		double targetRangeMax, boolean assertRange) {
		assert !assertRange || inRange(value, valueRangeMin, valueRangeMax);
		assert !assertRange || valueRangeMin < valueRangeMax;
		assert !assertRange || targetRangeMin < targetRangeMax;
		double result = targetRangeMin + ((value - valueRangeMin) / (valueRangeMax - valueRangeMin))
			* (targetRangeMax - targetRangeMin);
		//		Log.debug(Util.class,
		//			".map(" + Util.joinAll(value, valueRangeMin, valueRangeMax, targetRangeMin, targetRangeMax) + "): result",
		//			result);
		assert !assertRange || inRange(result, targetRangeMin, targetRangeMax);
		return result;
	}

	/**
	 * @param value
	 * @param min
	 * @param max
	 * @return true if min <= value <= max
	 */
	public static boolean inRange(int value, int min, int max) {
		return value >= min && value <= max;
	}

	/**
	 * @param value
	 * @param min
	 * @param max
	 * @return true if min <= value <= max
	 */
	public static boolean inRange(double value, double min, double max) {
		return value >= min && value <= max;
	}

	public static boolean inRange(long value, long min, long max) {
		return value >= min && value <= max;
	}

	/**
	 * Returns true if min and max lie within the given boundaries.
	 * 
	 * @param min
	 * @param max
	 * @param minValue
	 * @param maxValue
	 * @return
	 */
	public static boolean inRange(double min, double max, double minValue, double maxValue) {
		//		return minValue >= min && maxValue <= max;
		return inRange(min, minValue, maxValue) && inRange(max, minValue, maxValue);
	}

	/**
	 * @param values
	 * @param min
	 * @param max
	 * @return true if min <= values.size <= max; if values==null, size is
	 *         assumed 0
	 */
	public static boolean inRange(Collection<?> values, int min, int max) {
		return inRange(Util.size(values), min, max);
	}
	
	/**
	 * Checks if the given date is startDate <= date <= endDate; if startDate and/or endDate is null means that this there is no restriction in this direction (i.e. if both are null, this method will return true always).
	 * @param date the date to be checked if it is in range
	 * @param startDate the lower bound or null for no boundary
	 * @param endDate the upper bound or null for no boundary
	 * @return true if startDate <= date <= endDate
	 */
	public static boolean inRange(Date date, Date startDate, Date endDate, boolean dateOnlyInoreTime) {
		if (dateOnlyInoreTime) {
			setTimeToZero(startDate);
			setTimeToMax(endDate);
		}
		return (date != null) &&
			(startDate == null || date.after(startDate) || date.equals(startDate)) &&
			(endDate == null || date.before(endDate) || date.equals(endDate));
	}

	private static void setTimeToZero(Date date) {
		date.setHours(0);
		date.setMinutes(0);
		date.setSeconds(0);
	}

	private static void setTimeToMax(Date date) {
		date.setHours(23);
		date.setMinutes(59);
		date.setSeconds(59);
	}
	
	/**
	 * @param min
	 *            lower bound
	 * @param max
	 *            upper bound
	 * @param value
	 *            to keep between min and max bounds
	 * @return
	 */
	public static double keepInRange(double min, double max, double value) {
		return Math.min(max, Math.max(min, value));
	}

	public static int keepInRange(int min, int max, int value) {
		return Math.min(max, Math.max(min, value));
	}

	/**
	 * @param string
	 */
	public static int parseInt(String string, int defaultValue) {
		try {
			return Integer.parseInt(string);
		} catch (Exception e) {
			return defaultValue;
		}
	}

	/**
	 * @param string
	 * @return
	 */
	public static boolean parseBoolean(String string) {
		if (string == null) {
			return false;
		}
		string = string.trim().toLowerCase();
		return Boolean.parseBoolean(string) || "on".equals(string) || "yes".equals(string) || "1".equals(string);
	}

	/**
	 * Returns the parsed Double or null if an error occurred.
	 * 
	 * @param doubleString
	 * @return double value or null
	 */
	public static Double parseDouble(String doubleString) {
		try {
			return Double.parseDouble(doubleString.trim());
		} catch (Throwable t) {}
		return null;
	}

	/**
	 * Returns the parsed Long or null if an error occurred.
	 * 
	 * @param longString
	 * @return long value or null
	 */
	public static Long parseLong(String longString) {
		try {
			return Long.parseLong(longString.trim());
		} catch (Throwable t) {}
		return null;
	}
	
	public static void print(Object source, String methodName, String message, Object... values) {
		StringBuilder sb = new StringBuilder();
		sb.append(formatTime24(new Date()));
		sb.append(" ");
		sb.append(getSimpleClassName(source));
		sb.append(".");
		sb.append(methodName);
		sb.append(": ");
		sb.append(message);
		if (notEmpty(values)) {
			sb.append(" = ");
			sb.append(join(values, ", "));
		}
		System.out.println(sb);
	}
	
	/**
	 * Returns the class' name without the packages
	 * 
	 * @param object
	 * @return name with package information
	 */
	public static String getSimpleClassName(Object object) {
		return getClassName(object, false);
	}

	public static String getClassName(Object object, boolean includePackage) {
		if (object == null) {
			return null;
		}
		String name = ((object instanceof Class) ? (Class<?>) object : object.getClass()).getName();
		if (includePackage) {
			return name;
		}
		return name.substring(name.lastIndexOf(".") + 1);
	}

	public static String shortenString(Object object, int maxLength) {
		return shortenString(object, maxLength, false);
	}

	public static String shortenString(Object object, int maxLength, boolean removeLineBreaks) {
		return shortenString((object == null) ? null : object.toString(), maxLength, removeLineBreaks, false);
	}

	/**
	 * @param maxLength
	 * @param padIfShorter
	 * @param movieEntity
	 * @return
	 */
	public static String shortenString(String string, int maxLength, boolean removeLineBreaks, boolean padIfShorter) {
		if (string == null) {
			return null;
		}
		if (removeLineBreaks) {
			string = removeAll(string, "\n", "\r");
		}
		if (maxLength > 0) {
			string = truncate(string, maxLength);
		}
		if (padIfShorter) {
			while (string.length() < maxLength) {
				string = " " + string;
			}
		}
		return string;
	}

	/**
	 * Null-safe way to turn a string to lower case / small caps.
	 * 
	 * @param string
	 * @return string as small caps or null if input was null
	 */
	public static String smallCaps(String string) {
		return (string != null) ? string.toLowerCase() : null;
	}

	public static String removeAll(String string, String... searchFor) {
		return replaceAll(string, "", searchFor);
	}

	/**
	 * @param allEntities
	 * @return
	 */
	public static <T extends Comparable> Collection<T> removeDuplicates(Collection<T> collection) {
		if (collection instanceof Set) {
			return collection;
		}
		return new HashSet<T>(collection);
	}

	public static String replaceAll(String string, String replaceWith, String... searchFor) {
		for (String search : searchFor) {
			string = string.replace(search, replaceWith);
		}
		return string;
	}

	/**
	 * Makes sure the string represented by longString gets truncated if it
	 * exceeds the given maximum length maxLength
	 * 
	 * @param longString
	 * @param maxLength
	 * @return truncated string with length <= maxLength
	 */
	public static String abbreviate(String longString, int maxLength) {
		return abbreviate(longString, maxLength, false);
	}

	public static String abbreviate(String longString, int maxLength, boolean htmlSafe) {
		final String suffix = "...";
		//		maxLength = Math.max(maxLength - suffix.length(), 0) + suffix.length();
		if (longString == null || maxLength < 0) {
			return longString;
		}
		// return without suffix, if the max length < shorter than the suffix itself
		if (maxLength < suffix.length()) {
			return longString.substring(0, maxLength);
		}
		if (longString.length() <= maxLength) {
			return longString;
		}

		// abbreviate string and add "..."
		if (!htmlSafe) {
			return longString.substring(0, maxLength - suffix.length()) + suffix;
		}
		longString = longString.substring(0, maxLength - suffix.length());
		int lastTagEnd = longString.lastIndexOf(">");
		int lastTagBeginning = longString.lastIndexOf("<");
		if (lastTagBeginning > -1 && lastTagBeginning > lastTagEnd) {
			longString = longString.substring(0, lastTagBeginning);
		}
		return longString + suffix;
	}
	
	public static String capitalizeSentence(String string) {
		// capitalize all not in NOT_TO_BE_CAPITALIZED or being the first or last word.
		String[] words = split(string, " ");
		if (words.length < 1) return string;
		String first=first(words), last = last(words);
		LinkedList<String> capitalizedWords = new LinkedList<String>();
		for (String word : words) {
			capitalizedWords.add(capitalizeWord(word, word == first || word == last));
		}
		return join(capitalizedWords, " ");
	}
	
	private static final List<String> NOT_TO_BE_CAPITALIZED = createList(
			// following the The Chicago Manual of Style
			// articles
			"a", "an", "the", 
			// conjunctions
			"and", "but", "or", "for", "nor",
			// prepositions
			"by", "as", "like", "to", "on", "at", "of", "in");
	
	public static String capitalizeWord(String word) {
		return capitalizeWord(word, false);
	}
	public static String capitalizeWord(String word, boolean forced) {
		word = word.trim();
		if (word != null && (word = word.trim()).length() > 0 && (forced || word.length() > 3 || !NOT_TO_BE_CAPITALIZED.contains(word))) {
//			return Character.toUpperCase(word.charAt(0)) + word.substring(1).toLowerCase();
			// take care of the case where the first, or the first few signs are not letters
			int firstLetter = 0;
			char[] chars = word.toLowerCase().toCharArray();
			for (char c : chars) {
				if (c >= 'A' && c <= 'Z') break;
				if (c >= 'a' && c <= 'z') {
					chars[firstLetter] = Character.toUpperCase(c);
					break;
				}
				firstLetter++;
			}
			return new String(chars);
		}
		return word;
	}
	
	private static String DEFAULT_SYMBOLS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
	public static Long decode(String encoded) {
		return decode(encoded, DEFAULT_SYMBOLS);
	}
	
	public static Long decode(String encoded, String symbols) {
		return decode((encoded == null) ? null : encoded.toCharArray(), symbols);
	}

	public static Long decode(char[] encoded) {
		return decode(encoded, DEFAULT_SYMBOLS);
	}
	
	public static Long decode(char[] encoded, String symbols) {
		if (encoded == null) return null;
		try {
		    final int length = symbols.length();
		    long decoded = 0;
		    for (char c : encoded) {
		        decoded *= length;
		        decoded += symbols.indexOf(c);
		    }
		    return decoded;
		} catch (Exception e) { e.printStackTrace(); }
		return null;
	}
	
	public static String encode(Long decoded) {
		return encode(decoded, DEFAULT_SYMBOLS);
	}
	
	public static String encode(Long decoded, String symbols) {
		if (decoded == null) return null;
		try {
		    final int length = symbols.length();
//		    StringBuilder sb = new StringBuilder();
		    String result = "";
		    while (decoded != 0) {
//		        sb.append(symbols.charAt((int) (decoded % length)));
		    	result = symbols.charAt((int) (decoded % length)) + result;
		    	decoded /= length;
		    }
//		    return sb.reverse().toString();
		    return result;
		} catch (Exception e) { e.printStackTrace(); }
		return null;
	}

	/**
	 * A map to count things. When putting "things" inside the map counts the
	 * same ones. Afterwards the keys represent a set of unique items and the
	 * value is the count of how many times each item has been put.
	 * 
	 * @author Sven Buschbeck
	 * 
	 * @param <Type>
	 *            The type of the "things" to be counted.
	 */
	public static class Counter<Type> implements Serializable {
		private static final long serialVersionUID = 5230248238270734685L;
		private HashMap<Type, Integer> countMap = new HashMap<Type, Integer>();

		public Counter() {
		}

		public Counter<Type> clone() {
			Counter<Type> clone = new Counter<Type>();
			clone.countMap = (HashMap<Type, Integer>) countMap.clone();
			return clone;
		}

		public void add(Type object) {
			add(object, 1);
		}

		public void remove(Type object) {
			add(object, -1);
		}

		/**
		 * @param object
		 * @param i
		 */
		public void add(Type object, int delta) {
			countMap.put(object, get(object) + delta);
		}

		public int get(Type object) {
			return countMap.containsKey(object) ? countMap.get(object) : 0;
		}

		public Map<Type, Integer> getCountMap() {
			return countMap;
		}

		public List<KeyValuePair<Type, Integer>> getMappings() {
			return getMappings(true);
		}

		public List<KeyValuePair<Type, Integer>> getMappings(final boolean sortDescending, int maxLength) {
			LinkedList<KeyValuePair<Type, Integer>> mappings = (LinkedList<KeyValuePair<Type, Integer>>) getMappings(sortDescending);
			truncate(mappings, maxLength);
			return mappings;
		}

		public List<KeyValuePair<Type, Integer>> getMappings(final boolean sortDescending) {
			List<KeyValuePair<Type, Integer>> list = new LinkedList<KeyValuePair<Type, Integer>>();
			for (Type key : getObjects()) {
				list.add(new KeyValuePair<Type, Integer>(key, getCountMap().get(key)));
			}
			sort(list, new Comparator<KeyValuePair<Type, Integer>>() {

				@Override public int compare(KeyValuePair<Type, Integer> o1, KeyValuePair<Type, Integer> o2) {
					return sortDescending ? o2.value - o1.value : o1.value - o2.value;
				}
			});
			return list;
		}

		public Set<Type> getObjects() {
			return countMap.keySet();
		}

		public boolean isEmpty() {
			return countMap.isEmpty();
		}

		/**
		 * @return
		 */
		public int getMaxCount() {
			int max = 0;
			for (Integer count : countMap.values()) {
				if (count > max) {
					max = count;
				}
			}
			return max;
		}

		/**
		 * @return
		 */
		public int size() {
			return countMap.size();
		}
	}

	public static class CountMap<Type> extends Counter<Type> {
	};

	/**
	 * @param string
	 * @param string2
	 * @return
	 */
	public static int countCharacters(String string, String string2) {
		int count = 0;
		int lastIndex = -1;
		while ((lastIndex = string.indexOf(string2, lastIndex + 1)) > 0) {
			count++;
		}
		return count;
	}

	/**
	 * Escape an html string. Escaping data received from the client helps to
	 * prevent cross-site script vulnerabilities.
	 * 
	 * @param html
	 *            the html string to escape
	 * @return the escaped string
	 */
	public static String escapeHtml(String html) {
		if (html == null) {
			return null;
		}
		return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;");
	}

	/**
	 * Null-safe way to escape strings in several, combinable ways.
	 * 
	 * @param string
	 * @param smallCaps
	 * @param removeSpaces
	 * @param escapeHtml
	 * @return
	 */
	public static String escape(String string, boolean smallCaps, boolean removeSpaces, boolean escapeHtml) {
		if (isEmpty(string)) {
			return string;
		}
		if (smallCaps) {
			string = string.toLowerCase();
		}
		if (removeSpaces) {
			string = string.replaceAll(" ", "");
		}
		if (escapeHtml) {
			string = escapeHtml(string);
		}
		return string;
	}

	private static RegExp PATTERN_SANITIZE_SNW = RegExp.compile("[\\s\\W]", "gim");
	private static RegExp PATTERN_SANITIZE_NW = RegExp.compile("[\\W]", "gim");
	private static RegExp PATTERN_SANITIZE_SN = RegExp.compile("[\\s\\D]", "gim");
	private static RegExp PATTERN_SANITIZE_N = RegExp.compile("[\\D]", "gim");
	private static RegExp PATTERN_SANITIZE_S = RegExp.compile("[\\s]", "gim");

	/**
	 * Sanitizing string by truncating, transforming, and removing illegal
	 * characters.
	 * 
	 * @return
	 */
	public static String sanitize(String string, boolean toSmallCaps, int maxLength, boolean removeSpaces,
		boolean removeNonWordChars, boolean removeNumbers) {
		assert !(removeNonWordChars && removeNumbers) : "You can either remove all non-word characters (all others besides a..z, A..Z, and 0..9) or remove all non-numbers.";
		if (Util.isEmpty(string)) {
			return string;
		}
		// truncate twice: first to improve performance for regexp if the string is very long; 2x as rough estimation of what might be cut off due to the regexps
		if (maxLength > 0) {
			string = truncate(string, maxLength * 2);
		}
		string = sanitize(string, removeSpaces, removeNonWordChars, removeNumbers);
		if (toSmallCaps) {
			string = smallCaps(string);
		}
		if (maxLength > 0) {
			string = truncate(string, maxLength);
		}

		return string;
	}

	/**
	 * Sanitizing string by and removing illegal characters.
	 * 
	 * @return
	 */
	public static String sanitize(String string, boolean removeSpaces, boolean removeNonWordChars, boolean removeNumbers) {
		if (removeSpaces) {
			if (removeNonWordChars) {
				return PATTERN_SANITIZE_SNW.replace(string, "");
			}
			if (removeNumbers) {
				return PATTERN_SANITIZE_SN.replace(string, "");
			}
			return PATTERN_SANITIZE_S.replace(string, "");
		} else {
			if (removeNonWordChars) {
				return PATTERN_SANITIZE_NW.replace(string, "");
			}
			if (removeNumbers) {
				return PATTERN_SANITIZE_N.replace(string, "");
			}
		}
		return string;
	}

	public static String toString(Map<?, ?> map) {
		return (map == null) ? null : map.toString();
	}

	/**
	 * Null-safe to turn an object into a string.
	 * 
	 * @param object
	 * @return toString() or null
	 */
	public static String toString(Object object) {
		return (object == null) ? null : object.toString();
	}

	public static String toString(Object object, Object... attributes) {
		return joinAll("", getSimpleClassName(object), "(", joinAll("; ", attributes), ")");
	}

	public static String toString(ExtendedToString object, boolean extended) {
		return (object == null) ? null : object.toString(extended);
	}

	/**
	 * Returns true if the given string is a valid URL.
	 * 
	 * @param url
	 * @return True if the given string is a valid URL.
	 */
	public static boolean isValidUrl(String url) {
		return isValidUrl(url, true);
	}

	/**
	 * Returns true if the given string is a valid URL.
	 * 
	 * @param url
	 * @param topLevelDomainRequired
	 *            http://localhost/folder is a valid URL, but usually not
	 *            desired. Set to true to force the URL to have a TLD like
	 *            ".com".
	 * @return True if the given string is a valid URL.
	 */
	public static boolean isValidUrl(String url, boolean topLevelDomainRequired) {
		if (urlValidator == null || urlPlusTldValidator == null) {
			//			urlValidator = RegExp.compile("(http|https|ftp|mailto)://([\\w-]+\\.)+[\\w-]+(/[\\w- ./?%&=]*)?");
			//			urlValidator = RegExp
			//					.compile("(http|https){1}:\\/\\/(\\w+:{0,1}\\w*@)?(\\S+)(:[0-9]+)?(\\/|\\/([\\w#!:.?+=&%@!\\-\\/]))?");

			urlValidator = RegExp
				.compile("^((ftp|http|https)://[\\w@.\\-\\_]+(:\\d{1,5})?(/[\\w#!:.?+=&%@!\\_\\-/]*)*){1}$");
			//            protocol             something@      255.255 or domain  port      directories
			urlPlusTldValidator = RegExp
				.compile("^((ftp|http|https)://[\\w@.\\-\\_]+\\.[a-zA-Z1-9]{2,}(:\\d{1,5})?(/[\\w#!:.?+=&%@!\\_\\-/]*)*){1}$");
		}
		//		Log.info("shared.Util.isValidUrl: matches in \"" + url + "\": " + urlValidator.exec(url));
		return (topLevelDomainRequired ? urlPlusTldValidator : urlValidator).exec(url) != null;
	}

	/**
	 * If string != null and has some text.
	 * 
	 * @param string
	 * @return
	 */
	public static boolean notEmpty(String string) {
		return notEmpty(string, false);
	}
	
	/**
	 * If string != null and has some text. Eliminates white space if trimWhitespace is set true.
	 * 
	 * @param string
	 * @param trimWhitespace
	 * @return
	 */
	public static boolean notEmpty(String string, boolean trimWhitespace) {
		return !isEmpty(string, trimWhitespace);
	}
	
	/**
	 * Null-safe string trimming. Returns string.trim() or null if string is null.
	 * @param string
	 * @return
	 */
	public static String trim(String string) {
		return (string == null) ? null : string.trim();
	}
	
	/**
	 * Null-safe. Returns string.toLowerCase() or null if string is null.
	 * @param string
	 * @return
	 */
	public static String toLowerCase(String string) {
		return (string == null) ? null : string.toLowerCase();
	}

	/**
	 * Null-safe. Returns string.toUpperCase() or null if string is null.
	 * @param string
	 * @return
	 */
	public static String toUpperCase(String string) {
		return (string == null) ? null : string.toUpperCase();
	}
	
	/**
	 * If the iterator is not null and hasNext() is true.
	 * 
	 * @param string
	 * @return
	 */
	public static boolean notEmpty(Iterator<?> iterator) {
		return !isEmpty(iterator);
	}
	
	/**
	 * If collection != null and has elements.
	 * 
	 * @param collection
	 * @return
	 */
	public static boolean notEmpty(Collection<?> collection) {
		return !isEmpty(collection);
	}

	/**
	 * If array != null and has elements.
	 * 
	 * @param collection
	 * @return
	 */
	public static boolean notEmpty(Object[] array) {
		return !isEmpty(array);
	}

	/**
	 * If array != null and has elements.
	 * 
	 * @param collection
	 * @return
	 */
	public static boolean notEmpty(byte[] array) {
		return !isEmpty(array);
	}

	/**
	 * @param map
	 * @return
	 */
	public static boolean notEmpty(Map<?, ?> map) {
		return !isEmpty(map);
	}

	/**
	 * @param map
	 * @return
	 */
	public static boolean notEmpty(Iterable<?> iterable) {
		return !isEmpty(iterable);
	}

	/**
	 * If string == null or has no text.
	 * 
	 * @param string
	 * @return
	 */
	public static boolean isEmpty(String string) {
		return isEmpty(string, false);
	}

	/**
	 * If string == null, has no text. Eliminates white space if trimWhitespace is set true.
	 * 
	 * @param string
	 * @param trimWhitespace 
	 * @return
	 */
	public static boolean isEmpty(String string, boolean trimWhitespace) {
		return size(string) < 1;
	}
	
	/**
	 * If the iterator is null or hasNext() is false.
	 * 
	 * @param string
	 * @return
	 */
	public static boolean isEmpty(Iterator<?> iterator) {
		return iterator == null || !iterator.hasNext();
	}
	
	/**
	 * If collection == null or has no elements.
	 * 
	 * @param collection
	 * @return
	 */
	public static boolean isEmpty(Collection<?> collection) {
		return size(collection) < 1;
	}

	/**
	 * If array == null or has no elements.
	 * 
	 * @param collection
	 * @return
	 */
	public static boolean isEmpty(Object[] array) {
		return size(array) < 1;
	}

	/**
	 * If array == null, has zero elements or one only that is either null or an
	 * empty string.
	 * 
	 * @param collection
	 * @return
	 */
	public static boolean isEmpty(String[] array) {
		return size(array) < 1 || (size(array) == 1 && isEmpty(array[0]));
	}

	/**
	 * If array == null or has no elements.
	 * 
	 * @param collection
	 * @return
	 */
	public static boolean isEmpty(byte[] array) {
		return size(array) < 1;
	}

	/**
	 * @param map
	 * @return
	 */
	public static boolean isEmpty(Map<?, ?> map) {
		return size(map) < 1;
	}

	public static boolean isEmpty(Iterable<?> iterable) {
		Iterator<?> i;
		return iterable == null || (i = iterable.iterator()) == null || !i.hasNext();
	}

	public static int size(Collection<?> collection) {
		return (collection == null) ? -1 : collection.size();
	}

	public static int size(String string) {
		return size(string, false);
	}

	public static int size(String string, boolean trimWhitespace) {
		return (string == null) ? -1 : (trimWhitespace ? string.trim() : string).length();
	}
	
	public static int size(Object[] array) {
		return (array == null) ? -1 : array.length;
	}

	public static int size(Map<?, ?> map) {
		return (map == null) ? -1 : map.size();
	}

	public static int size(byte[] data) {
		return (data == null) ? -1 : data.length;
	}

	private static String indentBase = "                                                                                                                                                                                        ";
	private static DateTimeFormat formatAmericanDate;
	private static DateTimeFormat formatAmericanDateTime;
	private static DateTimeFormat formatAmericanDateTimeWithDay;

	/**
	 * @param indent
	 * @return
	 */
	public static String getIndentString(int indent) {
		return indentBase.substring(0, indent);
	}

	/**
	 * 
	 */
	public static int countOnes(long number) {
		return Long.bitCount(number);
	}
	
	/**
	 * Get first element of the given array
	 * @param array
	 * @return
	 */
	public static<T> T first(T[] array) {
		return (array == null || array.length < 1) ? null : array[0];
	}
	
	/**
	 * Get the first element from the given iterator.
	 * @param iterator
	 * @return
	 */
	public static<T> T first(Iterator<T> iterator) {
		if (notEmpty(iterator)) return iterator.next();
		return null;
	}
	
	public static<T> T first(Collection<T> collection) {
		return isEmpty(collection) ? null : collection.iterator().next();
	}

	public static<T> T last(T[] array) {
		return (array == null || array.length < 1) ? null : array[array.length-1];
	}

	/**
	 * @param running
	 * @param filter
	 * @return
	 */
	public static <T> int count(Collection<T> objects, Filter<T> filter) {
		if (objects == null) {
			return 0;
		}
		int count = 0;
		for (T object : objects) {
			if (filter.putIn(object)) {
				count++;
			}
		}
		return count;
	}

	public static <T extends HasId> List<String> getIds(Collection<T> collectionOfObjectsWithIds) {
		//		List<String> ids = new LinkedList<String>();
		//		if (collectionOfObjectsWithIds != null) {
		//			for (HasId withId : collectionOfObjectsWithIds) {
		//				ids.add(withId.getId());
		//			}
		//		}
		//		return ids;
		return mapList(collectionOfObjectsWithIds, true, false, new Util.Converter<T, String>() {

			@Override public String convert(T source) {
				return source.getId();
			}
		});
	}

	public static <T extends HasLabel> List<String> getLabels(Collection<T> collectionOfObjectsWithLabels) {
		//		List<String> labels = new LinkedList<String>();
		//		if (collectionOfObjectsWithLabels != null) {
		//			for (HasLabel withLabel : collectionOfObjectsWithLabels) {
		//				labels.add(withLabel.getLabel());
		//			}
		//		}
		//		return labels;

		return mapList(collectionOfObjectsWithLabels, true, false, new Converter<T, String>() {

			@Override public String convert(T source) {
				return source.getLabel();
			}
		});
	}

	public static <T extends HasId> List<T> getAllById(Collection<T> collectionOfObjectsWithIds, String id) {
		LinkedList<T> result = new LinkedList<T>();
		for (T objectWithId : collectionOfObjectsWithIds) {
			if (Util.equals(id, objectWithId.getId())) {
				result.add(objectWithId);
			}
		}
		return result;
	}

	public static <T extends HasId> T getById(Collection<T> collectionOfObjectsWithIds, String id) {
		return getById(collectionOfObjectsWithIds, id, true);
	}

	public static <T extends HasId> T getById(Collection<T> collectionOfObjectsWithIds, String id,
		boolean errorIfMoreThanOne) {
		List<T> results = getAllById(collectionOfObjectsWithIds, id);
		if (errorIfMoreThanOne) {
			assert results.size() < 2 : "#results=" + results.size() + "; results=" + results;
		}
		return isEmpty(results) ? null : results.get(0);
	}

	public static <T> List<T> filter(Collection<T> objects, Filter<T> filter) {
		return filter(objects, filter, false);
	}

	public static <T> List<T> filter(Collection<T> objects, Filter<T> filter, boolean removeFilteredFromInputList) {
		List<T> results = new LinkedList<T>();
		if (objects == null) {
			return results;
		}
		for (T object : objects) {
			if (filter.putIn(object)) {
				results.add(object);
			}
		}
		if (removeFilteredFromInputList) {
			objects.removeAll(results);
		}
		return results;
	}

	public static <T> T filterFirst(Collection<T> objects, Filter<T> filter, boolean removeFilteredFromInputList) {
		T result = null;
		if (objects == null) {
			return result;
		}
		for (T object : objects) {
			if (filter.putIn(object)) {
				result = object;
				break;
			}
		}
		if (removeFilteredFromInputList) {
			objects.remove(result);
		}
		return result;
	}

	public static interface Filter<T> {
		boolean putIn(T object);
	}

	public static int hslToRgbInt(float[] hsl) {
		float[] color = hslToRgb(hsl);
		int r = (int) (color[0] * (255.f) + 0.5f);
		int g = (int) (color[1] * (255.f) + 0.5f);
		int b = (int) (color[2] * (255.f) + 0.5f);
		return r << 16 | g << 8 | b;
	}

	public static float[] hslToRgb(float[] hsl) {
		float h = hsl[0];
		float s = hsl[1];
		float l = hsl[2];

		float c = (1 - Math.abs(2.f * l - 1.f)) * s;
		float h_ = h / 60.f;
		float h_mod2 = h_;
		if (h_mod2 >= 4.f) {
			h_mod2 -= 4.f;
		} else if (h_mod2 >= 2.f) {
			h_mod2 -= 2.f;
		}

		float x = c * (1 - Math.abs(h_mod2 - 1));
		float r_, g_, b_;
		if (h_ < 1) {
			r_ = c;
			g_ = x;
			b_ = 0;
		} else if (h_ < 2) {
			r_ = x;
			g_ = c;
			b_ = 0;
		} else if (h_ < 3) {
			r_ = 0;
			g_ = c;
			b_ = x;
		} else if (h_ < 4) {
			r_ = 0;
			g_ = x;
			b_ = c;
		} else if (h_ < 5) {
			r_ = x;
			g_ = 0;
			b_ = c;
		} else {
			r_ = c;
			g_ = 0;
			b_ = x;
		}

		float[] result = new float[3];
		float m = l - (0.5f * c);
		result[0] = r_ + m;
		result[1] = g_ + m;
		result[2] = b_ + m;
		return result;
	}

	public static <T> T firstNotNull(T... firstNotNullToBeReturned) {
		for (T item : firstNotNullToBeReturned) {
			if (item != null) {
				return item;
			}
		}
		return null;
	}

	/**
	 * @param resultIfNotNull
	 * @param defaultIfNull
	 * @return resultIfNotNull if null, or defaultIfNull otherwise
	 */
	public static <T> T notNull(T resultIfNotNull, T defaultIfNull) {
		return (resultIfNotNull != null) ? resultIfNotNull : defaultIfNull;
	}
	
	public static boolean noNulls(Object... objects) {
		return !anyNulls(objects);
	}
	
	public static boolean anyNulls(Object... objects) {
		return contains(objects, null);
	}

	/**
	 * @param x
	 * @param y
	 * @param targetX
	 * @param targetY
	 * @return
	 */
	public static double distance(double x, double y, double targetX, double targetY) {
		return Math.sqrt(sqr(targetX - x) + sqr(targetY - y));
	}

	/**
	 * @param d
	 * @return
	 */
	private static double sqr(double d) {
		return d * d;
	}

	/**
	 * Clones the map, keeping references to all keys and values.
	 * @param the map to be cloned
	 * @return
	 */
	public static <T1, T2> Map<T1, T2> clone(Map<T1, T2> map) {
		return new HashMap<T1, T2>(map);
	}
	
	public static byte[] clone(byte[] array) {
		if (array == null) return null;
		byte[] clone = new byte[array.length];
		System.arraycopy(array, 0, clone, 0, array.length);
		return clone;
	}

	public static interface Transformer<Type> {
		Type transform(Type original);
	}

	/**
	 * Runs all map values through the transformer and puts them back in the
	 * same map.
	 * 
	 * @param properties
	 * @return
	 */
	public static <T1, T2, MapType extends Map<T1, T2>> MapType transformValues(MapType map, Transformer<T2> transformer) {
		return transformValues(map, map, transformer);
	}

	/**
	 * Runs all map values through the given transformer and puts them in the
	 * new map with the same key.
	 * 
	 * @param map
	 * @param targetMap
	 * @param transformer
	 * @return
	 */
	public static <T1, T2, MapType extends Map<T1, T2>> MapType transformValues(MapType map, MapType targetMap,
		Transformer<T2> transformer) {
		return transformValues(map, targetMap, false, transformer);
	}

	public static <T1, T2, MapType extends Map<T1, T2>> MapType transformValues(MapType map, MapType targetMap,
		boolean ignoreNullValues, Transformer<T2> transformer) {
		for (T1 key : map.keySet()) {
			T2 value = map.get(key);
			if (ignoreNullValues && value == null) {
				continue;
			}
			targetMap.put(key, transformer.transform(value));
		}
		return map;
	}

	/**
	 * Copies the given collection but runs each value through the given
	 * transformer and puts the result in the given new collection.
	 * 
	 * @param collection
	 * @param newCollection
	 * @param transformer
	 * @return Given collection filled with all values run though the given
	 *         transformer.
	 */
	public static <Type, CollectionType extends Collection<Type>> CollectionType transform(CollectionType collection,
		CollectionType newCollection, Transformer<Type> transformer) {
		for (Type item : collection) {
			newCollection.add(transformer.transform(item));
		}
		return newCollection;
	}

	/**
	 * @return height of screen this browser window is on
	 */
	public static native int getScreenWidth()/*-{
		return window.screen.width;
	}-*/;

	/**
	 * @return width of screen this browser window is on
	 */
	public static native int getScreenHeight()/*-{
		return window.screen.height;
	}-*/;

	/**
	 * Can do %s only for now. Will be improved on demand.
	 * 
	 * @param template
	 * @param data
	 * @return
	 */
	public static String format(String template, Object... data) {
		for (Object item : data) {
			template = template.replaceFirst("%s", (item == null) ? "" : item.toString());
		}
		return template;
	}

	/**
	 * @return
	 */
	public static boolean isClient() {
		try {
			// sometimes ends up with "java.lang.NoClassDefFoundError: com/google/gwt/core/client/GWTBridge"
			return GWT.isClient();
		} catch (Throwable e) {
		}
		return false;
	}

	/**
	 * Maps all elements from source to the target type (given by the return value), ignoring null.
	 * @param source collection
	 * @param mapper Converter<Source, Target>
	 * @return collection of target type
	 */
	public static <SourceCollectionType extends Collection<SourceType>, TargetCollectionType extends Collection<TargetType>, SourceType, TargetType> TargetCollectionType mapList(
			SourceCollectionType source, Converter<SourceType, TargetType> mapper) {
		return mapList(source, true, true, mapper);
	}
	
	/**
	 * Maps all elements from source to the target type (given by the return value), ignoring null.
	 * @param source collection
	 * @param target any kind of collection (allows to specify the collection type to be used)
	 * @param mapper Converter<Source, Target>
	 * @return collection of target type
	 */
	public static <SourceCollectionType extends Collection<SourceType>, TargetCollectionType extends Collection<TargetType>, SourceType, TargetType> TargetCollectionType mapList(
			SourceCollectionType source, TargetCollectionType target, Converter<SourceType, TargetType> mapper) {
		return mapList(source, target, true, true, mapper);
	}
	
	public static <SourceCollectionType extends Collection<SourceType>, TargetCollectionType extends Collection<TargetType>, SourceType, TargetType> TargetCollectionType mapList(
		SourceCollectionType source, boolean ignoreNullSources, boolean removeNullMappings,
		Converter<SourceType, TargetType> mapper) {
		return (TargetCollectionType) mapList(source, new LinkedList<TargetType>(), ignoreNullSources,
			removeNullMappings, mapper);
	}

	public static <SourceCollectionType extends Collection<SourceType>, TargetCollectionType extends Collection<TargetType>, SourceType, TargetType> TargetCollectionType mapList(
		SourceCollectionType source, TargetCollectionType target, boolean ignoreNullSources,
		boolean removeNullMappings, Converter<SourceType, TargetType> mapper) {
		target.clear();
		if (source != null) {
			// if there is no source, instead of retuning target=null, this method returns target=[] to avoid all that "funny" NPEs... thus, the underlying assumption is empty list equals no list
			for (SourceType s : source) {
				if (ignoreNullSources && s == null) {
					continue;
				}
				TargetType t = mapper.convert(s);
				if (!removeNullMappings || t != null) {
					target.add(t);
				}
			}
		}
		return target;
	}

	public static <SourceMatrix extends Collection<SourceRow>, SourceRow extends Collection<SourceType>, TargetMatrix extends Collection<? extends Collection<TargetType>>, SourceType, TargetType> TargetMatrix mapMatrix(
		SourceMatrix source, final boolean ignoreNullSources, final boolean removeNullMappings,
		final Converter<SourceType, TargetType> mapper) {
		//		LinkedList<LinkedList<TargetType>> target = new LinkedList<LinkedList<TargetType>>();
		TargetMatrix target = (TargetMatrix) Util.mapList(source, new LinkedList<LinkedList<TargetType>>(),
			ignoreNullSources, removeNullMappings, new Util.Converter<SourceRow, LinkedList<TargetType>>() {

				@Override public LinkedList<TargetType> convert(SourceRow sourceRow) {
					LinkedList<TargetType> targetRow = Util.mapList(sourceRow, new LinkedList<TargetType>(),
						ignoreNullSources, removeNullMappings, mapper);
					return Util.notEmpty(targetRow) ? targetRow : null;
				}
			});
		return target;
	}

	public static <Matrix extends Collection<? extends Collection<?>>> int maxWidth(Matrix matrix) {
		int max = 0;
		for (Collection<?> line : matrix) {
			max = Math.max(size(line), max);
		}
		return max;
	}

}
