/**
 * 
 */
package gwt.tools.shared;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class Constants {

	public static final long TIME_ONE_SECONDS = 1000;
	public static final long TIME_ONE_MINUTE = TIME_ONE_SECONDS * 60;
	public static final long TIME_ONE_HOUR = TIME_ONE_MINUTE * 60;
	public static final long TIME_ONE_DAY = TIME_ONE_HOUR * 24;
	public static final long TIME_ONE_YEAR = TIME_ONE_DAY * 365;
	public static final long TIME_ONE_MONTH = TIME_ONE_DAY * 30;
	public static final long TIME_ONE_WEEK = TIME_ONE_DAY * 7;
	/**
	 * 
	 */
	public static final String RDF_MIME_TYPE = "application/rdf+xml";
	/**
	 * when the post data is in the format used for a get
	 */
	public static final String URLENCODED_MIME_TYPE = "application/x-www-form-urlencoded";

	//	public static final Charset UTF8 = Charsets.UTF_8;
	public static final String UTF8 = "UTF-8";
	public static final String CONTENT_TYPE_HEADER = "Content-Type";

	/**
	 * Maximum length of an URL. More might be possible but is unrelyable.
	 */
	public static final int MAX_URL_LENGTH = 2000;

	public static final double DEG_TO_RAD = 0.0174532925199433;
	public static final double EPSILON = 1e-12;
}
