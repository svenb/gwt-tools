/**
 * 
 */
package gwt.tools.shared.handler;

/**
 * 
 * 
 * @author Sven Buschbeck
 * @param <DataType>
 * 
 */
public interface SimpleCallbackWithData<DataType> {

	void done(DataType data, boolean successful);

}
