/**
 * 
 */
package gwt.tools.shared.handler;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public interface SimpleCallback {

	void done();

}
