/**
 * 
 */
package gwt.tools.shared.handler;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public interface SimpleCallbackMaybeSuccessful {

	void done(boolean successful);

}
