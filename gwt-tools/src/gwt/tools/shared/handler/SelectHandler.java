/**
 * 
 */
package gwt.tools.shared.handler;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public interface SelectHandler<SourceType> {

	<T extends SourceType> void onSelect(T selected);

}
