package gwt.tools.shared;

public class GeoPoint {
	public final double lat;
	public final double lng;

	public GeoPoint(double lat, double lng) {
		this.lat = lat;
		this.lng = lng;
	}
	
	public double getLat() { return lat; }
	public double getLon() { return lng; }
	
	@Override
	public String toString() {
		return Util.toString(this, lat, lng);
	}
	
}