package gwt.tools.shared;

import java.io.Serializable;

public class GeoRect implements Serializable {

	public double north;
	public double east;
	public double south;
	public double west;

	/**
	 * assumes east > west; north > south
	 * 
	 * @param east
	 * @param north
	 * @param west
	 * @param south
	 */
	public GeoRect(double north, double east, double south, double west) {

		//		double north_, south_;
		//		if (south > north) {
		//			south_ = north;
		//			north_ = south;
		//		} else {
		//			south_ = south;
		//			north_ = north;
		//		}

		this.north = north;
		this.east = east;
		this.south = south;
		this.west = west;

	}

	public double getCenterLat() {
		return (north + south) / 2;
	}

	public double getCenterLng() {
		return (east + west) / 2;
	}

	public double getArea() {
		//			return getArea(this);
		return getWidth() * getHeight();
	}

	public boolean doesOverlap(GeoRect other) {
		return getIntersection(other).getArea() > 0;
	}

	public GeoRect getIntersection(GeoRect other) {
		//			double ix = Math.max(x, other.x);
		//			double iy = Math.max(y, other.y);
		//			double ir = Math.min(r, other.r);
		//			double ib = Math.min(b, other.b);
		// TODO does not accommodate crossing 180/-180
		double dNorth = Math.min(north, other.north);
		double dEast = Math.min(east, other.east);
		double dSouth = Math.max(south, other.south);
		double dWest = Math.max(west, other.west);

		if (dNorth < dSouth || dEast < dWest) {
			return new GeoRect(0, 0, 0, 0);
		}
		return new GeoRect(dNorth, dEast, dSouth, dWest);
	}

	/**
	 * @param other
	 * @return
	 */
	public double getRelativeOverlap(GeoRect other) {
		return getOverlap(other) / getArea();
	}

	/**
	 * @param other
	 * @return
	 */
	public double getOverlap(GeoRect other) {
		return getIntersection(other).getArea();
	}

	public boolean contains(double lat, double lng) {
		return Util.contains(lat, lng, south, west, north, east);
	}

	/**
	 * @param other
	 * @return
	 */
	public boolean contains(GeoRect other) {
		return west <= other.west && south <= other.south && east >= other.east && north >= other.north;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override public String toString() {
		return "GeoRect(ne(" + north + ", " + east + "), sw(" + south + ", " + west + "))";
	}

	/**
	 * @return
	 */
	public double getWidth() {
		// overcome issue when cross 180 to -180
		return Math.min(Math.abs(east - west), Math.abs(west - east));
	}

	/**
	 * @return
	 */
	public double getHeight() {
		return Math.min(Math.abs(north - south), Math.abs(south - north));
	}

	protected GeoRect clone() {
		return new GeoRect(north, east, south, west);
	}

	public double getNorth() { return north; }
	public double getEast() { return east; }
	public double getSouth() { return south; }
	public double getWest() { return west; }

}