/**
 * 
 */
package gwt.tools.shared;

import static gwt.tools.shared.Util.inRange;

import java.util.LinkedList;
import java.util.List;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class GeoHash {

	public static final int GEO_HASH_WORLD_SCALE_DEPTH = 30;

	public static final Double GEO_HASH_MAX_ACCURACY = null;
	public static final Double GEO_HASH_HIGH_ACCURACY = .75;
	public static final Double GEO_HASH_DEFAULT_ACCURACY = .5;
	public static final Double GEO_HASH_LOW_ACCURACY = .2;

	public static final int NORTH = 90;
	public static final int EAST = 180;
	public static final int SOUTH = -90;
	public static final int WEST = -180;
	public static final GeoRect BOUNDING_BOX = new GeoRect(NORTH, EAST, SOUTH, WEST);

	public static class GeoHashRange {
		public long min, max;

		public GeoHashRange(long min, long max) {
			this.min = min;
			this.max = max;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override public boolean equals(Object object) {
			return object != null && object instanceof GeoHashRange && equals((GeoHashRange) object);
		}

		public boolean equals(GeoHashRange range) {
			return range != null && range.min == min && range.max == max;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override public String toString() {
			return "Range(" + min + "(" + Long.toHexString(min) + "), " + max + "(" + Long.toHexString(max) + "))";
		}
	}

	public GeoHash() {
		this(GEO_HASH_DEFAULT_ACCURACY);
	}

	public GeoHash(Double accuracy) {
		this(new GeoRect(NORTH, EAST, SOUTH, WEST), GEO_HASH_WORLD_SCALE_DEPTH, accuracy);
	}

	public GeoHash(int hashDepth, Double accuracy) {
		this(new GeoRect(NORTH, EAST, SOUTH, WEST), hashDepth, accuracy);
	}

	private final GeoRect boundingBox;
	private final int hashDepth;
	private Double accuracy;
	public int unifiedRanges;
	private boolean precise;

	/**
	 * @param boundingBox
	 * @param hashDepth
	 *            number of bit pairs to generate - more means more effort in
	 *            searching but more precision, too. For earth, 17 is precise
	 *            down to about 10cm.
	 * @param accuracy
	 *            It's guaranteed that all items within the b.box will be
	 *            covered by the returned ranges. A lower accuracy will result
	 *            in more items being fetched outside of the box but reduce
	 *            number of required ranges to be queried.
	 */
	public GeoHash(GeoRect boundingBox, int hashDepth, Double accuracy) {
		this.boundingBox = boundingBox;
		this.hashDepth = hashDepth;
		setAccuracy(accuracy);
	}

	/**
	 * Set the accuracy with which ranges around a bounding box should be generated. Set 1d or null to be precise - WARNING: this will generate a huge amount of ranges. A value of .2 seems somewhat optimal, it will produce a precision of .5 by generating about 10 ranges in <300µs.
	 * @param accuracy
	 */
	public void setAccuracy(Double accuracy) {
		precise = accuracy == null || accuracy > .99d;
		this.accuracy = !precise ? Util.keepInRange(0, 1, accuracy) : null;
	}

	public long get(double lat, double lon) {
		return get(new GeoPoint(lat, lon));
	}

	public long get(GeoPoint point) {
		return compileGeoHash(getBitPairs(point));
	}

	/**
	 * Calculates GeoHash ranges to retrieve all points covered by the given
	 * bounding box. Higher accuracy will return less out-of-box items but
	 * increases amount of ranges required to query for the items easily by 10.
	 * Depth also increases the accuracy. Experiments suggest an accuracy of 75%
	 * and depth of 2 bytes as optimal. A depth of 4 bytes
	 * (GEO_HASH_STRONG_DEPTH) might only be required for precisely mapping
	 * global earth coordinates. Otherwise, it high depth will just cause
	 * useless computing overhead and more ranges.
	 * 
	 * It's guaranteed that all items within the b.box will be covered by the
	 * returned ranges. A lower accuracy will result in more items being fetched
	 * outside of the box but reduce number of required ranges to be queried.
	 * 
	 * @param left
	 *            left side of bounding box (x)
	 * @param top
	 *            top side of bounding box (y)
	 * @param right
	 *            right side of bounding box (x+width)
	 * @param bottom
	 *            bottom side of bounding box (y+height)
	 * @return A list of hash ranges that would need to be queried for to
	 *         retrieve all items within the given bounding box
	 */
	public List<GeoHashRange> getBoundingBoxHashes(double north, double east, double south, double west) {
		return getBoundingBoxHashes(new GeoRect(north, east, south, west));
	}

	public List<GeoHashRange> getBoundingBoxHashes(GeoRect box) {
		LinkedList<GeoHashRange> results = new LinkedList<GeoHashRange>();
		byte[] bitPairs = createBitArray();
		//		System.out.println("box: (" + left + "," + top + "),(" + right + "," + bottom + ")");
		unifiedRanges = 0;
		getHashRanges(boundingBox.clone(), box, bitPairs, 0, results);
		return results;
	}

	//	private static void getHashRanges(int left, int top, int right, int bottom, GeoRect box, byte[] bitPairs,
	//		final int depth, Double accuracy, LinkedList<GeoHashRange> hashRanges) {
	private void getHashRanges(GeoRect sector, GeoRect box, byte[] bitPairs, final int depth,
		LinkedList<GeoHashRange> hashRanges) {
		//		String intent = "";
		//		for (int x = 0; x < depth; x++) {
		//			intent += ' ';
		//		}
		//		String intent = Util.getIndentString(depth);
		//		boolean leftInBox = inRange(box.x, box.r, left);
		//		boolean rightInBox = inRange(box.x, box.r, right);
		//		boolean topInBox = inRange(box.y, box.b, bottom);
		//		boolean bottomInBox = inRange(box.y, box.b, top);
		boolean northInBox = inRange(sector.north, box.south, box.north);
		boolean eastInBox = inRange(sector.east, box.west, box.east);
		boolean southInBox = inRange(sector.south, box.south, box.north);
		boolean westInBox = inRange(sector.west, box.west, box.east);
		boolean sectorIsIncludedInBox = northInBox && eastInBox && southInBox && westInBox;
		// OPTIMIZE_ accept sector if sector overlaps with box to x%, e.g. 95%; make percentage depend on depth and maybe ranges already assigned
		if (!sectorIsIncludedInBox && !precise) {
			//			sectorIsIncludedInBox = new GeoRect(left, top, right, bottom).getRelativeOverlap(box) >= accuracy;
			sectorIsIncludedInBox = sector.getRelativeOverlap(box) >= accuracy;
		}

		// if this sector is completely included in the box
		//		System.out.print(intent + "sector: " + sector + ": ");
		boolean isLastLevel = (depth + 1) >= bitPairs.length;
		if (sectorIsIncludedInBox || isLastLevel) {
			//			System.out.println(" inBox=" + sectorIsIncludedInBox + "; isLastLevel=" + isLastLevel);
			//    Range range = new Range();
//			byte[] min = bitPairs.clone(); <primitive array>.clone() does not work in GWT client side :(
//			byte[] max = bitPairs.clone();
			byte[] min = Util.clone(bitPairs);
			byte[] max = Util.clone(bitPairs);
			for (int currentDepth = depth; currentDepth < bitPairs.length; currentDepth++) {
				//    range.minHash = sectionID filled with 0'x from right side -- cleaning up bitPair's lower pairs
				min[currentDepth] = 0;
				//    range.maxHash = sectionID filled with 1's from right side
				max[currentDepth] = 0x3;
			}
			//    result.add(range);
			//				hashRanges.add(new GeoHashRange(get(left, top), get(right, bottom)));
			long maxHash = compileGeoHash(max);
			long minHash = compileGeoHash(min);
			GeoHashRange previousRange = (hashRanges.size() > 0) ? hashRanges.getLast() : null;
			// OPTIMIZE_ Unify sectors directly connected to each other, i.e. where rangeA.maxHash+1 = rangeB.minHash; might require sorting ranges by min/max first. Might be done one the fly!
			if (previousRange != null && previousRange.max + 1 == minHash) {
				unifiedRanges++;
				//				System.out.println(intent + "GeoHash.getHashRanges: unify hashs: previous=" + previousRange
				//					+ "; current.min=" + Long.toHexString(minHash) + "; .max=" + Long.toHexString(maxHash));
				previousRange.max = maxHash;
			} else {
				hashRanges.add(new GeoHashRange(minHash, maxHash));
			}
			//			System.out.println(intent + "current ranges=" + Util.join(hashRanges));
			return;
		}
		// if sector is overlapping with box
		boolean sectorPartiallyInBox = (northInBox && eastInBox) || (southInBox && eastInBox)
			|| (northInBox && westInBox) || (southInBox && westInBox);
		//		boolean leftInSector = inRange(left, right, box.x);
		//		boolean rightInSector = inRange(left, right, box.r);
		//		boolean topInSector = inRange(top, bottom, box.y);
		//		boolean bottomInSector = inRange(top, bottom, box.b);
		boolean northInSector = inRange(box.north, sector.south, sector.north);
		boolean eastInSector = inRange(box.east, sector.west, sector.east);
		boolean southInSector = inRange(box.south, sector.south, sector.north);
		boolean westInSector = inRange(box.west, sector.west, sector.east);

		//		boolean boxPartiallyInSector = (leftInSector && topInSector) || (rightInSector && topInSector)
		//			|| (leftInSector && bottomInSector) || (rightInSector && topInSector);
		boolean boxPartiallyInSector = (northInSector && eastInSector) || (southInSector && eastInSector)
			|| (northInSector && westInSector) || (southInSector && westInSector);
		if (sectorPartiallyInBox || boxPartiallyInSector) {
			//			System.out.println(" sInBox=" + sectorPartiallyInBox + "; bInSector=" + boxPartiallyInSector);
			//    one step down into sector all sub-sectors (depth + 1)
			//			int sectorCenterX = (left + right) >> 1;
			//			int sectorCenterY = (top + bottom) >> 1;
			double sectorCenterLat = sector.getCenterLat();
			double sectorCenterLng = sector.getCenterLng();
			// TODO is it right that the same value is set multiple times? Thus overwriting the previous one...?
			// order: north = _1; south = _0; east = 1_; west = 0_
			bitPairs[depth] = 0; // 00 south west
			getHashRanges(new GeoRect(sectorCenterLat, sectorCenterLng, sector.south, sector.west), box, bitPairs,
				depth + 1, hashRanges);
			bitPairs[depth] = 1; // 01 north west
			getHashRanges(new GeoRect(sector.north, sectorCenterLng, sectorCenterLat, sector.west), box, bitPairs,
				depth + 1, hashRanges);
			bitPairs[depth] = 2; // 10 south east
			getHashRanges(new GeoRect(sectorCenterLat, sector.east, sector.south, sectorCenterLng), box, bitPairs,
				depth + 1, hashRanges);
			bitPairs[depth] = 3; // 11 north east
			getHashRanges(new GeoRect(sector.north, sector.east, sectorCenterLat, sectorCenterLng), box, bitPairs,
				depth + 1, hashRanges);
			return;
		}
		//		System.out.println(" is off");
		// ignore sectors not overlapping with box at all
		// search all object in box by iterating over all ranges and search for them + merge all result sets.
		// ### TEST: resulting sets must (should) not overlap
	}

	/**
	 * Filter the baseRanges by the given second ranges to produce a set of
	 * delta ranges; i.e. the resulting ranges do not contain any from the
	 * second set of ranges. Use: when querying for items in a moving bounding
	 * box; putting in the new bounding box and subtracting the previous
	 * bounding box will result in ranges that exactly query for that set of
	 * items that have not been included in the previous bounding box already.
	 * Aka. "querying for a delta frame".
	 * 
	 * @param baseRanges
	 * @param rangesToBeSubtractedFromBase
	 * @return delta ranges, i.e. base ranges without the subtracted ranges
	 */
	public LinkedList<GeoHashRange> subtractRanges(List<GeoHashRange> baseRanges,
		List<GeoHashRange> rangesToBeSubtractedFromBase) {
		LinkedList<GeoHashRange> diffRanges = new LinkedList<GeoHashRange>();

		for (GeoHashRange baseRange : baseRanges) {
			GeoHashRange previousRange = null;
			boolean touched = false;
			for (GeoHashRange subtractRange : rangesToBeSubtractedFromBase) {
				if (inRange(subtractRange.min, subtractRange.max, baseRange.min, baseRange.max)) {
					touched = true;
					// base range is completely covered by subtract range, thus, gets deleted
					continue;
				}
				if (inRange(baseRange.min, baseRange.max, subtractRange.min, subtractRange.max)) {
					touched = true;
					// subtract range overlapping a part within the base; thus, we have to split the range and exclude the subtract range
					if (previousRange != null) {
						// this base range overlapped several times with subtract ranges
						assert previousRange.max != subtractRange.min : "The previous subtract range must have touched this subtract range. Ranges that touch should have been merged beforehand.";
						previousRange.max = subtractRange.min;
					} else {
						// baseRange.min == subtractRange.min if subtract range just starts at the beginning of the base range
						if (baseRange.min < subtractRange.min) {
							previousRange = new GeoHashRange(baseRange.min, subtractRange.min);
							diffRanges.add(previousRange);
						}
					}
					// subtractRange.max == baseRange.max means that the subtract range just end at the end of the base range
					if (subtractRange.max < baseRange.max) {
						previousRange = new GeoHashRange(subtractRange.max, baseRange.max);
						diffRanges.add(previousRange);
					}
					continue;
				}
				if (Util.inRange(baseRange.min, baseRange.max, subtractRange.max)) {
					touched = true;
					// cutting off the beginning
					assert previousRange == null;
					previousRange = new GeoHashRange(subtractRange.max, baseRange.max);
					diffRanges.add(previousRange);
					continue;
				}
				if (Util.inRange(baseRange.min, baseRange.max, subtractRange.min)) {
					touched = true;
					// cut the end off + we can end this loop because there can't be any other subtract range overlapping with this base as that would mean that this subtract and the other subtract range would overlap.
					if (previousRange != null) {
						previousRange.max = subtractRange.min;
					} else {
						previousRange = new GeoHashRange(baseRange.min, subtractRange.min);
						diffRanges.add(previousRange);
					}
					break;
				}
			}
			if (!touched) {
				// base range did not overlap with any subtract range, so we just keep it the way it was
				diffRanges.add(new GeoHashRange(baseRange.min, baseRange.max));
			}
		}

		// in all tests, non of the ranges have been merged 
		//		GeoHashRange previous = null;
		//		int merged = 0;
		//		for (GeoHashRange diffRange : diffRanges) {
		//			if (previous == null) {
		//				previous = diffRange;
		//				continue;
		//			}
		//			if (previous.max == diffRange.min + 1) {
		//				previous.max = diffRange.max;
		//				diffRanges.remove(diffRange);
		//				merged++;
		//			}
		//		}
		//		System.out.println("GeoHash.subtractRanges: merged=" + merged);

		return diffRanges;
	}

	private byte[] getBitPairs(GeoPoint point) {
		return getBitPairs(point, boundingBox, 0, createBitArray());
	}

	private byte[] createBitArray() {
		return new byte[hashDepth];
	}

	private byte[] getBitPairs(GeoPoint point, GeoRect box, int depth, byte[] bitPairs) {
		//		System.out.println(x + "," + y + " - (" + left + "," + right + "),(" + top + "," + bottom + ")");
		bitPairs[depth] = 0;
		//		if (x > centerX) {
		//			bitPairs[depth] = 2; // 10
		//			left = centerX;
		//		} else {
		//			right = centerX;
		//		}
		//		if (y > centerY) {
		//			bitPairs[depth] += 1; // 01
		//			top = centerY;
		//		} else {
		//			bottom = centerY;
		//		}
		//		if (depth < bitPairs.length - 1) {
		//			getBitPairs(x, y, left, top, right, bottom, depth + 1, bitPairs);
		//		}
		// order:      00          01          10         11
		//        north east, south east, north west, south west
		// -> 1_ = west, 0_ = east
		// -> _1 = south _0 = north
		// new order: north = _1; south = _0; east = 1_; west = 0_ (= official order of geohash.org)

		GeoRect subBox = box.clone();
		if (point.lat >= box.getCenterLat()) {
			subBox.south = box.getCenterLat();
			bitPairs[depth] |= 1; // 01
		} else {
			subBox.north = box.getCenterLat();
		}
		if (point.lng >= box.getCenterLng()) {
			subBox.west = box.getCenterLng();
			bitPairs[depth] |= 2; // 10
		} else {
			subBox.east = box.getCenterLng();
		}
		if (depth < bitPairs.length - 1) {
			getBitPairs(point, subBox, depth + 1, bitPairs);
		}
		//		System.out.println(bitPairs);
		return bitPairs;
	}

	private static long compileGeoHash(byte[] pairs) {
		long result = 0;
		for (byte pair : pairs) {
			result <<= 2;
			result |= (pair & 0x3);
		}
		return result;
	}

	private static String gcBase32 = "0123456789bcdefghjkmnpqrstuvwxyz";

	public static Long decode(String geoHashBase32) {
		Long number = 0l;
		for (int x = 0; x < geoHashBase32.length(); x++) {
			number <<= 5;
			number |= gcBase32.indexOf(geoHashBase32.charAt(x));
		}
		return number;
	}

	public String encode(Long hash) {
		return encode(hash, (hashDepth * 2) / 5);
	}

	public static String encode(Long code, int length) {
		String result = "";
		for (int x = 0; x < length; x++) {
			result = gcBase32.charAt((int) (code & 0x1f)) + result;
			code >>= 5;
		}
		return result;
	}
	
	public GeoRect calculateBoundingBox(GeoPoint center, double minRadiusInMeters) {
		GeoPoint north = moveInDirection(center, 0, minRadiusInMeters);
		GeoPoint east = moveInDirection(center, 90, minRadiusInMeters);
		GeoPoint south = moveInDirection(center, 180, minRadiusInMeters);
		GeoPoint west = moveInDirection(center, 270, minRadiusInMeters);
		Util.print(this, "calculateBoundingBox", "north, east, south, west", north, east, south, west);
		return new GeoRect(north.lat, east.lng, south.lat, west.lng);
	}

	/**
	 * returns the {@link GeoPoint} that is in the given direction at the following
	 * radiusInKm of the given point.<br>
	 * Uses Vincenty's formula and the WGS84 ellipsoid.
	 * 
	 * Copyright 2010, Silvio Heuberger @ IFS www.ifs.hsr.ch
	 * 
	 * @param directionInDegrees must be within 0 and 360, south=0, east=90, north=180, west=270
	 */
	public static GeoPoint moveInDirection(GeoPoint point, double directionInDegrees, double distanceInMeters) {

		if (directionInDegrees < 0 || directionInDegrees > 360) {
			throw new IllegalArgumentException("direction must be in (0,360)");
		}

		double a = 6378137, b = 6356752.3142, f = 1 / 298.257223563; // WGS-84
		// ellipsiod
		double alpha1 = directionInDegrees * Constants.DEG_TO_RAD;
		double sinAlpha1 = Math.sin(alpha1), cosAlpha1 = Math.cos(alpha1);

		double tanU1 = (1 - f) * Math.tan(point.lat * Constants.DEG_TO_RAD);
		double cosU1 = 1 / Math.sqrt((1 + tanU1 * tanU1)), sinU1 = tanU1 * cosU1;
		double sigma1 = Math.atan2(tanU1, cosAlpha1);
		double sinAlpha = cosU1 * sinAlpha1;
		double cosSqAlpha = 1 - sinAlpha * sinAlpha;
		double uSq = cosSqAlpha * (a * a - b * b) / (b * b);
		double A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
		double B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));

		double sinSigma = 0, cosSigma = 0, cos2SigmaM = 0;
		double sigma = distanceInMeters / (b * A), sigmaP = 2 * Math.PI;
		while (Math.abs(sigma - sigmaP) > 1e-12) {
			cos2SigmaM = Math.cos(2 * sigma1 + sigma);
			sinSigma = Math.sin(sigma);
			cosSigma = Math.cos(sigma);
			double deltaSigma = B
				* sinSigma
				* (cos2SigmaM + B
					/ 4
					* (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) - B / 6 * cos2SigmaM
						* (-3 + 4 * sinSigma * sinSigma) * (-3 + 4 * cos2SigmaM * cos2SigmaM)));
			sigmaP = sigma;
			sigma = distanceInMeters / (b * A) + deltaSigma;
		}

		double tmp = sinU1 * sinSigma - cosU1 * cosSigma * cosAlpha1;
		double lat2 = Math.atan2(sinU1 * cosSigma + cosU1 * sinSigma * cosAlpha1,
			(1 - f) * Math.sqrt(sinAlpha * sinAlpha + tmp * tmp));
		double lambda = Math.atan2(sinSigma * sinAlpha1, cosU1 * cosSigma - sinU1 * sinSigma * cosAlpha1);
		double C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
		double L = lambda - (1 - C) * f * sinAlpha
			* (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM)));

		double newLat = lat2 / Constants.DEG_TO_RAD;
		double newLon = point.lng + L / Constants.DEG_TO_RAD;

		newLon = (newLon > 180.0 ? 360.0 - newLon : newLon);
		newLon = (newLon < -180.0 ? 360.0 + newLon : newLon);

		return new GeoPoint(newLat, newLon);
	}

	/**
	 * Copyright 2010, Silvio Heuberger @ IFS www.ifs.hsr.ch
	 * 
	 * @param foo
	 * @param bar
	 * @return
	 */
	public static double distanceInMeters(GeoPoint foo, GeoPoint bar) {
		double a = 6378137, b = 6356752.3142, f = 1 / 298.257223563; // WGS-84
		// ellipsiod
		double L = (bar.lng - foo.lng) * Constants.DEG_TO_RAD;
		double U1 = Math.atan((1 - f) * Math.tan(foo.lat * Constants.DEG_TO_RAD));
		double U2 = Math.atan((1 - f) * Math.tan(bar.lat * Constants.DEG_TO_RAD));
		double sinU1 = Math.sin(U1), cosU1 = Math.cos(U1);
		double sinU2 = Math.sin(U2), cosU2 = Math.cos(U2);

		double cosSqAlpha, sinSigma, cos2SigmaM, cosSigma, sigma;

		double lambda = L, lambdaP, iterLimit = 20;
		do {
			double sinLambda = Math.sin(lambda), cosLambda = Math.cos(lambda);
			sinSigma = Math.sqrt((cosU2 * sinLambda) * (cosU2 * sinLambda)
				+ (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda) * (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda));
			if (sinSigma == 0) {
				return 0; // co-incident points
			}
			cosSigma = sinU1 * sinU2 + cosU1 * cosU2 * cosLambda;
			sigma = Math.atan2(sinSigma, cosSigma);
			double sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma;
			cosSqAlpha = 1 - sinAlpha * sinAlpha;
			cos2SigmaM = cosSigma - 2 * sinU1 * sinU2 / cosSqAlpha;
			if (cos2SigmaM == Double.NaN) {
				cos2SigmaM = 0; // equatorial line: cosSqAlpha=0 (�6)
			}
			double C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
			lambdaP = lambda;
			lambda = L + (1 - C) * f * sinAlpha
				* (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM)));
		} while (Math.abs(lambda - lambdaP) > Constants.EPSILON && --iterLimit > 0);

		if (iterLimit == 0) {
			return Double.NaN;
		}
		double uSquared = cosSqAlpha * (a * a - b * b) / (b * b);
		double A = 1 + uSquared / 16384 * (4096 + uSquared * (-768 + uSquared * (320 - 175 * uSquared)));
		double B = uSquared / 1024 * (256 + uSquared * (-128 + uSquared * (74 - 47 * uSquared)));
		double deltaSigma = B
			* sinSigma
			* (cos2SigmaM + B
				/ 4
				* (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) - B / 6 * cos2SigmaM * (-3 + 4 * sinSigma * sinSigma)
					* (-3 + 4 * cos2SigmaM * cos2SigmaM)));
		double s = b * A * (sigma - deltaSigma);

		return s;
	}
}
