/**
 * 
 */
package gwt.tools.shared;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class TemplateRenderer {
	public static final SafeHtml DEFAULT_EMPTY_VALUE = SafeHtmlUtils.fromTrustedString("&mdash;");

	private final SafeHtml emptyHtml;
	private final String empty;
	private final CustomValueFormatter formatter;

	public String template;

	public TemplateRenderer() {
		this(DEFAULT_EMPTY_VALUE);
	}

	public TemplateRenderer(String useThisIfValueIsEmpty) {
		this(SafeHtmlUtils.fromString(useThisIfValueIsEmpty));
	}

	public TemplateRenderer(SafeHtml useThisIfValueIsEmpty) {
		this(useThisIfValueIsEmpty, null);
	}

	public TemplateRenderer(SafeHtml useThisIfValueIsEmpty, CustomValueFormatter formatter) {
		emptyHtml = useThisIfValueIsEmpty;
		empty = emptyHtml.asString();
		this.formatter = formatter;
	}

	public static interface CustomValueFormatter {
		SafeHtml format(String dataId, String rawValue);
	}

	public SafeHtml renderData(Map<String, String> data) {
		return renderData(template, data);
	}

	public SafeHtml renderData(String template, Map<String, String> data) {
		return renderData(template, data, this.formatter);
	}

	public SafeHtml renderData(String template, Map<String, String> data, CustomValueFormatter formatter) {
		assert Util.notEmpty(template);
		assert Util.notEmpty(data);
		for (String dataId : data.keySet()) {
			final String markUp = "%%" + dataId + "%%";
			// Got this error message one time: 
			// java.lang.IndexOutOfBoundsException: No group 2
			// at java.util.regex.Matcher.group(Matcher.java:470)
			if (template.contains(markUp)) {
				//				Log.debug(this, "renderData: found", dataId);
				String formattedValue = formatValue(dataId, data.get(dataId), formatter).asString();
				try {
					//					template = template.replaceAll(markUp, Matcher.quoteReplacement(formattedValue));
					template = template.replaceAll(markUp, formattedValue);
				} catch (Throwable t) {
					Log.warn(this,
						"renderData: markup=" + markUp + "; value=" + formattedValue + "; error=" + t.getMessage());
				}
			}
		}
		template = template.replaceAll("%%[^%]*%%", empty); // clean up all we did not have with the empty value
		//		Log.debug(this, "renderData: result", template);
		return SafeHtmlUtils.fromTrustedString(template);
	}

	private static final String DOLLAR_ESCAPE_CODE = "&#36;";

	/**
	 * @param dataId
	 * @param rawValue
	 * @param formatter
	 * @return
	 */
	private SafeHtml formatValue(String dataId, String rawValue, CustomValueFormatter formatter) {
		if (Util.isEmpty(rawValue)) {
			return emptyHtml;
		}
		return (formatter != null) ? formatter.format(dataId, rawValue) : SafeHtmlUtils.fromString(rawValue);
	}

	/**
	 * @param template2
	 * @param join
	 * @return
	 */
	public static SafeHtml render(String template, String... values) {
		TemplateRenderer renderer = new TemplateRenderer();
		Map<String, String> map = new HashMap<String, String>();
		for (int x = 0; x < values.length; x++) {
			map.put(Integer.toString(x), values[x]);
		}
		return renderer.renderData(template, map);
	}
}
