/**
 * 
 */
package gwt.tools.shared;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public interface Cache {

	/**
	 * @param key
	 * @param value
	 */
	void put(Object key, Object value);

	/**
	 * @param map
	 */
	void putAll(Map<? extends Object, ? extends Object> keyValuePairs);

	/**
	 * @param key
	 * @return
	 * @throws ExecutionException
	 * @throws InterruptedException
	 */
	Object get(Object key);

	Map<Object, Object> getAll(Collection<Object> keys);

	/**
	 * @param uri
	 * @return
	 */
	void remove(Object key);

	/**
	 * Removes all instances from the cache.
	 */
	void clear();

}
