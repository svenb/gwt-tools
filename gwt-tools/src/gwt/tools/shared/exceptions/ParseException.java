package gwt.tools.shared.exceptions;

import java.io.Serializable;

public class ParseException extends Exception implements Serializable {

	/**
	 * 
	 */
	public ParseException() {
		super();
	}

	public ParseException(String message) {
		super(message);
	}

	public ParseException(String message, Throwable cause) {
		super(message, cause);
	}

}
