package gwt.tools.shared.exceptions;

import java.io.Serializable;

public class TimeoutException extends NetworkException implements Serializable {

	/**
	 * 
	 */
	public TimeoutException() {
		super();
	}

	public TimeoutException(String message) {
		super(message);
	}

}
