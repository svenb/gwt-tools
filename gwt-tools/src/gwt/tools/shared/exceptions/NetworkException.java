package gwt.tools.shared.exceptions;

import java.io.Serializable;

public class NetworkException extends Exception implements Serializable {

	public static final int ServiceTemporarilyUnavailable = 503;
	
	public int expectedResultCode;
	public int returnedResultCode;
	
	/**
	 * 
	 */
	public NetworkException() {
		super();
	}

	public NetworkException(String message) {
		super(message);
	}
	public NetworkException(int expectedResultCode, int returnedResultCode) {
		this("The server responded with error code "
				+ returnedResultCode + ", not "+expectedResultCode+" as expected.",expectedResultCode,returnedResultCode);		
	}
	public NetworkException(String message, int expectedResultCode, int returnedResultCode) {
		this(message);
		this.expectedResultCode = expectedResultCode;
		this.returnedResultCode = returnedResultCode;
	}

	/**
	 * @param message
	 * @param timeoutE
	 */
	public NetworkException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
