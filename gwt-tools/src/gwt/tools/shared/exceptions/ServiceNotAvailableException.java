package gwt.tools.shared.exceptions;

import java.io.Serializable;

public class ServiceNotAvailableException extends Exception implements Serializable {

	private static final long serialVersionUID = -8921372747181036605L;
	private String serviceUrl;

	public ServiceNotAvailableException(String message, String serviceUrl, Throwable cause) {
		super(message, cause);
		this.serviceUrl = serviceUrl;
	}

	public ServiceNotAvailableException(String message, String serviceUrl) {
		super(message);
		this.serviceUrl = serviceUrl;
	}

	public ServiceNotAvailableException() {
		super();
	}

	public String getUrl() {
		return serviceUrl;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#getMessage()
	 */
	@Override public String getMessage() {
		return "Service not available. Service URL: " + serviceUrl + ", message: " + super.getMessage();
	}

	/*
	 * // * (non-Javadoc) // * // * @see
	 * java.lang.Throwable#getLocalizedMessage() //
	 */
	//	@Override public String getLocalizedMessage() {
	//		return super.getLocalizedMessage() + "\nService URL: " + serviceUrl;
	//	}
}
