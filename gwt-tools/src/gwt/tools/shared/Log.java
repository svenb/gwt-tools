/**
 * 
 */
package gwt.tools.shared;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class Log {

	public static enum Level {
		TRACE("TRC"), DEBUG("DGB"), NORMAL("NRM"), WARN("WRN"), SEVERE("SVR");

		private final String label;

		Level(String label) {
			this.label = label;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Enum#toString()
		 */
		@Override public String toString() {
			return label;
		}
	}

	public static interface SevereErrorCallback {
		void onSevereError(String message);
	}

	public static final List<SevereErrorCallback> severeErrorCallbacks = new LinkedList<SevereErrorCallback>();

	private static interface UnformattedData {
		String format(Formatter formatter);
	}

	private static interface Formatter {
		String format(ObjectData data);

		String format(FormatOnDemand data);
	}

	public static interface FormatOnDemand {
		String format();
	}

	private static class OnDemandData implements UnformattedData {

		private final FormatOnDemand data;

		/**
		 * 
		 */
		public OnDemandData(FormatOnDemand data) {
			this.data = data;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * gwt.tools.shared.Log.UnformattedData#format(gwt.tools.shared.Log.
		 * Formatter)
		 */
		@Override public String format(Formatter formatter) {
			return formatter.format(data);
		}

	}

	private static class ObjectData implements UnformattedData {

		private final Object[] objects;

		/**
		 * 
		 */
		public ObjectData(Object... objects) {
			this.objects = objects;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * gwt.tools.shared.Log.UnformattedData#accept(gwt.tools.shared.Log.
		 * Formatter)
		 */
		@Override public String format(Formatter formatter) {
			return formatter.format(this);
		}
	}

	private static class StringFormatter implements Formatter {
		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * gwt.tools.shared.Log.Formatter#format(gwt.tools.shared.Log.FormatOnDemand
		 * )
		 */
		@Override public String format(FormatOnDemand data) {
			return data.format();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * gwt.tools.shared.Log.Formatter#format(gwt.tools.shared.Log.ObjectData
		 * )
		 */
		@Override public String format(ObjectData data) {
			return Util.join(data.objects, ",");
		}
	}

	private static final StringFormatter defaultFormatter = new StringFormatter();

	private static final Map<Class<?>, Level> classFilter = new HashMap<Class<?>, Level>();
	private static final Map<String, Level> classNameFilter = new HashMap<String, Level>();
	private static final Map<String, Level> packageFilter = new HashMap<String, Level>();

	private static long start = System.currentTimeMillis();
	private static boolean filterByClass = false;
	private static boolean filterByPackageName = false;
	private static Level generalMinimumLevel = Level.DEBUG;

	public static void setFilterByClass(boolean on) {
		filterByClass = on;
	}

	public static void setFilterByPackageName(boolean on) {
		filterByPackageName = on;
	}

	public static void addFilter(String packageName, Level minimumLogLevel) {
		packageFilter.put(packageName, minimumLogLevel);
	}

	public static void addFilter(Class<?> targetClass, Level minimumLogLevel) {
		classFilter.put(targetClass, minimumLogLevel);
		classNameFilter.put(targetClass.getName(), minimumLogLevel);
	}

	private static boolean valid(Object source, Level level) {
		assert source != null;
		assert level != null;
		if (generalMinimumLevel != null && generalMinimumLevel.compareTo(level) > 0) {
			return false;
		}
		if (filterByPackageName) {
			Class<?> sourceClass = (source instanceof Class) ? (Class<?>) source : source.getClass();
			String name = sourceClass.getName();
			int lastDot = name.lastIndexOf(".");
			if (lastDot > -1) {
				name = name.substring(0, lastDot);
			} else {
				name = "";
			}
			if (packageFilter.containsKey(name)) {
				if (packageFilter.get(name).compareTo(level) > 0) {
					return false;
				}
			}
			// partial match of package name?
			for (String packageName : packageFilter.keySet()) {
				if (name.startsWith(packageName)) {
					Level filterLevel = packageFilter.get(packageName);
					// cache match to avoid iterating over the map next time
					packageFilter.put(name, filterLevel);
					if (filterLevel.compareTo(level) > 0) {
						return false;
					}
				}
			}
		}
		if (filterByClass) {
			Class<?> sourceClass = (source instanceof Class) ? (Class<?>) source : source.getClass();
			while (sourceClass != null) {
				if (classFilter.containsKey(sourceClass)) {
					if (classFilter.get(sourceClass).compareTo(level) > 0) {
						return false;
					}
					break;
				}
				sourceClass = sourceClass.getSuperclass();
			}
		}
		return true;
	}

	private static void print(Object source, Level level, String message, UnformattedData data) {
		if (valid(source, level)) {
			String formattedMessage = formatMessage(source, level, message,
				(data == null) ? null : data.format(defaultFormatter));
			((level.compareTo(Level.WARN) >= 0) ? System.err : System.out).println(formattedMessage);

			if (level.compareTo(Level.SEVERE) >= 0 && Util.size(severeErrorCallbacks) > 0) {
				for (SevereErrorCallback callback : severeErrorCallbacks) {
					try {
						callback.onSevereError(formattedMessage);
					} catch (Exception e) { // no errors while printing error
					}
				}
			}
		}
	}

	private static String formatMessage(Object source, Level level, String message, String formattedData) {
		//		return (GWT.isClient() ? "C " : "S ") + level + " " + getCurrentTimeCode() + " "
		//			+ Util.getSimpleClassName(source) + ": " + message
		//			+ (Util.notEmpty(formattedData) ? "=" + formattedData : "");
		StringBuilder builder = new StringBuilder(2 + 3 + 1 + 10 + 22 + Util.size(message) + 1
			+ Util.size(formattedData));
		try {
			builder.append(Util.isClient() ? "C " : "S ");
		} catch (Throwable t) {
		}
		builder.append(level);
		builder.append(" ");
		builder.append(getCurrentTimeCode());
		builder.append(" ");
		builder.append(Util.getSimpleClassName(source));
		builder.append(": ");
		if (Util.notEmpty(message)) {
			builder.append(message);
		}
		if (Util.notEmpty(formattedData)) {
			builder.append("=");
			builder.append(formattedData);
		}
		return builder.toString();
	}

	public static long getCurrentTimeCode() {
		return System.currentTimeMillis() - start;
	}

	public static void trace(Object source, String message, Object... objects) {
		print(source, Level.TRACE, message, new ObjectData(objects));
	}

	public static void trace(Object source, FormatOnDemand formatOnDemand) {
		trace(source, null, formatOnDemand);
	}

	public static void trace(Object source, String message, FormatOnDemand formatOnDemand) {
		print(source, Level.TRACE, message, new OnDemandData(formatOnDemand));
	}

	public static void debug(Object source, FormatOnDemand formatOnDemand) {
		print(source, Level.DEBUG, "", new OnDemandData(formatOnDemand));
	}

	public static void debug(Object source, Object object) {
		print(source, Level.DEBUG, "", new ObjectData(object));
	}

	public static void debug(Object source, String message) {
		print(source, Level.DEBUG, message, null);
	}

	public static void debug(Object source, String message, FormatOnDemand formatOnDemand) {
		print(source, Level.DEBUG, message, new OnDemandData(formatOnDemand));
	}

	public static void debug(Object source, String message, Object... objects) {
		//		print(source, Level.DEBUG, message + ": " + Util.join(objects, ", "));
		print(source, Level.DEBUG, message, new ObjectData(objects));
	}

	public static void log(Object source, String message, Object... objects) {
		print(source, Level.NORMAL, message, new ObjectData(objects));
	}

	public static void log(Object source, FormatOnDemand formatOnDemand) {
		log(source, null, formatOnDemand);
	}

	public static void log(Object source, String message, FormatOnDemand formatOnDemand) {
		print(source, Level.NORMAL, message, new OnDemandData(formatOnDemand));
	}

	/**
	 * @param string
	 */
	public static void warn(Object source, String message, Object... objects) {
		print(source, Level.WARN, message, new ObjectData(objects));
	}

	public static void warn(Object source, String message, Throwable t, Object... objects) {
		print(source, Level.WARN, message, new ObjectData(objects));
		printError(t);
	}

	public static void severe(Object source, String message, Throwable t) {
		severe(source, message);
		printError(t);
	}

	/**
	 * @param t
	 */
	private static void printError(Throwable t) {
		System.err.print("    -> ");
		t.printStackTrace();
	}

	public static void severe(Object source, String message, Object... objects) {
		print(source, Level.SEVERE, message, new ObjectData(objects));
	}

	//	/**
	//	 * Execute UI building code in the callback, not right after calling this
	//	 * method!
	//	 * 
	//	 * @param doneCallback
	//	 * 
	//	 */
	//	public static void setUncaughtExceptionHandler(final SimpleCallback doneCallback) {
	//		assert doneCallback != null;
	//		GWT.setUncaughtExceptionHandler(new GWT.UncaughtExceptionHandler() {
	//
	//			@Override public void onUncaughtException(Throwable e) {
	//				severe(this, "UNCAUGHT EXCEPTION", e);
	//			}
	//		});
	//
	//		// further execution needs to be deferred so that the generic
	//		// "uncaught exception handler" can hook in before any UI-related code
	//		// gets executed
	//		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
	//
	//			@Override public void execute() {
	//				doneCallback.done();
	//			}
	//		});
	//	}

	public static Level getGeneralMinimumLevel() {
		return generalMinimumLevel;
	}

	public static void setGeneralMinimumLevel(Level generalMinimumLevel) {
		Log.generalMinimumLevel = generalMinimumLevel;
	}

}
