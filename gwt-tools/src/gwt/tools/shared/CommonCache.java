/**
 * 
 */
package gwt.tools.shared;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class CommonCache implements Cache {

	public static interface CacheImpl {
		Object get(Object key);

		void put(Object key, Object value);
	};

	//	private static final String PREFIX = "cache";
	//	private static final boolean USE_PREFIX = true;
	protected final Cache cacheImpl;
	//	protected final boolean isMultiGet;
	private boolean isServer;
	private String printPrefix;
	private static Statistics statistics = new Statistics();

	//	protected Cache(CacheImpl cacheInstance) {
	//		this.cacheInstance = cacheInstance;
	//	}

	protected CommonCache(Cache cacheImpl) {
		this.cacheImpl = cacheImpl;
		isServer = cacheImpl.getClass().getName().indexOf("server") >= 0;
		printPrefix = isServer ? "server" : "client";
	}

	/**
	 * Puts value in cache if key != null.
	 * 
	 * @param key
	 * @param value
	 */
	@Override public void put(Object key, Object value) {
		if (key != null) {
			statistics.puts++;
			try {
				//				print("put", key + "=" + ((value instanceof byte[]) ? ((byte[]) value).length + " bytes blob" : value));
				Log.debug(this, ".put: $key=$value", key, ((value instanceof byte[]) ? ((byte[]) value).length
					+ " bytes blob" : value));
				//				cacheImpl.put(getCacheKey(key), value);
				cacheImpl.put(key, value);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			statistics.invalidPuts++;
		}
	}

	//	/**
	//	 * Puts all the objects given in the map in the same time - should perform
	//	 * better than calling put for each.
	//	 * 
	//	 * @param map
	//	 */
	//	public void putAll(Map<Object, Object> map) {
	//		//		Map<Object, Object> castedMap = Util.downCastMap(map, Object.class, Object.class);
	//		if (Util.notEmpty(map)) {
	//			statistics.multiPuts++;
	//			Map<Object, Object> toCache = new HashMap<Object, Object>();
	//			for (Object key : map.keySet()) {
	//				if (key != null) {
	//					toCache.put(getCacheKey(key), map.get(key));
	//				} else {
	//					statistics.invalidPuts++;
	//				}
	//			}
	//			cacheImpl.putAll(toCache);
	//			print("putAll", "result=" + Util.toString(toCache));
	//		}
	//	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.shared.CommonCache#putAll(java.util.Map)
	 */
	@Override public void putAll(Map<? extends Object, ? extends Object> map) {
		if (Util.notEmpty(map)) {
			statistics.multiPuts++;
			//			Map<Object, Object> toCache = new HashMap<Object, Object>();
			//			for (Iterator<? extends Object> iterator = map.keySet().iterator(); iterator.hasNext();) {
			//				//				if (iterator.next()!=null) {
			//				//					toCache.put(getCacheKey(key), map.get(key));
			//				//				} else {
			//				//					statistics.invalidPuts++;
			//				//				}
			//				if (iterator.next() == null) {
			//					statistics.invalidPuts++;
			//					iterator.remove();
			//				}
			//			}
			if (map.containsKey(null)) {
				map.remove(null);
				statistics.invalidPuts++;
			}
			//			cacheImpl.putAll(toCache);
			//			print("putAll", "result=" + Util.toString(toCache));
			cacheImpl.putAll(map);
			//						print("putAll", "result=" + Util.toString(map));
			Log.debug(this, ".putAll: result", map);
		}
	}

	/**
	 * Returns the cached value for the given key if there is any and key !=
	 * null - null otherwise.
	 * 
	 * @param key
	 * @param resultClass
	 * @return cached value for given key or null
	 */
	public <T extends Object> T get(Object key, Class<T> resultClass) {
		if (key != null) {
			Object cachedObject = null;
			try {
				if ((cachedObject = get(key)) == null) {
					return null;
				}
				if (resultClass == null || resultClass == Object.class) {
					return (T) cachedObject;
				}
				//				if (!resultClass.isAssignableFrom(cachedObject.getClass())) { ... does not work: 	[ERROR] [magnetv1] - Line 93: The method isAssignableFrom(Class<capture#2-of ? extends Object>) is undefined for the type Class<T>
				if (!resultClass.getName().equals(cachedObject.getClass().getName())) {
					//					print("get", "could not cast result(" + cachedObject.getClass() + "=" + cachedObject
					//						+ ") into required " + resultClass + "; key=" + key);
					Log.debug(this,
						".get($key): could not cast result($class=$object) into required $resultClass; $key=", key,
						cachedObject.getClass(), cachedObject, resultClass);
					return null;
				}
				T cachedResult = (T) cachedObject;
				//				print(
				//					"get",
				//					"key=" + Util.shortenString(key.toString(), 30) + " -> "
				//						+ Util.shortenString(cachedResult.toString(), 40));
				//				print("get", key + "=" + cachedResult);
				Log.debug(this, ".get($key): $cachedResult", key, cachedResult);
				return cachedResult;
			} catch (ClassCastException cce) {
				//				print("get",
				//					"key=" + key + "; could not cast result("
				//						+ ((cachedObject != null) ? cachedObject.getClass() + "=" + cachedObject : null)
				//						+ ") into required " + resultClass);
				Log.debug(this, ".get($key): could not cast result(#cachedClass, $object) into $requiredClass ", key,
					(cachedObject != null) ? cachedObject.getClass() : null, cachedObject, resultClass);
			} catch (Throwable t) {

			}
		}
		statistics.invalidGets++;
		return null;
	}

	/**
	 * Returns the cached value for the given key if there is any and key !=
	 * null - null otherwise.
	 * 
	 * @param key
	 * @return cached value for given key or null
	 * @throws ExecutionException
	 * @throws InterruptedException
	 */
	@Override public Object get(Object key) {
		if (key != null) {
			statistics.gets++;
			//			Object cached = cacheImpl.get(getCacheKey(key));
			Object cached = cacheImpl.get(key);
			if (cached != null) {
				statistics.hits++;
			}
			return cached;
		}
		statistics.invalidGets++;
		return null;
	}

	/**
	 * @param uris
	 * @param class1
	 * @param prefix
	 * @return
	 */
	public <T extends Object & Serializable> Map<Object, T> getAll(Collection<Object> keys, Class<T> resultClass) {
		if (Util.isEmpty(keys) || keys.iterator().next() == null) {
			return new HashMap<Object, T>();
		}
		Map<Object, T> results = (Map<Object, T>) getAll(keys);

		//		print("getAll(" + keys.size() + "->" + results.keySet().size() + ")", "results=" + Util.toString(results));
		Log.debug(this, ".getAll($keys): $result.keys, results", keys.size(), results.keySet().size(), results);
		return results;
	}

	@Override public Map<Object, Object> getAll(Collection<Object> keys) {
		assert keys != null;
		//		Map<Object, Object> results = new HashMap<Object, Object>();
		//		if (isMultiGet) {
		statistics.multiGets++;
		statistics.gets += keys.size();
		// no key mapping because the layers below handle the name spacing internally :)
		//			Map<Object, Object> allCached = null;
		//			Map<Object, Object>< cacheKeysMap = getCacheKeyMap(keys);
		//			Collection<Object> cacheKeys = getCacheKeys(keys, cacheKeysMap);
		//			try {
		//				allCached = ((MultiGetMap<Object, Object>) cacheImpl).getAll(cacheKeys);
		//			} catch (Throwable t) {
		//				Log.debug(this, ".getAll: reading cache failed. error=" + t.getMessage());
		//			}
		//			if (Util.notEmpty(allCached)) {
		//				for (Object cacheKey : allCached.keySet()) {
		//					Object cached = allCached.get(cacheKey);
		//					if (cached != null) {
		//						statistics.hits++;
		//						results.put(getKey(cacheKey, cacheKeysMap), cached);
		//					}
		//				}
		//			}
		try {
			//				return ((MultiGetCache<Object, Object>) cacheImpl).getAll(keys);
			Map<Object, Object> cached = cacheImpl.getAll(keys);
			for (Object result : cached.values()) {
				if (result != null) {
					statistics.hits++;
				}
			}
			return cached;
		} catch (Throwable t) {
			Log.debug(this, ".getAll: reading cache failed. ", t);
		}
		//		} else {
		//			for (Object key : keys) {
		//				Object cached;
		//				try {
		//					// get(key, resultClass);
		//					cached = get(key);
		//					if (cached != null) {
		//						results.put(key, cached);
		//					}
		//				} catch (Throwable t) {
		//					Log.debug(this,
		//						".getAll: reading each separately failed on key=" + key + ". error=" + t.getMessage());
		//				}
		//			}
		//		}
		//		return results;
		return Collections.emptyMap();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.shared.Cache#remove(java.lang.Object)
	 */
	@Override public void remove(Object key) {
		cacheImpl.remove(key);
	}

	//
	//	/**
	//	 * Removes all items from cache.
	//	 */
	//	public void clear() {
	//		cacheImpl.clear();
	//	}
	//
	//	/**
	//	 * @return Number of items currently in the cache.
	//	 */
	//	public long size() {
	//		return cacheImpl.size();
	//	}

	// cacheKey to key mapping ////////////////////////////////////////////////

	// mapping replaced by using name space features of each layer of caching :)
	//	private Object getCacheKey(Object key) {
	//		return USE_PREFIX ? ((key == null) ? null : PREFIX + key.toString()) : key;
	//	}
	//
	//	/**
	//	 * @param keys
	//	 * @return
	//	 */
	//	private Map<Object, Object> getCacheKeyMap(Collection<Object> keys) {
	//		Map<Object, Object> cacheKeysMap = null;
	//		if (USE_PREFIX) {
	//			cacheKeysMap = new HashMap<Object, Object>();
	//			for (Object key : keys) {
	//				cacheKeysMap.put(getCacheKey(key), key);
	//			}
	//		}
	//		return cacheKeysMap;
	//	}
	//
	//	private Collection<Object> getCacheKeys(Collection<Object> keys, Map<Object, Object> cacheKeysMap) {
	//		return (cacheKeysMap == null) ? keys : cacheKeysMap.keySet();
	//	}
	//
	//	private Object getKey(Object cacheKey, Map<Object, Object> cacheKeysMap) {
	//		return (cacheKeysMap == null) ? cacheKey.toString() : cacheKeysMap.get(cacheKey);
	//	}

	public static class Statistics {
		public int gets;
		public int multiGets;
		public int hits;
		public int invalidGets;
		public int puts;
		public int multiPuts;
		public int invalidPuts;

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#clone()
		 */
		protected Statistics clone() {
			Statistics clone = new Statistics();
			clone.gets = gets;
			clone.multiGets = multiGets;
			clone.hits = hits;
			clone.invalidGets = invalidGets;
			clone.puts = puts;
			clone.multiPuts = multiPuts;
			clone.invalidPuts = invalidPuts;
			return clone;
		}
	}

	public static Statistics getStatistics() {
		// TODO GAE memcache also provides stats which should be integrated
		return statistics.clone();
	}

	// helper /////////////////////////////////////////////////////////////////

	//	private void print(String methode, String message) {
	//		Log.debug(this, "[" + printPrefix + "]." + methode + ": " + message);
	//	}

	/**
	 * Removes all cached instances
	 */
	public void clear() {
		cacheImpl.clear();
	}
}
